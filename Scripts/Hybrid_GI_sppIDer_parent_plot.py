#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 18 23:24:59 2021

@author: devin
"""
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import matplotlib.cm as cm
import matplotlib as mpl

font = {'family' : 'sans-serif','weight' : 'normal','size' : 10}
plt.rc('font', **font)

#%% NUCLEAR GENOMES
species=['Scer','Spar','Smik','Sjur','Skud','Sarb','Seub','Suva']
species_mito=['Scer_mito','Spar_mito','Smik_mito','Sjur_mito','Skud_mito','Sarb_mito','Seub_mito','Suva_mito']
data_mapped=pd.DataFrame(columns=species,index=species)
data_mapped=data_mapped.astype(float)

for i in species:
    data=pd.read_csv('../DATA/sppIDer/parents/parent_nuclear_summary/{}_MQsummary.txt'.format(i), sep='\t',skiprows=6,header = None,index_col=0,names=['total','%reads','%nonzero','aveMQ','nonzeroMQ','medianMQ','nonzeromedMQ'])
    for j in species:
        data_mapped.loc[i,j]=data.loc[j,'%nonzero']
for j in species:
    data_mapped[j] = data_mapped[j].str.rstrip('%').astype('float')

plot_loc={'Scer':811,'Spar':812,'Smik':813,'Sjur':814,'Skud':815,'Sarb':816,'Seub':817,'Suva':818}
colors={'Scer':"#015482",'Spar':"#dc4d01",'Smik':"#ffda03",'Sjur':'#a2653e','Skud':'#2b5d34','Sarb':'gray','Seub':'#8c000f','Suva':'#4b006e'}

plt.figure(figsize=[6,3])
for i in colors:
    plt.subplot(plot_loc[i])
    x=pd.DataFrame(data_mapped[i])
    x_t=x.transpose()
    sns.heatmap(x_t,cmap=sns.light_palette(colors[i], as_cmap=True),vmin=0,vmax=100,cbar=False,annot=True,fmt='g',linecolor='black',linewidth=1)
    plt.xticks([])
    plt.yticks([])

plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=0)
plt.savefig('../Figures/Hybrid_GI_parents_nuclear.svg',bbox_inches='tight',dpi=1000)

#%% MITOCHONDRIA PARENTS

species=['Scer','Spar','Smik','Sjur','Skud','Sarb','Seub','Suva']
species_mito=['Scer_mito','Spar_mito','Smik_mito','Sjur_mito','Skud_mito','Sarb_mito','Seub_mito_FM','Suva_mito']
species_mito2=['Scer_mito','Spar_mito','Smik_mito','Sjur_mito','Skud_mito','Sarb_mito','Seub_mito','Suva_mito']
data_mapped=pd.DataFrame(columns=species_mito,index=species)
data_mapped=data_mapped.astype(float)

for i in species:
    data=pd.read_csv('../DATA/sppIDer/parents/parent_mito_summary/{}_MQsummary.txt'.format(i), sep='\t',skiprows=6,header = None,index_col=0,names=['total','%reads','%nonzero','aveMQ','nonzeroMQ','medianMQ','nonzeromedMQ'])
    for j in species_mito:
        data_mapped.loc[i,j]=data.loc[j,'%nonzero']
for j in species_mito:
    data_mapped[j] = data_mapped[j].str.rstrip('%').astype('float')

plot_loc={'Scer_mito':811,'Spar_mito':812,'Smik_mito':813,'Sjur_mito':814,'Skud_mito':815,'Sarb_mito':816,'Seub_mito_FM':817,'Suva_mito':818}
colors={'Scer_mito':"#015482",'Spar_mito':"#dc4d01",'Smik_mito':"#ffda03",'Sjur_mito':'#a2653e','Skud_mito':'#2b5d34','Sarb_mito':'gray','Seub_mito_FM':'#8c000f','Suva_mito':'#4b006e'}

plt.figure(figsize=[6,3])
for i in colors:
    plt.subplot(plot_loc[i])
    x=pd.DataFrame(data_mapped[i])
    x_t=x.transpose()
    sns.heatmap(x_t,cmap=sns.light_palette(colors[i], as_cmap=True),vmin=0,vmax=100,cbar=False,annot=True,fmt='g',linecolor='black',linewidth=1)
    plt.xticks([])
    plt.yticks([])
    #plt.hlines(y=[0,1],xmin=0,xmax=204,color='black',linewidth=5)
    #plt.vlines(x=[0,204],ymin=0,ymax=1,color='black',linewidth=5)
plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=0.)
plt.savefig('../Figures/Hybrid_GI_parents_mito.svg',bbox_inches='tight',dpi=1000)