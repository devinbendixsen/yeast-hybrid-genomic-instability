#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 18 14:27:52 2021

@author: devin
"""
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import matplotlib.cm as cm
import matplotlib as mpl
import pickle
import scipy.stats
font = {'family' : 'sans-serif','weight' : 'normal','size' : 12}
plt.rc('font', **font)

#%% import pickles / set variables
hybrid_order = pickle.load( open( "../Pickle/hybrid_order.p", "rb" ) )
hybrid_sp={'SRR1119201':['Suva','Scer','Skud'],'SRR1119202':['Suva','Scer','Skud'],'SRR1119203':['Suva','Scer'],'SRR1119204':['Scer','Suva','Seub'],'SRR1649181':['Scer','Seub'],'SRR1649183':['Scer','Seub'],'SRR800854':['Scer','Spar'],'SRR800807':['Scer','Spar'],'SRR800768':['Scer','Spar'],'SRR2967902':['Scer','Spar'],'SRR2967903':['Scer','Spar'],'SRR2967904':['Scer','Skud'],'SRR2967905':['Scer','Smik'],'SRR2967906':['Scer','Suva'],'SRR2967849':['Scer','Skud'],'SRR2967853':['Scer','Skud'],'SRR2967864':['Scer','Skud'],'DRR040649':['Scer','Seub'],'DRR040656':['Scer','Seub'],'DRR040657':['Scer','Seub'],'DRR040637':['Scer','Seub'],'DRR040641':['Scer','Seub'],'DRR040645':['Scer','Seub'],'DRR040658':['Scer','Seub'],'DRR040650':['Scer','Seub'],'DRR040669':['Scer','Suva','Seub'],'DRR040660':['Scer','Seub'],'SRR2968024':['Scer','Skud'],'DRR040667':['Scer','Suva','Seub'],'SRR2968005':['Scer','Skud'],'SRR2967882':['Scer','Suva'],'ERR1111502':['Scer','Spar'],'ERR1111503':['Scer','Spar'],'SRR2967875':['Scer','Skud'],'DRR040654':['Scer','Seub'],'SRR2968042':['Scer','Skud'],'SRR2968046':['Scer','Seub'],'SRR3265414':['Scer','Skud'],'SRR7631525':['Scer','Suva'],'ERR3010122':['Scer','Spar'],'ERR3010128':['Scer','Spar'],'ERR3010129':['Scer','Spar'],'ERR3010125':['Scer','Spar'],'ERR3010126':['Scer','Spar'],'ERR3010127':['Scer','Spar'],'SRR8648839':['Scer','Seub'],'ERR3010130':['Scer','Spar'],'SRR8648841':['Scer','Seub'],'ERR3010131':['Scer','Spar'],'ERR3010132':['Scer','Spar'],'ERR3010133':['Scer','Spar'],'ERR3010134':['Scer','Spar'],'ERR3010135':['Scer','Spar'],'ERR3010136':['Scer','Spar'],'ERR3010137':['Scer','Spar'],'ERR3010138':['Scer','Spar'],'ERR3010139':['Scer','Spar'],'ERR3010140':['Scer','Spar'],'ERR3010143':['Scer','Spar'],'SRR10047154':['Scer','Seub'],'SRR10047336':['Scer','Seub','Skud'],'SRR10047416':['Seub','Suva'],'SRR10047092':['Scer','Seub'],'SRR10047152':['Scer','Seub'],'ERR3010146':['Scer','Spar'],'ERR3010147':['Scer','Spar'],'ERR3010149':['Scer','Spar'],'ERR3010150':['Scer','Spar'],'SRR8799642':['Scer','Seub'],'SRR8799680':['Scer','Seub'],'SRR8799685':['Scer','Seub'],'SRR8799646':['Scer','Seub'],'SRR8799652':['Scer','Seub'],'SRR8799686':['Scer','Seub'],'SRR8799647':['Scer','Seub'],'SRR8799692':['Scer','Seub'],'SRR8799694':['Scer','Seub'],'SRR8799654':['Scer','Seub'],'SRR8799683':['Scer','Seub'],'SRR8799682':['Scer','Seub'],'SRR8799666':['Scer','Seub'],'SRR8799653':['Scer','Seub'],'SRR8799631':['Scer','Seub'],'SRR8799695':['Scer','Seub'],'SRR8799649':['Scer','Seub'],'SRR8799645':['Scer','Seub'],'SRR8799681':['Scer','Seub'],'SRR8799684':['Scer','Seub'],'SRR8799676':['Scer','Seub'],'SRR8799687':['Scer','Seub'],'SRR8799691':['Scer','Seub'],'SRR8799693':['Scer','Seub'],'SRR8799659':['Scer','Seub'],'SRR8799643':['Scer','Seub'],'SRR8799644':['Scer','Seub'],'SRR8799662':['Scer','Seub'],'SRR8799663':['Scer','Seub'],'SRR8799634':['Scer','Seub'],'SRR8799640':['Scer','Seub'],'SRR8799671':['Scer','Seub'],'SRR8799667':['Scer','Seub'],'SRR8799637':['Scer','Seub'],'SRR8799690':['Scer','Seub'],'SRR8799673':['Scer','Skud'],'SRR8799672':['Scer','Skud'],'SRR8799674':['Scer','Skud'],'SRR8799650':['Scer','Skud'],'SRR8799633':['Scer','Skud'],'SRR8799677':['Scer','Skud'],'SRR8799678':['Scer','Skud'],'SRR8799651':['Scer','Skud'],'SRR8799679':['Scer','Skud'],'SRR8799657':['Suva','Seub'],'SRR8799661':['Suva','Seub'],'SRR8799665':['Suva','Seub'],'SRR8799698':['Suva','Seub'],'SRR8799660':['Suva','Seub'],'SRR8799658':['Suva','Seub'],'SRR8799655':['Suva','Seub'],'SRR8799668':['Suva','Seub'],'SRR8799641':['Suva','Seub'],'SRR8799656':['Suva','Seub'],'SRR8799630':['Suva','Seub'],'SRR8799629':['Suva','Seub'],'SRR8799664':['Suva','Seub'],'SRR10035841':['Scer','Skud'],'SRR10047007':['Scer','Skud'],'SRR10047062':['Scer','Seub'],'SRR10047068':['Scer','Seub'],'SRR10047077':['Scer','Seub'],'SRR10047225':['Scer','Seub'],'SRR10047243':['Scer','Seub','Suva'],'SRR10047261':['Scer','Seub'],'SRR10047170':['Scer','Seub'],'SRR10047181':['Scer','Seub'],'SRR10047231':['Scer','Skud'],'SRR10047188':['Scer','Seub'],'SRR10047256':['Scer','Seub'],'SRR10047124':['Scer','Seub'],'SRR10047019':['Seub','Suva'],'SRR10047000':['Scer','Seub'],'SRR10047191':['Scer','Seub'],'SRR10047313':['Scer','Seub'],'SRR10047163':['Scer','Seub'],'SRR10047100':['Scer','Seub'],'SRR10047089':['Scer','Seub'],'SRR10047028':['Scer','Skud'],'SRR10047024':['Scer','Seub'],'SRR10047374':['Scer','Seub'],'SRR10047307':['Scer','Seub'],'SRR10047017':['Scer','Seub'],'SRR10047241':['Scer','Seub'],'SRR10047295':['Seub','Suva'],'SRR10047331':['Seub','Suva'],'SRR10047328':['Seub','Suva'],'SRR10047359':['Seub','Suva'],'SRR10047351':['Seub','Suva'],'SRR10047335':['Seub','Suva'],'SRR10035842':['Seub','Suva'],'SRR10047405':['Seub','Suva'],'SRR10047402':['Seub','Suva'],'SRR10047389':['Seub','Suva'],'SRR10047380':['Seub','Suva'],'SRR10047371':['Seub','Suva'],'SRR10047378':['Seub','Suva'],'SRR10047356':['Scer','Seub','Skud','Suva'],'SRR10047345':['Seub','Suva'],'SRR10047410':['Scer','Seub','Skud','Suva'],'SRR10047383':['Scer','Seub','Skud','Suva'],'SRR10047395':['Seub','Suva'],'SRR10047344':['Seub','Suva'],'SRR10047362':['Scer','Seub'],'SRR10047189':['Scer','Seub'],'SRR10047157':['Scer','Seub'],'SRR10047176':['Scer','Seub'],'SRR10047149':['Scer','Seub'],'SRR10047146':['Scer','Seub'],'SRR10047122':['Scer','Skud'],'SRR10047102':['Scer','Seub'],'SRR10047096':['Scer','Seub'],'SRR10047094':['Scer','Seub'],'SRR10047057':['Scer','Seub'],'SRR10046945':['Scer','Seub'],'SRR10047365':['Scer','Seub','Suva'],'SRR10046950':['Seub','Suva'],'SRR10046952':['Seub','Suva'],'SRR10046972':['Scer','Skud'],'SRR10046976':['Scer','Seub'],'SRR5141258':['Scer','Seub'],'SRR5141260':['Scer','Seub'],'SRR12703370':['Scer','Seub'],'SRR12703369':['Scer','Seub'],'SRR12703368':['Scer','Seub'],'SRR12703367':['Scer','Seub'],'SRR12703366':['Scer','Seub'],'SRR5251559':['Scer','Suva'],'SRR10208681':['Scer','Seub'],'SRR9925228':['Scer','Skud'],'SRR9925227':['Scer','Skud'],'SRR9925226':['Scer','Skud'],'SRR9925224':['Scer','Skud'],'SRR9925223':['Scer','Skud'],'SRR9925222':['Scer','Skud'],'SRR9925225':['Scer','Skud']}
species=['Scer','Spar','Smik','Sjur','Skud','Sarb','Seub','Suva']
hybrid_depth = pickle.load( open( "../Pickle/hybrid_depth.p", "rb" ) )
hybrid_depth_pos = pickle.load( open( "../Pickle/hybrid_depth_pos.p", "rb" ) )
chro_gain = pickle.load( open( "../Pickle/chro_gain.p", "rb" ) )
chro_loss = pickle.load( open( "../Pickle/chro_loss.p", "rb" ) )
data_mapped = pickle.load( open( "../Pickle/data_mapped.p", "rb" ) )
depth_var = pickle.load( open( "../Pickle/depth_var.p", "rb" ) )
map_filt = pickle.load( open( "../Pickle/map_filt.p", "rb" ) )
#%%
sp1_dict={}
sp_diff={}
sp1=[]
sp2=[]
color=[]
sp1_var=[]
sp2_var=[]
sp1_gain=[]
sp2_gain=[]
sp1_loss=[]
sp2_loss=[]
hybrid_sp={'SRR1119201':['Suva','Scer','Skud'],'SRR1119202':['Suva','Scer','Skud'],'SRR1119203':['Suva','Scer'],'SRR1119204':['Scer','Suva','Seub'],'SRR1649181':['Scer','Seub'],'SRR1649183':['Scer','Seub'],'SRR800854':['Scer','Spar'],'SRR800807':['Scer','Spar'],'SRR800768':['Scer','Spar'],'SRR2967902':['Scer','Spar'],'SRR2967903':['Scer','Spar'],'SRR2967904':['Scer','Skud'],'SRR2967905':['Scer','Smik'],'SRR2967906':['Scer','Suva'],'SRR2967849':['Scer','Skud'],'SRR2967853':['Scer','Skud'],'SRR2967864':['Scer','Skud'],'DRR040649':['Scer','Seub'],'DRR040656':['Scer','Seub'],'DRR040657':['Scer','Seub'],'DRR040637':['Scer','Seub'],'DRR040641':['Scer','Seub'],'DRR040645':['Scer','Seub'],'DRR040658':['Scer','Seub'],'DRR040650':['Scer','Seub'],'DRR040669':['Scer','Suva','Seub'],'DRR040660':['Scer','Seub'],'SRR2968024':['Scer','Skud'],'DRR040667':['Scer','Suva','Seub'],'SRR2968005':['Scer','Skud'],'SRR2967882':['Scer','Suva'],'ERR1111502':['Scer','Spar'],'ERR1111503':['Scer','Spar'],'SRR2967875':['Scer','Skud'],'DRR040654':['Scer','Seub'],'SRR2968042':['Scer','Skud'],'SRR2968046':['Scer','Seub'],'SRR3265414':['Scer','Skud'],'SRR7631525':['Scer','Suva'],'ERR3010122':['Scer','Spar'],'ERR3010128':['Scer','Spar'],'ERR3010129':['Scer','Spar'],'ERR3010125':['Scer','Spar'],'ERR3010126':['Scer','Spar'],'ERR3010127':['Scer','Spar'],'SRR8648839':['Scer','Seub'],'ERR3010130':['Scer','Spar'],'SRR8648841':['Scer','Seub'],'ERR3010131':['Scer','Spar'],'ERR3010132':['Scer','Spar'],'ERR3010133':['Scer','Spar'],'ERR3010134':['Scer','Spar'],'ERR3010135':['Scer','Spar'],'ERR3010136':['Scer','Spar'],'ERR3010137':['Scer','Spar'],'ERR3010138':['Scer','Spar'],'ERR3010139':['Scer','Spar'],'ERR3010140':['Scer','Spar'],'ERR3010143':['Scer','Spar'],'SRR10047154':['Scer','Seub'],'SRR10047336':['Scer','Seub','Skud'],'SRR10047416':['Seub','Suva'],'SRR10047092':['Scer','Seub'],'SRR10047152':['Scer','Seub'],'ERR3010146':['Scer','Spar'],'ERR3010147':['Scer','Spar'],'ERR3010149':['Scer','Spar'],'ERR3010150':['Scer','Spar'],'SRR8799642':['Scer','Seub'],'SRR8799680':['Scer','Seub'],'SRR8799685':['Scer','Seub'],'SRR8799646':['Scer','Seub'],'SRR8799652':['Scer','Seub'],'SRR8799686':['Scer','Seub'],'SRR8799647':['Scer','Seub'],'SRR8799692':['Scer','Seub'],'SRR8799694':['Scer','Seub'],'SRR8799654':['Scer','Seub'],'SRR8799683':['Scer','Seub'],'SRR8799682':['Scer','Seub'],'SRR8799666':['Scer','Seub'],'SRR8799653':['Scer','Seub'],'SRR8799631':['Scer','Seub'],'SRR8799695':['Scer','Seub'],'SRR8799649':['Scer','Seub'],'SRR8799645':['Scer','Seub'],'SRR8799681':['Scer','Seub'],'SRR8799684':['Scer','Seub'],'SRR8799676':['Scer','Seub'],'SRR8799687':['Scer','Seub'],'SRR8799691':['Scer','Seub'],'SRR8799693':['Scer','Seub'],'SRR8799659':['Scer','Seub'],'SRR8799643':['Scer','Seub'],'SRR8799644':['Scer','Seub'],'SRR8799662':['Scer','Seub'],'SRR8799663':['Scer','Seub'],'SRR8799634':['Scer','Seub'],'SRR8799640':['Scer','Seub'],'SRR8799671':['Scer','Seub'],'SRR8799667':['Scer','Seub'],'SRR8799637':['Scer','Seub'],'SRR8799690':['Scer','Seub'],'SRR8799673':['Scer','Skud'],'SRR8799672':['Scer','Skud'],'SRR8799674':['Scer','Skud'],'SRR8799650':['Scer','Skud'],'SRR8799633':['Scer','Skud'],'SRR8799677':['Scer','Skud'],'SRR8799678':['Scer','Skud'],'SRR8799651':['Scer','Skud'],'SRR8799679':['Scer','Skud'],'SRR8799657':['Suva','Seub'],'SRR8799661':['Suva','Seub'],'SRR8799665':['Suva','Seub'],'SRR8799698':['Suva','Seub'],'SRR8799660':['Suva','Seub'],'SRR8799658':['Suva','Seub'],'SRR8799655':['Suva','Seub'],'SRR8799668':['Suva','Seub'],'SRR8799641':['Suva','Seub'],'SRR8799656':['Suva','Seub'],'SRR8799630':['Suva','Seub'],'SRR8799629':['Suva','Seub'],'SRR8799664':['Suva','Seub'],'SRR10035841':['Scer','Skud'],'SRR10047007':['Scer','Skud'],'SRR10047062':['Scer','Seub'],'SRR10047068':['Scer','Seub'],'SRR10047077':['Scer','Seub'],'SRR10047225':['Scer','Seub'],'SRR10047243':['Scer','Seub','Suva'],'SRR10047261':['Scer','Seub'],'SRR10047170':['Scer','Seub'],'SRR10047181':['Scer','Seub'],'SRR10047231':['Scer','Skud'],'SRR10047188':['Scer','Seub'],'SRR10047256':['Scer','Seub'],'SRR10047124':['Scer','Seub'],'SRR10047019':['Seub','Suva'],'SRR10047000':['Scer','Seub'],'SRR10047191':['Scer','Seub'],'SRR10047313':['Scer','Seub'],'SRR10047163':['Scer','Seub'],'SRR10047100':['Scer','Seub'],'SRR10047089':['Scer','Seub'],'SRR10047028':['Scer','Skud'],'SRR10047024':['Scer','Seub'],'SRR10047374':['Scer','Seub'],'SRR10047307':['Scer','Seub'],'SRR10047017':['Scer','Seub'],'SRR10047241':['Scer','Seub'],'SRR10047295':['Seub','Suva'],'SRR10047331':['Seub','Suva'],'SRR10047328':['Seub','Suva'],'SRR10047359':['Seub','Suva'],'SRR10047351':['Seub','Suva'],'SRR10047335':['Seub','Suva'],'SRR10035842':['Seub','Suva'],'SRR10047405':['Seub','Suva'],'SRR10047402':['Seub','Suva'],'SRR10047389':['Seub','Suva'],'SRR10047380':['Seub','Suva'],'SRR10047371':['Seub','Suva'],'SRR10047378':['Seub','Suva'],'SRR10047356':['Scer','Seub','Skud','Suva'],'SRR10047345':['Seub','Suva'],'SRR10047410':['Scer','Seub','Skud','Suva'],'SRR10047383':['Scer','Seub','Skud','Suva'],'SRR10047395':['Seub','Suva'],'SRR10047344':['Seub','Suva'],'SRR10047362':['Scer','Seub'],'SRR10047189':['Scer','Seub'],'SRR10047157':['Scer','Seub'],'SRR10047176':['Scer','Seub'],'SRR10047149':['Scer','Seub'],'SRR10047146':['Scer','Seub'],'SRR10047122':['Scer','Skud'],'SRR10047102':['Scer','Seub'],'SRR10047096':['Scer','Seub'],'SRR10047094':['Scer','Seub'],'SRR10047057':['Scer','Seub'],'SRR10046945':['Scer','Seub'],'SRR10047365':['Scer','Seub','Suva'],'SRR10046950':['Seub','Suva'],'SRR10046952':['Seub','Suva'],'SRR10046972':['Scer','Skud'],'SRR10046976':['Scer','Seub'],'SRR5141258':['Scer','Seub'],'SRR5141260':['Scer','Seub'],'SRR12703370':['Scer','Seub'],'SRR12703369':['Scer','Seub'],'SRR12703368':['Scer','Seub'],'SRR12703367':['Scer','Seub'],'SRR12703366':['Scer','Seub'],'SRR5251559':['Scer','Suva'],'SRR10208681':['Scer','Seub'],'SRR9925228':['Scer','Skud'],'SRR9925227':['Scer','Skud'],'SRR9925226':['Scer','Skud'],'SRR9925224':['Scer','Skud'],'SRR9925223':['Scer','Skud'],'SRR9925222':['Scer','Skud'],'SRR9925225':['Scer','Skud']}
for i in map_filt:
    if len(hybrid_sp[i])==2 and i in map_filt:
        if 'Scer' in hybrid_sp[i] and 'Spar' in hybrid_sp[i]:
            s1='Scer'
            s2='Spar'
            if data_mapped.loc[i,s1]>data_mapped.loc[i,s2]:
                sp1.append(data_mapped.loc[i,s1])
                sp1_dict[i]=data_mapped.loc[i,s1]
                sp2.append(data_mapped.loc[i,s2])
                sp_diff[i]=data_mapped.loc[i,s1]-data_mapped.loc[i,s2]
            else:
                sp2.append(data_mapped.loc[i,s1])
                sp1.append(data_mapped.loc[i,s2])
                sp1_dict[i]=data_mapped.loc[i,s2]
                sp_diff[i]=data_mapped.loc[i,s2]-data_mapped.loc[i,s1]

        elif 'Scer' in hybrid_sp[i] and 'Smik' in hybrid_sp[i]:
            s1='Scer'
            s2='Smik'
            if data_mapped.loc[i,s1]>data_mapped.loc[i,s2]:
                sp1.append(data_mapped.loc[i,s1])
                sp1_dict[i]=data_mapped.loc[i,s1]
                sp2.append(data_mapped.loc[i,s2])
                sp_diff[i]=data_mapped.loc[i,s1]-data_mapped.loc[i,s2]
            else:
                sp2.append(data_mapped.loc[i,s1])
                sp1.append(data_mapped.loc[i,s2])
                sp1_dict[i]=data_mapped.loc[i,s2]
                sp_diff[i]=data_mapped.loc[i,s2]-data_mapped.loc[i,s1]
        elif 'Scer' in hybrid_sp[i] and 'Skud' in hybrid_sp[i]:
            s1='Scer'
            s2='Skud'
            if data_mapped.loc[i,s1]>data_mapped.loc[i,s2]:
                sp1.append(data_mapped.loc[i,s1])
                sp1_dict[i]=data_mapped.loc[i,s1]
                sp2.append(data_mapped.loc[i,s2])
                sp_diff[i]=data_mapped.loc[i,s1]-data_mapped.loc[i,s2]
            else:
                sp2.append(data_mapped.loc[i,s1])
                sp1.append(data_mapped.loc[i,s2])
                sp1_dict[i]=data_mapped.loc[i,s2]
                sp_diff[i]=data_mapped.loc[i,s2]-data_mapped.loc[i,s1]
        elif 'Scer' in hybrid_sp[i] and 'Seub' in hybrid_sp[i]:
            s1='Scer'
            s2='Seub'
            if data_mapped.loc[i,s1]>data_mapped.loc[i,s2]:
                sp1.append(data_mapped.loc[i,s1])
                sp1_dict[i]=data_mapped.loc[i,s1]
                sp2.append(data_mapped.loc[i,s2])
                sp_diff[i]=data_mapped.loc[i,s1]-data_mapped.loc[i,s2]
            else:
                sp2.append(data_mapped.loc[i,s1])
                sp1.append(data_mapped.loc[i,s2])
                sp1_dict[i]=data_mapped.loc[i,s2]
                sp_diff[i]=data_mapped.loc[i,s2]-data_mapped.loc[i,s1]
        elif 'Scer' in hybrid_sp[i] and 'Suva' in hybrid_sp[i]:
            s1='Scer'
            s2='Suva'
            if data_mapped.loc[i,s1]>data_mapped.loc[i,s2]:
                sp1.append(data_mapped.loc[i,s1])
                sp1_dict[i]=data_mapped.loc[i,s1]
                sp2.append(data_mapped.loc[i,s2])
                sp_diff[i]=data_mapped.loc[i,s1]-data_mapped.loc[i,s2]
            else:
                sp2.append(data_mapped.loc[i,s1])
                sp1.append(data_mapped.loc[i,s2])
                sp1_dict[i]=data_mapped.loc[i,s2]
                sp_diff[i]=data_mapped.loc[i,s2]-data_mapped.loc[i,s1]
        elif 'Seub' in hybrid_sp[i] and 'Suva' in hybrid_sp[i]:
            s1='Seub'
            s2='Suva'
            if data_mapped.loc[i,s1]>data_mapped.loc[i,s2]:
                sp1.append(data_mapped.loc[i,s1])
                sp1_dict[i]=data_mapped.loc[i,s1]
                sp2.append(data_mapped.loc[i,s2])
                sp_diff[i]=data_mapped.loc[i,s1]-data_mapped.loc[i,s2]
            else:
                sp2.append(data_mapped.loc[i,s1])
                sp1.append(data_mapped.loc[i,s2])
                sp1_dict[i]=data_mapped.loc[i,s2]
                sp_diff[i]=data_mapped.loc[i,s2]-data_mapped.loc[i,s1]
#%%
var=[]
for i in map_filt:
    if len(hybrid_sp[i])==2:
        var.append(((depth_var[i]))*6)
        

gain=[]
for i in map_filt:
    if len(hybrid_sp[i])==2:
        gain.append(((chro_gain[i])*20)+1)
        
loss=[]
for i in map_filt:
    if len(hybrid_sp[i])==2:
        loss.append(((chro_loss[i])*20)+1)

plt.figure(figsize=[12,5])
plt.subplot(133)
plt.scatter(sp2,sp1,c=var,cmap='binary',edgecolor='black',alpha=1,s=var)
plt.xlabel('species 2 (%)',weight='bold')
plt.ylabel('species 1 (%)',weight='bold')
plt.ylim(40,100)
plt.xticks([0,10,20,30,40,50])
plt.subplot(131)
plt.scatter(sp2,sp1,c=gain,cmap='Reds',edgecolor='black',alpha=1,s=gain)
plt.xlabel('species 2 (%)',weight='bold')
plt.ylabel('species 1 (%)',weight='bold')
plt.ylim(40,100)
plt.xticks([0,10,20,30,40,50])
plt.subplot(132)
plt.scatter(sp2,sp1,c=loss,cmap='Blues',edgecolor='black',alpha=1,s=loss)
plt.xlabel('species 2 (%)',weight='bold')
plt.ylabel('species 1 (%)',weight='bold')
plt.ylim(40,100)
plt.xticks([0,10,20,30,40,50])
sns.despine(trim=True)
plt.tight_layout(w_pad=3.5)

plt.savefig('../Figures/Hybrid_GI_S1vsS2_var.svg',bbox_inches='tight',dpi=1000,transparent=True)

#%%
plt.figure(figsize=[12,3])
plt.subplot(131)
x=[]
y=[]
for i in sp_diff:
    x.append(chro_gain[i])
    y.append(sp_diff[i])
plt.scatter(y,x,c=x,cmap='Reds',edgecolor='black',s=gain)
plt.yticks([0,1,2,3,4,5,6,7])
plt.xlabel('difference in genomic\nspecies contributions (%)',weight='bold')
plt.ylabel('chromosome gain (n)',weight='bold')
plt.xlim(-5,100)
plt.subplot(132)
x=[]
y=[]
for i in sp_diff:
    x.append(chro_loss[i])
    y.append(sp_diff[i])
plt.scatter(y,x,c=x,cmap='Blues',edgecolor='black',s=loss)

plt.yticks([0,1,2,3,4,5,6,7])
plt.xlabel('difference in genomic\nspecies contributions (%)',weight='bold')
plt.ylabel('chromosome loss (n)',weight='bold')
plt.xlim(-5,100)
plt.subplot(133)
x=[]
y=[]
for i in sp_diff:
    x.append(depth_var[i])
    y.append(sp_diff[i])
plt.scatter(y,x,c=x,cmap='binary',edgecolor='black',s=var)

plt.xlabel('difference in genomic\nspecies contributions (%)',weight='bold')
plt.ylabel('read depth variance',weight='bold')
plt.xlim(-5,100)
plt.ylim(-2,47)
sns.despine(trim=True)
plt.tight_layout(w_pad=3.5)
plt.savefig('../Figures/Hybrid_GI_sp_diff.svg',bbox_inches='tight',dpi=1000,transparent=True)


#%%
plt.figure(figsize=[0.5,3])
plt.scatter([1,1,1,1],[1,2,3,4],s=[1,80,160,240],edgecolor='black',color='grey')
plt.ylim(0,5)
plt.savefig('../Figures/Hybrid_GI_sp_diff_size.svg',bbox_inches='tight',dpi=1000,transparent=True)
#%%
heatmap=pd.DataFrame(index=range(40,101),columns=['mean'])
heatmap.sort_index(ascending=False,inplace=True)
heatmap=heatmap.astype(float)

for i in range(40,101):
    heatmap.loc[i,'mean']=0
    x=[]
    for j in sp1_dict:
        if round(sp1_dict[j])==i:
            x.append(depth_var[j])
    heatmap.loc[i,'mean']=np.mean(x)

plt.figure(figsize=[0.5,5])
sns.heatmap(data=heatmap,cmap='binary',cbar=False)
plt.yticks([],[])
#plt.ylim(30,100)
plt.xticks([])
plt.hlines([0,60],xmin=0,xmax=1,color='black')
plt.vlines([0.0,1],ymin=60,ymax=0,color='black')
plt.ylim(60.5,-0.5)
plt.xlim(-0.05,1.05)
plt.savefig('../Figures/Hybrid_GI_S1vsS2_var_y.svg',bbox_inches='tight',dpi=1000,transparent=True)
#%%
heatmap=pd.DataFrame(index=range(40,101),columns=['mean'])
heatmap.sort_index(ascending=False,inplace=True)
heatmap=heatmap.astype(float)

for i in range(40,101):
    heatmap.loc[i,'mean']=0
    x=[]
    for j in sp1_dict:
        if round(sp1_dict[j])==i:
            x.append(chro_gain[j])
    heatmap.loc[i,'mean']=np.mean(x)

plt.figure(figsize=[0.5,5])
sns.heatmap(data=heatmap,cmap='Reds',cbar=False)
plt.yticks([],[])
#plt.ylim(30,100)
plt.xticks([])
plt.hlines([0,60],xmin=0,xmax=1,color='black')
plt.vlines([0.0,1],ymin=60,ymax=0,color='black')
plt.ylim(60.5,-0.5)
plt.xlim(-0.05,1.05)
plt.savefig('../Figures/Hybrid_GI_S1vsS2_gain_y.svg',bbox_inches='tight',dpi=1000,transparent=True)
#%%
heatmap=pd.DataFrame(index=range(40,101),columns=['mean'])
heatmap.sort_index(ascending=False,inplace=True)
heatmap=heatmap.astype(float)


for i in range(40,101):
    heatmap.loc[i,'mean']=0
    x=[]
    for j in sp1_dict:
        if round(sp1_dict[j])==i:
            x.append(chro_loss[j])
    heatmap.loc[i,'mean']=np.mean(x)

plt.figure(figsize=[0.5,5])
sns.heatmap(data=heatmap,cmap='Blues',cbar=False)
plt.yticks([],[])
#plt.ylim(30,100)
plt.xticks([])
plt.hlines([0,60],xmin=0,xmax=1,color='black')
plt.vlines([0.0,1],ymin=60,ymax=0,color='black')
plt.ylim(60.5,-0.5)
plt.xlim(-0.05,1.05)
plt.savefig('../Figures/Hybrid_GI_S1vsS2_loss_y.svg',bbox_inches='tight',dpi=1000,transparent=True)

#%%
fig, ax = plt.subplots(figsize=(0.25, 2))
fig.subplots_adjust(bottom=0.5)

cmap = cm.Blues
#plt.yticks([0,1],['min','max'])
plt.yticks([])

cb1 = mpl.colorbar.ColorbarBase(ax, cmap=cmap,orientation='vertical')
plt.savefig('../Figures/Hybrid_GI_read_depth_cbar-var.svg',bbox_inches='tight',dpi=1000,transparent=True)


#%%
plt.figure(figsize=[4,4])
hybrid_genome = pickle.load( open( "../Pickle/hybrid_genome_read.p", "rb" ) )

for i in hybrid_genome:
    plt.scatter(hybrid_genome[i],sp1_dict[i])
    
