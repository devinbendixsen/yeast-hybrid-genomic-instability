#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 15 22:48:20 2021

@author: devin
"""
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import matplotlib.cm as cm
import matplotlib as mpl
import pickle
font = {'family' : 'sans-serif','weight' : 'normal','size' : 12}
plt.rc('font', **font)

#%%import pickle
species=['Scer','Spar','Smik','Sjur','Skud','Sarb','Seub','Suva']
chro=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]

hybrid_depth={}
hybrid_depth_pos={}
hybrid_mean_init={}
for i in species:
    hybrid_depth[i]={}
    hybrid_depth_pos[i]={}
    hybrid_mean_init[i]={}
    for chro in range(1,17):
        hybrid_depth[i][chro]=[]
        hybrid_depth_pos[i][chro]=[]
        hybrid_mean_init[i][chro]=0
#%%
for i in hybrid_mean_init:
    depth=pd.read_csv('../DATA/sppIDer/parents/parent_nuclear_summary/{}_chrAvgDepth-d.txt'.format(i), sep='\t',index_col='chrom')
    for chro in range(1,17):
        for index, row in depth.iterrows():
            if index =='{}-{}'.format(i,chro):
                hybrid_mean_init[i][chro]=row['log2mean']
            
#%%
for i in hybrid_depth:
    data=pd.read_csv('../DATA/sppIDer/parents/parent_nuclear_summary/{}_winAvgDepth-d.txt'.format(i), sep='\t')
    for index, row in data.iterrows():
        if row['species'] == i:
            for chro in range(1,17):
                if row['chrom']=='{}-{}'.format(i,chro):
                    if row['log2mean']<=(1.1*hybrid_mean_init[i][chro]):
                        hybrid_depth_pos[i][chro].append(float(row['Genome_Pos']))
                        hybrid_depth[i][chro].append(2**row['log2mean'])
#%%
norm = cm.colors.Normalize(vmin=0, vmax=4)
titles={'Scer':'S. cerevisiae','Spar':'S. paradoxus','Smik':'S. mikatae','Sjur':'S. jurei','Skud':'S. kudriavzevii','Sarb':'S. arboricola','Seub':'S. eubayanus','Suva':'S. uvarum'}
plt.figure(figsize=[14,12])
n=1
for sample in species:
    plt.subplot(4,2,n)
    xtick=[]
    lines=[]
    for chro in hybrid_depth[sample]:
        plt.scatter(hybrid_depth_pos[sample][chro],np.log2(hybrid_depth[sample][chro]),color='black',s=1)
        plt.hlines(np.log2(np.mean(hybrid_depth[sample][chro])),xmin=min(hybrid_depth_pos[sample][chro]),xmax=max(hybrid_depth_pos[sample][chro]),color=cm.RdBu_r(norm(np.mean(hybrid_depth[sample][chro]))),linewidth=3)
        xtick.append(np.mean(hybrid_depth_pos[sample][chro]))
        lines.append(max(hybrid_depth_pos[sample][chro]))
    lines.append(min(hybrid_depth_pos[sample][1]))
    plt.vlines(lines,ymin=0,ymax=5,linestyle='dashed',zorder=-100,linewidth=1.5,color='grey')
    plt.xlim(min(hybrid_depth_pos[sample][1])-150000,max(hybrid_depth_pos[sample][16])+150000)
    plt.xticks(xtick,range(1,17))
    plt.xlabel('chromosome',weight='bold')
    plt.ylabel('mean read depth (log2)',weight='bold')
    plt.yticks([0,1,2,3,4])
    plt.ylim([0,4])
    n+=1
    plt.title(titles[sample],fontstyle='italic', weight='bold')
plt.tight_layout()


plt.savefig('../Figures/Hybrid_GI_read_depth_parents.png',bbox_inches='tight',dpi=1000) 
