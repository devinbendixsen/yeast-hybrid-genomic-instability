#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 30 10:21:34 2021

@author: devin
"""
import pickle
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from pywaffle import Waffle
font = {'family' : 'sans-serif','weight' : 'normal','size' : 12}
plt.rc('font', **font)

#%%
plt.figure(figsize=[6,6])
# Pie chart

labels = ['beer/ lager\n (131)', 'wine/ cider\n (23)','olive (22)','wild (9)','fruit (6)','unknown (4)', 'other (9)']
sizes = [131, 23, 22, 9, 6, 4, 9]
#colors
colors = ['#ff9999','#66b3ff','#99ff99','#ffcc99']
#explsion
explode = (0.0,0.1,0.2,0.3,0.4,0.5,0.6)
cmap = plt.get_cmap("gist_earth")
colors = cmap([20,50,80,110,140,170,210])
plt.pie(sizes, colors=colors,autopct=None, startangle=40, pctdistance=0.75, explode = explode,labels=labels,rotatelabels=False)
#draw circle
centre_circle = plt.Circle((0,0),0.6,fc='white')
fig = plt.gcf()
fig.gca().add_artist(centre_circle)
# Equal aspect ratio ensures that pie is drawn as a circle
#plt.axis('equal'
plt.text(0,0,s='204 interspecific\n hybrids',horizontalalignment='center',verticalalignment='center',weight='bold')
plt.tight_layout()
plt.savefig('../Figures/Hybrid_GI_hybrid_origin.png',bbox_inches='tight',dpi=1000)

#%%
plt.figure(figsize=[6,6])
# Pie chart
labels = ['Scer x Seub\n (94)', 'Seub x Suva\n(33)','Scer x Skud\n(31)','Scer x Spar (29)','>2 parents (11)','Scer x Suva (5)', 'Scer x Smik (1)']
sizes = [94,33,31,29,11,5,1]
#colors
colors = ['#ff9999','#66b3ff','#99ff99','#ffcc99']
#explsion
explode = (0.0,0.05,0.1,0.15,0.2,0.25,0.3)
explode = (0.0,0.1,0.2,0.3,0.4,0.5,0.6)
cmap = plt.get_cmap("gist_earth")
colors = cmap([20,50,80,110,140,170,210])
plt.pie(sizes, colors=colors,autopct=None, startangle=40, pctdistance=0.75, explode = explode,labels=labels,rotatelabels=False)
#draw circle
centre_circle = plt.Circle((0,0),0.6,fc='white')
fig = plt.gcf()
fig.gca().add_artist(centre_circle)
# Equal aspect ratio ensures that pie is drawn as a circle
#plt.axis('equal'
plt.text(0,0,s='204 interspecific\n hybrids',horizontalalignment='center',verticalalignment='center',weight='bold')
plt.tight_layout()
plt.savefig('../Figures/Hybrid_GI_hybrid_type.png',bbox_inches='tight',dpi=1000)

#%%
fig = plt.figure(
    FigureClass=Waffle, 
    rows=12, 
    columns=17, 
    values=[29, 1, 31,94,5,33,11],
    colors=['#3D405B','white','#81B29A','#E07A5F','#F2CC8F','peru','grey'],
    figsize=(9, 3)
)
fig.set_facecolor('#EEEEEE')

plt.savefig('../Figures/Hybrid_GI_hybrid_waffle.png',bbox_inches='tight',dpi=1000)

#%%
plt.figure(figsize=[3,3])
# Pie chart
labels = ['olive\n(22)', 'wild (3)','wine (2)','clinical (2)']
sizes = [22,3,2,2]
#colors
#colors = ['#ff9999','#66b3ff','#99ff99','#ffcc99']
#explsion
explode = (0.0,0.05,0.1,0.15)
cmap = plt.get_cmap("gray_r")
colors = cmap([20,50,80,110,140,170,210])
plt.pie(sizes, colors=colors,autopct=None, startangle=40, pctdistance=0.75, explode = explode,labels=labels,rotatelabels=False,shadow=True)
#draw circle
centre_circle = plt.Circle((0,0),0.6,fc='white')
fig = plt.gcf()
fig.gca().add_artist(centre_circle)
# Equal aspect ratio ensures that pie is drawn as a circle
#plt.axis('equal'
plt.text(0,0,s='29',horizontalalignment='center',verticalalignment='center',weight='bold')
plt.tight_layout()
plt.savefig('../Figures/Hybrid_GI_hybrid_type_ScxSp.svg',bbox_inches='tight',dpi=1000,transparent=True)

#%%
plt.figure(figsize=[3,3])
# Pie chart
labels = ['wine (1)']
sizes = [1]
#colors
#colors = ['#ff9999','#66b3ff','#99ff99','#ffcc99']
#explsion
#explode = (0.0)
cmap = plt.get_cmap("gray_r")
colors = cmap([20,50,80,110,140,170,210])
plt.pie(sizes, colors=colors,autopct=None, startangle=40, pctdistance=0.75,labels=labels,rotatelabels=False)
#draw circle
centre_circle = plt.Circle((0,0),0.6,fc='white')
fig = plt.gcf()
fig.gca().add_artist(centre_circle)
# Equal aspect ratio ensures that pie is drawn as a circle
#plt.axis('equal'
plt.text(0,0,s='1',horizontalalignment='center',verticalalignment='center',weight='bold')
plt.tight_layout()
plt.savefig('../Figures/Hybrid_GI_hybrid_type_ScxSm.svg',bbox_inches='tight',dpi=1000,transparent=True)


#%%
plt.figure(figsize=[3,3])
# Pie chart
labels = ['wine (14)','beer (13)','baking (2)','clinical (1)','dietary (1)']
sizes = [14,13,2,1,1]
#colors
#colors = ['#ff9999','#66b3ff','#99ff99','#ffcc99']
#explsion
#explode = (0.0)
cmap = plt.get_cmap("gray_r")
colors = cmap([20,50,80,110,140,170,210])
explode = (0.0,0.05,0.1,0.15,0.2)
plt.pie(sizes, colors=colors,autopct=None, startangle=40, pctdistance=0.75,explode = explode, labels=labels,rotatelabels=False)
#draw circle
centre_circle = plt.Circle((0,0),0.6,fc='white')
fig = plt.gcf()
fig.gca().add_artist(centre_circle)
# Equal aspect ratio ensures that pie is drawn as a circle
#plt.axis('equal'
plt.text(0,0,s='31',horizontalalignment='center',verticalalignment='center',weight='bold')
plt.tight_layout()
plt.savefig('../Figures/Hybrid_GI_hybrid_type_ScxSk.svg',bbox_inches='tight',dpi=1000,transparent=True)

#%%
plt.figure(figsize=[3,3])
# Pie chart
labels = ['beer/ lager\n(90)','wine (1)','fruit (1)','whiskey (1)','laboratory (1)']
sizes = [90,1,1,1,1]
#colors
#colors = ['#ff9999','#66b3ff','#99ff99','#ffcc99']
#explsion
#explode = (0.0)
cmap = plt.get_cmap("gray_r")
colors = cmap([20,50,80,110,140,170,210])
explode = (0.0,0.05,0.1,0.15,0.2)
plt.pie(sizes, colors=colors,autopct=None, startangle=40, pctdistance=0.75,explode = explode, labels=labels,rotatelabels=False)
#draw circle
centre_circle = plt.Circle((0,0),0.6,fc='white')
fig = plt.gcf()
fig.gca().add_artist(centre_circle)
# Equal aspect ratio ensures that pie is drawn as a circle
#plt.axis('equal'
plt.text(0,0,s='94',horizontalalignment='center',verticalalignment='center',weight='bold')
plt.tight_layout()
plt.savefig('../Figures/Hybrid_GI_hybrid_type_ScxSe.svg',bbox_inches='tight',dpi=1000,transparent=True)

#%%
plt.figure(figsize=[3,3])
# Pie chart
labels = ['wine (3)','beer (1)','laboratory (1)']
sizes = [3,1,1]
#colors
#colors = ['#ff9999','#66b3ff','#99ff99','#ffcc99']
#explsion
#explode = (0.0)
cmap = plt.get_cmap("gray_r")
colors = cmap([20,50,80,110,140,170,210])
explode = (0.0,0.05,0.1)
plt.pie(sizes, colors=colors,autopct=None, startangle=40, pctdistance=0.75,explode = explode, labels=labels,rotatelabels=False)
#draw circle
centre_circle = plt.Circle((0,0),0.6,fc='white')
fig = plt.gcf()
fig.gca().add_artist(centre_circle)
# Equal aspect ratio ensures that pie is drawn as a circle
#plt.axis('equal'
plt.text(0,0,s='5',horizontalalignment='center',verticalalignment='center',weight='bold')
plt.tight_layout()
plt.savefig('../Figures/Hybrid_GI_hybrid_type_ScxSu.svg',bbox_inches='tight',dpi=1000,transparent=True)

#%%

plt.figure(figsize=[3,3])
# Pie chart
labels = ['beer (20)','wild (5)','fruit (4)','unknown (4)']
sizes = [20,5,4,4]
#colors
#colors = ['#ff9999','#66b3ff','#99ff99','#ffcc99']
#explsion
#explode = (0.0)
cmap = plt.get_cmap("gray_r")
colors = cmap([20,50,80,110,140,170,210])
explode = (0.0,0.05,0.1,0.15)
plt.pie(sizes, colors=colors,autopct=None, startangle=40, pctdistance=0.75,explode = explode, labels=labels,rotatelabels=False)
#draw circle
centre_circle = plt.Circle((0,0),0.6,fc='white')
fig = plt.gcf()
fig.gca().add_artist(centre_circle)
# Equal aspect ratio ensures that pie is drawn as a circle
#plt.axis('equal'
plt.text(0,0,s='33',horizontalalignment='center',verticalalignment='center',weight='bold')
plt.tight_layout()
plt.savefig('../Figures/Hybrid_GI_hybrid_type_SexSu.svg',bbox_inches='tight',dpi=1000,transparent=True)

#%%
plt.figure(figsize=[3,3])
# Pie chart
labels = ['beer/ lager\n(7)','wine (2)','fruit (1)','wild (1)']
sizes = [7,2,1,1]
#colors
#colors = ['#ff9999','#66b3ff','#99ff99','#ffcc99']
#explsion
#explode = (0.0)
cmap = plt.get_cmap("gray_r")
colors = cmap([20,50,80,110,140,170,210])
explode = (0.0,0.05,0.1,0.15)
plt.pie(sizes, colors=colors,autopct=None, startangle=40, pctdistance=0.75,explode = explode, labels=labels,rotatelabels=False)
#draw circle
centre_circle = plt.Circle((0,0),0.6,fc='white')
fig = plt.gcf()
fig.gca().add_artist(centre_circle)
# Equal aspect ratio ensures that pie is drawn as a circle
#plt.axis('equal'
plt.text(0,0,s='11',horizontalalignment='center',verticalalignment='center',weight='bold')
plt.tight_layout()
plt.savefig('../Figures/Hybrid_GI_hybrid_type_2.svg',bbox_inches='tight',dpi=1000,transparent=True)

#%%
plt.figure(figsize=[3,3])
# Pie chart
labels = ['beer/ lager\n (131)', 'wine (23)','olive (22)','wild (9)','fruit (6)','unknown (4)', 'clinical (3)','baking (2)','labatory (2)','dietary (1)','whiskey (1)']
sizes = [131,23,22,9,6,4,3,2,2,1,1]
#colors
#colors = ['#ff9999','#66b3ff','#99ff99','#ffcc99']
#explsion
#explode = (0.0)
cmap = plt.get_cmap("gray_r")
colors = cmap([20,50,80,110,140,170,210])
explode = (0.0,0.05,0.1,0.15,0.20,0.25,0.3, 0.35,0.4,0.45,0.5)
plt.pie(sizes, colors=colors,autopct=None, startangle=40, pctdistance=0.75,explode = explode, labels=labels,rotatelabels=False)
#draw circle
centre_circle = plt.Circle((0,0),0.6,fc='white')
fig = plt.gcf()
fig.gca().add_artist(centre_circle)
# Equal aspect ratio ensures that pie is drawn as a circle
#plt.axis('equal'
plt.text(0,0,s='204',horizontalalignment='center',verticalalignment='center',weight='bold')
plt.tight_layout()
plt.savefig('../Figures/Hybrid_GI_hybrid_type_total.svg',bbox_inches='tight',dpi=1000,transparent=True)