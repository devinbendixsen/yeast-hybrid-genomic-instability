#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 30 16:46:11 2021

@author: devin
"""
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pickle
import scipy.stats
font = {'family' : 'sans-serif','weight' : 'normal','size' : 12}
plt.rc('font', **font)

#%%
cmap = sns.diverging_palette(220,20, sep=1,as_cmap=True)
plt.figure(figsize=[8,8])
genomes=['ERR1111503','ERR1111502','SRR2967902','ERR3010130','SRR2967903','SRR800807','SRR800768','ERR3010131','ERR3010132','ERR3010133','ERR3010134','ERR3010135','ERR3010136','ERR3010137','ERR3010138','ERR3010139','ERR3010140','ERR3010143','SRR800854','ERR3010122','ERR3010128','ERR3010129','ERR3010125','ERR3010126','ERR3010127','ERR3010146','ERR3010147','ERR3010149','ERR3010150','SRR2967905','SRR8799672','SRR10047028','SRR2968005','SRR8799673','SRR8799674','SRR8799678','SRR10035841','SRR2967904','SRR2967849','SRR2967853','SRR2967864','SRR2968024','SRR2967875','SRR2968042','SRR3265414','SRR8799650','SRR8799633','SRR8799677','SRR8799651','SRR8799679','SRR9925228','SRR9925227','SRR9925226','SRR9925224','SRR9925223','SRR9925222','SRR9925225','SRR10047154','DRR040657','DRR040645','DRR040654','SRR8648841','SRR10047092','DRR040649','DRR040656','DRR040637','DRR040641','DRR040658','DRR040650','DRR040660','SRR2968046','SRR8648839','SRR1649181','SRR1649183','SRR10047152','SRR8799642','SRR8799680','SRR8799685','SRR8799646','SRR8799652','SRR8799686','SRR8799647','SRR8799692','SRR8799694','SRR8799654','SRR8799683','SRR8799682','SRR8799666','SRR8799653','SRR8799631','SRR8799695','SRR8799649','SRR8799645','SRR8799681','SRR8799684','SRR8799676','SRR8799687','SRR8799691','SRR8799693','SRR8799659','SRR8799643','SRR8799644','SRR8799662','SRR8799663','SRR8799634','SRR8799640','SRR8799671','SRR8799667','SRR8799637','SRR8799690','SRR10047062','SRR10047068','SRR10047077','SRR10047225','SRR10047261','SRR10047170','SRR10047181','SRR10047188','SRR10047256','SRR10047124','SRR10047000','SRR10047191','SRR10047313','SRR10047163','SRR10047100','SRR10047089','SRR10047024','SRR10047374','SRR10047307','SRR10047017','SRR10047241','SRR10047362','SRR10047189','SRR10047157','SRR10047176','SRR10047149','SRR10047146','SRR10047102','SRR10047096','SRR10047094','SRR10047057','SRR10046945','SRR10046976','SRR5141258','SRR5141260','SRR12703370','SRR12703369','SRR12703368','SRR12703367','SRR12703366','SRR10208681','SRR2967882','SRR2967906','SRR5251559','SRR1119203','SRR10047416','DRR040667','DRR040669','SRR8799657','SRR8799661','SRR8799665','SRR8799698','SRR8799660','SRR8799658','SRR8799655','SRR8799668','SRR8799641','SRR8799656','SRR8799630','SRR8799629','SRR8799664','SRR10047019','SRR10047295','SRR10047331','SRR10047328','SRR10047359','SRR10047351','SRR10047335','SRR10035842','SRR10047405','SRR10047402','SRR10047389','SRR10047380','SRR10047371','SRR10047378','SRR10047345','SRR10047395','SRR10047344','SRR10046950','SRR10046952','SRR1119204','SRR10047336','SRR10047231','SRR10047356','SRR10047007','SRR10047410','SRR10047383','SRR10046972','SRR10047243','SRR10047365','SRR1119201','SRR7631525','SRR1119202','SRR10047122']

#data = pd.read_csv("../DATA/AAF_DATA/AAF_SvsL_20_7.xlsx",index_col=0,header=None,delim_whitespace=True,skiprows=(1))
data = pd.read_csv("../DATA/AAF/AAF_17_2_nshare.csv",index_col=0)
mask=np.triu(data)
sns.heatmap(data,mask=mask,square=True,cmap=cmap,cbar=True,linecolor='white',linewidths=0,xticklabels=data.index.values.astype(str),cbar_kws={"shrink": 0.6,"pad":-.1,"aspect":20})
plt.ylabel('hybrid',weight='bold')
plt.xlabel('hybrid',weight='bold')
plt.xticks([])
plt.yticks([])
plt.savefig('../Figures/Hybrid_GI_AAF_heatmap.png',bbox_inches='tight',dpi=1000)

#%%
hybrid_sp={'SRR1119201':['Suva','Scer','Skud'],'SRR1119202':['Suva','Scer','Skud'],'SRR1119203':['Suva','Scer'],'SRR1119204':['Scer','Suva','Seub'],'SRR1649181':['Scer','Seub'],'SRR1649183':['Scer','Seub'],'SRR800854':['Scer','Spar'],'SRR800807':['Scer','Spar'],'SRR800768':['Scer','Spar'],'SRR2967902':['Scer','Spar'],'SRR2967903':['Scer','Spar'],'SRR2967904':['Scer','Skud'],'SRR2967905':['Scer','Smik'],'SRR2967906':['Scer','Suva'],'SRR2967849':['Scer','Skud'],'SRR2967853':['Scer','Skud'],'SRR2967864':['Scer','Skud'],'DRR040649':['Scer','Seub'],'DRR040656':['Scer','Seub'],'DRR040657':['Scer','Seub'],'DRR040637':['Scer','Seub'],'DRR040641':['Scer','Seub'],'DRR040645':['Scer','Seub'],'DRR040658':['Scer','Seub'],'DRR040650':['Scer','Seub'],'DRR040669':['Scer','Suva','Seub'],'DRR040660':['Scer','Seub'],'SRR2968024':['Scer','Skud'],'DRR040667':['Scer','Suva','Seub'],'SRR2968005':['Scer','Skud'],'SRR2967882':['Scer','Suva'],'ERR1111502':['Scer','Spar'],'ERR1111503':['Scer','Spar'],'SRR2967875':['Scer','Skud'],'DRR040654':['Scer','Seub'],'SRR2968042':['Scer','Skud'],'SRR2968046':['Scer','Seub'],'SRR3265414':['Scer','Skud'],'SRR7631525':['Scer','Suva'],'ERR3010122':['Scer','Spar'],'ERR3010128':['Scer','Spar'],'ERR3010129':['Scer','Spar'],'ERR3010125':['Scer','Spar'],'ERR3010126':['Scer','Spar'],'ERR3010127':['Scer','Spar'],'SRR8648839':['Scer','Seub'],'ERR3010130':['Scer','Spar'],'SRR8648841':['Scer','Seub'],'ERR3010131':['Scer','Spar'],'ERR3010132':['Scer','Spar'],'ERR3010133':['Scer','Spar'],'ERR3010134':['Scer','Spar'],'ERR3010135':['Scer','Spar'],'ERR3010136':['Scer','Spar'],'ERR3010137':['Scer','Spar'],'ERR3010138':['Scer','Spar'],'ERR3010139':['Scer','Spar'],'ERR3010140':['Scer','Spar'],'ERR3010143':['Scer','Spar'],'SRR10047154':['Scer','Seub'],'SRR10047336':['Scer','Seub','Skud'],'SRR10047416':['Seub','Suva'],'SRR10047092':['Scer','Seub'],'SRR10047152':['Scer','Seub'],'ERR3010146':['Scer','Spar'],'ERR3010147':['Scer','Spar'],'ERR3010149':['Scer','Spar'],'ERR3010150':['Scer','Spar'],'SRR8799642':['Scer','Seub'],'SRR8799680':['Scer','Seub'],'SRR8799685':['Scer','Seub'],'SRR8799646':['Scer','Seub'],'SRR8799652':['Scer','Seub'],'SRR8799686':['Scer','Seub'],'SRR8799647':['Scer','Seub'],'SRR8799692':['Scer','Seub'],'SRR8799694':['Scer','Seub'],'SRR8799654':['Scer','Seub'],'SRR8799683':['Scer','Seub'],'SRR8799682':['Scer','Seub'],'SRR8799666':['Scer','Seub'],'SRR8799653':['Scer','Seub'],'SRR8799631':['Scer','Seub'],'SRR8799695':['Scer','Seub'],'SRR8799649':['Scer','Seub'],'SRR8799645':['Scer','Seub'],'SRR8799681':['Scer','Seub'],'SRR8799684':['Scer','Seub'],'SRR8799676':['Scer','Seub'],'SRR8799687':['Scer','Seub'],'SRR8799691':['Scer','Seub'],'SRR8799693':['Scer','Seub'],'SRR8799659':['Scer','Seub'],'SRR8799643':['Scer','Seub'],'SRR8799644':['Scer','Seub'],'SRR8799662':['Scer','Seub'],'SRR8799663':['Scer','Seub'],'SRR8799634':['Scer','Seub'],'SRR8799640':['Scer','Seub'],'SRR8799671':['Scer','Seub'],'SRR8799667':['Scer','Seub'],'SRR8799637':['Scer','Seub'],'SRR8799690':['Scer','Seub'],'SRR8799673':['Scer','Skud'],'SRR8799672':['Scer','Skud'],'SRR8799674':['Scer','Skud'],'SRR8799650':['Scer','Skud'],'SRR8799633':['Scer','Skud'],'SRR8799677':['Scer','Skud'],'SRR8799678':['Scer','Skud'],'SRR8799651':['Scer','Skud'],'SRR8799679':['Scer','Skud'],'SRR8799657':['Suva','Seub'],'SRR8799661':['Suva','Seub'],'SRR8799665':['Suva','Seub'],'SRR8799698':['Suva','Seub'],'SRR8799660':['Suva','Seub'],'SRR8799658':['Suva','Seub'],'SRR8799655':['Suva','Seub'],'SRR8799668':['Suva','Seub'],'SRR8799641':['Suva','Seub'],'SRR8799656':['Suva','Seub'],'SRR8799630':['Suva','Seub'],'SRR8799629':['Suva','Seub'],'SRR8799664':['Suva','Seub'],'SRR10035841':['Scer','Skud'],'SRR10047007':['Scer','Skud'],'SRR10047062':['Scer','Seub'],'SRR10047068':['Scer','Seub'],'SRR10047077':['Scer','Seub'],'SRR10047225':['Scer','Seub'],'SRR10047243':['Scer','Seub','Suva'],'SRR10047261':['Scer','Seub'],'SRR10047170':['Scer','Seub'],'SRR10047181':['Scer','Seub'],'SRR10047231':['Scer','Skud'],'SRR10047188':['Scer','Seub'],'SRR10047256':['Scer','Seub'],'SRR10047124':['Scer','Seub'],'SRR10047019':['Seub','Suva'],'SRR10047000':['Scer','Seub'],'SRR10047191':['Scer','Seub'],'SRR10047313':['Scer','Seub'],'SRR10047163':['Scer','Seub'],'SRR10047100':['Scer','Seub'],'SRR10047089':['Scer','Seub'],'SRR10047028':['Scer','Skud'],'SRR10047024':['Scer','Seub'],'SRR10047374':['Scer','Seub'],'SRR10047307':['Scer','Seub'],'SRR10047017':['Scer','Seub'],'SRR10047241':['Scer','Seub'],'SRR10047295':['Seub','Suva'],'SRR10047331':['Seub','Suva'],'SRR10047328':['Seub','Suva'],'SRR10047359':['Seub','Suva'],'SRR10047351':['Seub','Suva'],'SRR10047335':['Seub','Suva'],'SRR10035842':['Seub','Suva'],'SRR10047405':['Seub','Suva'],'SRR10047402':['Seub','Suva'],'SRR10047389':['Seub','Suva'],'SRR10047380':['Seub','Suva'],'SRR10047371':['Seub','Suva'],'SRR10047378':['Seub','Suva'],'SRR10047356':['Scer','Seub','Skud','Suva'],'SRR10047345':['Seub','Suva'],'SRR10047410':['Scer','Seub','Skud','Suva'],'SRR10047383':['Scer','Seub','Skud','Suva'],'SRR10047395':['Seub','Suva'],'SRR10047344':['Seub','Suva'],'SRR10047362':['Scer','Seub'],'SRR10047189':['Scer','Seub'],'SRR10047157':['Scer','Seub'],'SRR10047176':['Scer','Seub'],'SRR10047149':['Scer','Seub'],'SRR10047146':['Scer','Seub'],'SRR10047122':['Scer','Skud'],'SRR10047102':['Scer','Seub'],'SRR10047096':['Scer','Seub'],'SRR10047094':['Scer','Seub'],'SRR10047057':['Scer','Seub'],'SRR10046945':['Scer','Seub'],'SRR10047365':['Scer','Seub','Suva'],'SRR10046950':['Seub','Suva'],'SRR10046952':['Seub','Suva'],'SRR10046972':['Scer','Skud'],'SRR10046976':['Scer','Seub'],'SRR5141258':['Scer','Seub'],'SRR5141260':['Scer','Seub'],'SRR12703370':['Scer','Seub'],'SRR12703369':['Scer','Seub'],'SRR12703368':['Scer','Seub'],'SRR12703367':['Scer','Seub'],'SRR12703366':['Scer','Seub'],'SRR5251559':['Scer','Suva'],'SRR10208681':['Scer','Seub'],'SRR9925228':['Scer','Skud'],'SRR9925227':['Scer','Skud'],'SRR9925226':['Scer','Skud'],'SRR9925224':['Scer','Skud'],'SRR9925223':['Scer','Skud'],'SRR9925222':['Scer','Skud'],'SRR9925225':['Scer','Skud']}
hybrid_depth = pickle.load( open( "../Pickle/hybrid_genome_read.p", "rb" ) )
dist_intra={'Scer_Spar':[],'Scer_Skud':[],'Scer_Seub':[],'Scer_Suva':[],'Seub_Suva':[]}
depth_intra={'Scer_Spar':[],'Scer_Skud':[],'Scer_Seub':[],'Scer_Suva':[],'Seub_Suva':[]}
dist_inter={'Scer_Spar':[],'Scer_Smik':[],'Scer_Skud':[],'Scer_Seub':[],'Scer_Suva':[],'Seub_Suva':[]}
dist_tot_intra=[]
dist_tot_inter=[]
dist_total=[]
depth=[]
for i in genomes:
    for i2 in genomes:
        if len(hybrid_sp[i])==2 and len(hybrid_sp[i2])==2 and i!=i2:
            if hybrid_sp[i][0] in hybrid_sp[i2] and hybrid_sp[i][1] in hybrid_sp[i2]:
                for hy in dist_intra:
                    if hybrid_sp[i][0] in hy and hybrid_sp[i][1] in hy:
                        dist_intra[hy].append(data.loc[i,i2])
                        depth_intra[hy].append(np.mean([hybrid_depth[i],hybrid_depth[i2]]))
                        dist_total.append(data.loc[i,i2])
                        dist_tot_intra.append(data.loc[i,i2])
                        depth.append(np.mean([hybrid_depth[i],hybrid_depth[i2]]))
            else:
                for hy in dist_intra:
                    if hybrid_sp[i][0] in hy and hybrid_sp[i][1] in hy:
                        dist_inter[hy].append(data.loc[i,i2])
                        dist_total.append(data.loc[i,i2])
                        dist_tot_inter.append(data.loc[i,i2])
                        
#%%
color={'Scer_Spar':'#3D405B','Scer_Skud':'#81B29A','Scer_Seub':'#E07A5F','Scer_Suva':'#F2CC8F','Seub_Suva':'peru'}
plt.figure(figsize=[8,3])
hy_type='Scer_Spar'
plt.subplot(351)
sns.kdeplot(dist_total,shade=True,color='black',cut=0)
plt.vlines(np.mean(dist_total),ymin=0,ymax=0.75e-7,linestyle='dashed',color='black',linewidth=1.4)
plt.xlim(0,4e7)
plt.ylim(-0.05e-7)
plt.xticks([0,1e7,2e7,3e7,4e7],[])
plt.ylabel('')
plt.yticks([])
plt.subplot(356)
sns.kdeplot(dist_inter[hy_type],shade=True,color='grey',cut=0)
plt.vlines(np.mean(dist_inter[hy_type]),ymin=0,ymax=1.4e-7,linestyle='dashed',color='black',linewidth=1.4)
plt.xlim(0,4e7)
plt.ylim(-0.1e-7)
plt.xticks([0,1e7,2e7,3e7,4e7],[])
plt.ylabel('')
plt.yticks([])
plt.subplot(3,5,11)
sns.kdeplot(dist_intra[hy_type],shade=True,color=color[hy_type],cut=0)
plt.vlines(np.mean(dist_intra[hy_type]),ymin=0,ymax=3.25e-7,linestyle='dashed',color='black',linewidth=1.4)
plt.ylabel('')
plt.xlim(0,4e7)
plt.ylim(-0.3e-7)
plt.xticks([0,1e7,2e7,3e7,4e7],[0,1,2,3,4])
plt.yticks([])

hy_type='Scer_Skud'
plt.subplot(352)
sns.kdeplot(dist_total,shade=True,color='black',cut=0)
plt.vlines(np.mean(dist_total),ymin=0,ymax=0.75e-7,linestyle='dashed',color='black',linewidth=1.4)
plt.xlim(0,4e7)
plt.ylim(-0.05e-7)
plt.xticks([0,1e7,2e7,3e7,4e7],[])
plt.ylabel('')
plt.yticks([])
plt.subplot(357)
sns.kdeplot(dist_inter[hy_type],shade=True,color='grey',cut=0)
plt.vlines(np.mean(dist_inter[hy_type]),ymin=0,ymax=1.4e-7,linestyle='dashed',color='black',linewidth=1.4)
plt.xlim(0,4e7)
plt.ylim(-0.1e-7)
plt.xticks([0,1e7,2e7,3e7,4e7],[])
plt.ylabel('')
plt.yticks([])
plt.subplot(3,5,12)
sns.kdeplot(dist_intra[hy_type],shade=True,color=color[hy_type],cut=0)
plt.vlines(np.mean(dist_intra[hy_type]),ymin=0,ymax=0.75e-7,linestyle='dashed',color='black',linewidth=1.4)
plt.ylabel('')
plt.xlim(0,4e7)
plt.ylim(-0.1e-7)
plt.xticks([0,1e7,2e7,3e7,4e7],[0,1,2,3,4])
plt.yticks([])

hy_type='Scer_Seub'
plt.subplot(353)
sns.kdeplot(dist_total,shade=True,color='black',cut=0)
plt.vlines(np.mean(dist_total),ymin=0,ymax=0.75e-7,linestyle='dashed',color='black',linewidth=1.4)
plt.xlim(0,4e7)
plt.ylim(-0.05e-7)
plt.xticks([0,1e7,2e7,3e7,4e7],[])
plt.ylabel('')
plt.yticks([])
plt.subplot(358)
sns.kdeplot(dist_inter[hy_type],shade=True,color='grey',cut=0)
plt.vlines(np.mean(dist_inter[hy_type]),ymin=0,ymax=1.4e-7,linestyle='dashed',color='black',linewidth=1.4)
plt.xlim(0,4e7)
plt.ylim(-0.1e-7)
plt.xticks([0,1e7,2e7,3e7,4e7],[])
plt.ylabel('')
plt.yticks([])
plt.subplot(3,5,13)
sns.kdeplot(dist_intra[hy_type],shade=True,color=color[hy_type],cut=0)
plt.vlines(np.mean(dist_intra[hy_type]),ymin=0,ymax=1.25e-7,linestyle='dashed',color='black',linewidth=1.4)
plt.ylabel('')
plt.xlim(0,4e7)
plt.ylim(-0.2e-7)
plt.xticks([0,1e7,2e7,3e7,4e7],[0,1,2,3,4])
plt.yticks([])

hy_type='Scer_Suva'
plt.subplot(354)
sns.kdeplot(dist_total,shade=True,color='black',cut=0)
plt.vlines(np.mean(dist_total),ymin=0,ymax=0.75e-7,linestyle='dashed',color='black',linewidth=1.4)
plt.xlim(0,4e7)
plt.ylim(-0.05e-7)
plt.xticks([0,1e7,2e7,3e7,4e7],[])
plt.ylabel('')
plt.yticks([])
plt.subplot(359)
sns.kdeplot(dist_inter[hy_type],shade=True,color='grey',cut=0)
plt.vlines(np.mean(dist_inter[hy_type]),ymin=0,ymax=1.4e-7,linestyle='dashed',color='black',linewidth=1.4)
plt.xlim(0,4e7)
plt.ylim(-0.1e-7)
plt.xticks([0,1e7,2e7,3e7,4e7],[])
plt.ylabel('')
plt.yticks([])
plt.subplot(3,5,14)
sns.kdeplot(dist_intra[hy_type],shade=True,color=color[hy_type],cut=0)
plt.vlines(np.mean(dist_intra[hy_type]),ymin=0,ymax=.45e-7,linestyle='dashed',color='black',linewidth=1.4)
plt.ylabel('')
plt.xlim(0,4e7)
plt.ylim(-0.05e-7)
plt.xticks([0,1e7,2e7,3e7,4e7],[0,1,2,3,4])
plt.yticks([])

hy_type='Seub_Suva'
plt.subplot(355)
sns.kdeplot(dist_total,shade=True,color='black',cut=0)
plt.vlines(np.mean(dist_total),ymin=0,ymax=0.75e-7,linestyle='dashed',color='black',linewidth=1.4)
plt.xlim(0,4e7)
plt.ylim(-0.05e-7)
plt.xticks([0,1e7,2e7,3e7,4e7],[])
plt.ylabel('')
plt.yticks([])
plt.subplot(3,5,10)
sns.kdeplot(dist_inter[hy_type],shade=True,color='grey',cut=0)
plt.vlines(np.mean(dist_inter[hy_type]),ymin=0,ymax=1.4e-7,linestyle='dashed',color='black',linewidth=1.4)
plt.xlim(0,4e7)
plt.ylim(-0.1e-7)
plt.xticks([0,1e7,2e7,3e7,4e7],[])
plt.ylabel('')
plt.yticks([])
plt.subplot(3,5,15)
sns.kdeplot(dist_intra[hy_type],shade=True,color=color[hy_type],cut=0)
plt.vlines(np.mean(dist_intra[hy_type]),ymin=0,ymax=1.e-7,linestyle='dashed',color='black',linewidth=1.4)
plt.ylabel('')
plt.xlim(0,4e7)
plt.ylim(-0.1e-7)
plt.xticks([0,1e7,2e7,3e7,4e7],[0,1,2,3,4])
plt.yticks([])

sns.despine(trim=True,left=True)
plt.tight_layout(h_pad=0,w_pad=1.5)
    
plt.savefig('../Figures/Hybrid_GI_AAF_shared_kmers.png',bbox_inches='tight',dpi=1000)

#%%

plt.figure(figsize=[12,3.5])
plt.subplot(181)
sns.kdeplot(y=dist_total,shade=True,color='black',cut=0)
plt.hlines(np.mean(dist_total),xmin=0,xmax=1.5e-7,linestyle='dashed',color='black',linewidth=2)
print('total',np.mean(dist_total))
plt.ylim(0,2.5e7)
plt.xlim(-0.07e-7)
plt.yticks([0,0.5e7,1e7,1.5e7,2e7,2.5e7],[0,0.5,1.0,1.5,2.0,2.5])
plt.xlabel('')
plt.xticks([])
plt.title('total\nnetwork',weight='bold',fontsize=12)
plt.ylabel('shared k-mers (x10$^7$)',weight='bold')

plt.subplot(182)
sns.kdeplot(y=dist_tot_inter,shade=True,color='grey',cut=0)
plt.hlines(np.mean(dist_tot_inter),xmin=0,xmax=2.4e-7,linestyle='dashed',color='black',linewidth=2)
print('total inter',np.mean(dist_tot_inter))
plt.ylim(0,2.5e7)
plt.xlim(-0.15e-7)
plt.yticks([0,0.5e7,1e7,1.5e7,2e7,2.5e7],[])
plt.xlabel('')
plt.xticks([])
plt.title('inter-\nnetwork',weight='bold',fontsize=12)

plt.subplot(183)
sns.kdeplot(y=dist_tot_intra,shade=True,color='grey',cut=0)
plt.hlines(np.mean(dist_tot_intra),xmin=0,xmax=1.7e-7,linestyle='dashed',color='black',linewidth=2)
print('total intra',np.mean(dist_tot_intra))
plt.ylim(0,2.5e7)
plt.xlim(-0.07e-7)
plt.yticks([0,0.5e7,1e7,1.5e7,2e7,2.5e7],[])
plt.xlabel('')
plt.xticks([])
plt.title('intra-\nnetwork',weight='bold',fontsize=12)

plt.subplot(185)
hy_type='Scer_Spar'
sns.kdeplot(y=dist_intra[hy_type],shade=True,color=color[hy_type],cut=0)
plt.hlines(np.mean(dist_intra[hy_type]),xmin=0,xmax=6.5e-7,linestyle='dashed',color='black',linewidth=2)
print(hy_type,np.mean(dist_intra[hy_type]))
print(hy_type,'sd',np.std(dist_intra[hy_type]))
plt.ylim(0,2.5e7)
plt.xlim(-0.35e-7)
plt.yticks([0,0.5e7,1e7,1.5e7,2e7,2.5e7],[])
plt.xlabel('')
plt.xticks([])
plt.title('Scer x Spar',weight='bold',fontsize=12,fontstyle='italic')

plt.subplot(186)
hy_type='Scer_Skud'
sns.kdeplot(y=dist_intra[hy_type],shade=True,color=color[hy_type],cut=0)
plt.hlines(np.mean(dist_intra[hy_type]),xmin=0,xmax=1.6e-7,linestyle='dashed',color='black',linewidth=2)
print(hy_type,np.mean(dist_intra[hy_type]))
print(hy_type,'sd',np.std(dist_intra[hy_type]))
plt.ylim(0,2.5e7)
plt.xlim(-0.08e-7)
plt.yticks([0,0.5e7,1e7,1.5e7,2e7,2.5e7],[])
plt.xlabel('')
plt.xticks([])
plt.title('Scer x Skud',weight='bold',fontsize=12,fontstyle='italic')

plt.subplot(188)
hy_type='Scer_Seub'
sns.kdeplot(y=dist_intra[hy_type],shade=True,color=color[hy_type],cut=0)
plt.hlines(np.mean(dist_intra[hy_type]),xmin=0,xmax=2.6e-7,linestyle='dashed',color='black',linewidth=2)
print(hy_type,np.mean(dist_intra[hy_type]))
print(hy_type,'sd',np.std(dist_intra[hy_type]))
plt.ylim(0,2.5e7)
plt.xlim(-0.15e-7)
plt.yticks([0,0.5e7,1e7,1.5e7,2e7,2.5e7],[])
plt.xlabel('')
plt.xticks([])
plt.title('Scer x Seub',weight='bold',fontsize=12,fontstyle='italic')

plt.subplot(187)
hy_type='Scer_Suva'
sns.kdeplot(y=dist_intra[hy_type],shade=True,color=color[hy_type],cut=0)
plt.hlines(np.mean(dist_intra[hy_type]),xmin=0,xmax=1.15e-7,linestyle='dashed',color='black',linewidth=2)
print(hy_type,np.mean(dist_intra[hy_type]))
print(hy_type,'sd',np.std(dist_intra[hy_type]))
plt.ylim(0,2.5e7)
plt.xlim(-0.07e-7)
plt.yticks([0,0.5e7,1e7,1.5e7,2e7,2.5e7],[])
plt.xlabel('')
plt.xticks([])
plt.title('Scer x Suva',weight='bold',fontsize=12,fontstyle='italic')

plt.subplot(184)
hy_type='Seub_Suva'
sns.kdeplot(y=dist_intra[hy_type],shade=True,color=color[hy_type],cut=0)
plt.hlines(np.mean(dist_intra[hy_type]),xmin=0,xmax=1.99e-7,linestyle='dashed',color='black',linewidth=2)
print(hy_type,np.mean(dist_intra[hy_type]))
print(hy_type,'sd',np.std(dist_intra[hy_type]))
plt.ylim(0,2.5e7)
plt.xlim(-0.15e-7)
plt.yticks([0,0.5e7,1e7,1.5e7,2e7,2.5e7],[])
plt.xlabel('')
plt.xticks([])
plt.title('Seub x Suva',weight='bold',fontsize=12,fontstyle='italic')

sns.despine(trim=True,bottom=True)
plt.tight_layout(h_pad=0,w_pad=0)

plt.savefig('../Figures/Hybrid_GI_AAF_shared_kmers_2.png',bbox_inches='tight',dpi=1000)


#%%

plt.figure(figsize=[3.5,3.5])
GD={'Scer_Spar':0.1315,'Scer_Skud':0.204,'Scer_Seub':0.2158,'Scer_Suva':0.215,'Seub_Suva':0.1036}
mean={'Scer_Spar':10402444.990,'Scer_Skud':15907497.982795699,'Scer_Seub':18491555.62571494,'Scer_Suva':18329204.8,'Seub_Suva':9674831.87310606}
sd={'Scer_Spar':1454678.7392339606,'Scer_Skud':2571115.0775406603,'Scer_Seub':3383558.0596980406,'Scer_Suva':3605127.6682424657,'Seub_Suva':2479545.41292624}

x=[]
y=[]
for i in dist_intra:
    for num in dist_intra[i]:
        x.append(GD[i])
        y.append(num)
sns.regplot(x,y,color='black',ci=95,scatter=False,truncate=False)
for i in GD:
    plt.scatter(GD[i],mean[i],color=color[i],edgecolor='black',s=75,zorder=150)
    plt.vlines(GD[i],ymin=mean[i]-sd[i],ymax=mean[i]+sd[i],color=color[i],zorder=100)
slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(x, y)
print('r',r_value**2,'p',p_value)
plt.xlim(0.095,0.225)
plt.ylim(0.7e7,2.25e7)
plt.xticks([0.1,0.14,0.18,0.22])
plt.yticks([0.8e7,1e7,1.2e7,1.4e7,1.6e7,1.8e7,2.0e7,2.2e7],[0.8,1.0,1.2,1.4,1.6,1.8,2.0,2.2])
plt.xlabel('genetic distance',weight='bold')
plt.ylabel('mean shared k-mers (x10$^7$)',weight='bold')
plt.savefig('../Figures/Hybrid_GI_shared_x_gd.png',bbox_inches='tight',dpi=1000)

#%%

plt.figure(figsize=[3.5,3.5])
GD={'Scer_Spar':0.1315,'Scer_Skud':0.204,'Scer_Seub':0.2158,'Scer_Suva':0.215,'Seub_Suva':0.1036}
mean={'Scer_Spar':10855057.551724138,'Scer_Skud':16624400.823655915,'Scer_Seub':19844741.405628003,'Scer_Suva':22367909.2,'Seub_Suva':10777314.06060606}
sd={'Scer_Spar':1441433.9329963673,'Scer_Skud':2518818.9309481457,'Scer_Seub':2823362.6735398546,'Scer_Suva':2920348.4122588797,'Seub_Suva':1935177.769167007}

x=[]
y=[]
for i in dist_intra:
    for num in dist_intra[i]:
        x.append(GD[i])
        y.append(num)
sns.regplot(x,y,color='black',ci=95,scatter=False,truncate=False)

for i in GD:
    plt.scatter(GD[i],mean[i],color=color[i],edgecolor='black',s=75,zorder=150)
    plt.vlines(GD[i],ymin=mean[i]-sd[i],ymax=mean[i]+sd[i],color=color[i],zorder=100)
slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(x, y)
print('r',r_value**2,'p',p_value)
plt.xlim(0.095,0.225)
plt.ylim(0.7e7,2.6e7)
plt.xticks([0.1,0.14,0.18,0.22])
plt.yticks([0.8e7,1e7,1.2e7,1.4e7,1.6e7,1.8e7,2.0e7,2.2e7,2.4e7,2.6e7],[0.8,1.0,1.2,1.4,1.6,1.8,2.0,2.2,2.4,2.6])
plt.xlabel('genetic distance',weight='bold')
plt.ylabel('mean shared k-mers (x10$^7$)',weight='bold')
plt.savefig('../Figures/Hybrid_GI_shared_x_gd_2.png',bbox_inches='tight',dpi=1000)
#%%
plt.figure(figsize=[4,4])
#sns.regplot(dist_tot_intra,depth,scatter=False)
#plt.scatter(dist_tot_intra,depth)
for i in depth_intra:
    plt.scatter(np.mean(depth_intra[i]),np.mean(dist_intra[i]),color=color[i],s=75,edgecolor='black')
plt.xlabel('genome read depth',weight='bold')
plt.ylabel('mean shared k-mers',weight='bold')
