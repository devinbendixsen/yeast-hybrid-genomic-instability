#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  1 16:45:49 2020

@author: devin
"""
import os
import time

dir='../DATA/seq_data'
for file1 in os.listdir(dir):
    if '_1' in file1:
        file2=file1.split('_1',1)[0]
        file2+='_2.fastq.gz'
        os.system("sbatch -M snowy trim_slurm.sh {} {}".format(file1,file2))
        time.sleep(5)
