#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  8 13:30:31 2021

@author: devin
"""
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import matplotlib.cm as cm
import matplotlib as mpl
import pickle
import scipy.stats
font = {'family' : 'sans-serif','weight' : 'normal','size' : 12}
plt.rc('font', **font)

#%%
hybrid_order = pickle.load( open( "../Pickle/hybrid_order.p", "rb" ) )
hybrid_sp={'SRR1119201':['Suva','Scer','Skud'],'SRR1119202':['Suva','Scer','Skud'],'SRR1119203':['Suva','Scer'],'SRR1119204':['Scer','Suva','Seub'],'SRR1649181':['Scer','Seub'],'SRR1649183':['Scer','Seub'],'SRR800854':['Scer','Spar'],'SRR800807':['Scer','Spar'],'SRR800768':['Scer','Spar'],'SRR2967902':['Scer','Spar'],'SRR2967903':['Scer','Spar'],'SRR2967904':['Scer','Skud'],'SRR2967905':['Scer','Smik'],'SRR2967906':['Scer','Suva'],'SRR2967849':['Scer','Skud'],'SRR2967853':['Scer','Skud'],'SRR2967864':['Scer','Skud'],'DRR040649':['Scer','Seub'],'DRR040656':['Scer','Seub'],'DRR040657':['Scer','Seub'],'DRR040637':['Scer','Seub'],'DRR040641':['Scer','Seub'],'DRR040645':['Scer','Seub'],'DRR040658':['Scer','Seub'],'DRR040650':['Scer','Seub'],'DRR040669':['Scer','Suva','Seub'],'DRR040660':['Scer','Seub'],'SRR2968024':['Scer','Skud'],'DRR040667':['Scer','Suva','Seub'],'SRR2968005':['Scer','Skud'],'SRR2967882':['Scer','Suva'],'ERR1111502':['Scer','Spar'],'ERR1111503':['Scer','Spar'],'SRR2967875':['Scer','Skud'],'DRR040654':['Scer','Seub'],'SRR2968042':['Scer','Skud'],'SRR2968046':['Scer','Seub'],'SRR3265414':['Scer','Skud'],'SRR7631525':['Scer','Suva'],'ERR3010122':['Scer','Spar'],'ERR3010128':['Scer','Spar'],'ERR3010129':['Scer','Spar'],'ERR3010125':['Scer','Spar'],'ERR3010126':['Scer','Spar'],'ERR3010127':['Scer','Spar'],'SRR8648839':['Scer','Seub'],'ERR3010130':['Scer','Spar'],'SRR8648841':['Scer','Seub'],'ERR3010131':['Scer','Spar'],'ERR3010132':['Scer','Spar'],'ERR3010133':['Scer','Spar'],'ERR3010134':['Scer','Spar'],'ERR3010135':['Scer','Spar'],'ERR3010136':['Scer','Spar'],'ERR3010137':['Scer','Spar'],'ERR3010138':['Scer','Spar'],'ERR3010139':['Scer','Spar'],'ERR3010140':['Scer','Spar'],'ERR3010143':['Scer','Spar'],'SRR10047154':['Scer','Seub'],'SRR10047336':['Scer','Seub','Skud'],'SRR10047416':['Seub','Suva'],'SRR10047092':['Scer','Seub'],'SRR10047152':['Scer','Seub'],'ERR3010146':['Scer','Spar'],'ERR3010147':['Scer','Spar'],'ERR3010149':['Scer','Spar'],'ERR3010150':['Scer','Spar'],'SRR8799642':['Scer','Seub'],'SRR8799680':['Scer','Seub'],'SRR8799685':['Scer','Seub'],'SRR8799646':['Scer','Seub'],'SRR8799652':['Scer','Seub'],'SRR8799686':['Scer','Seub'],'SRR8799647':['Scer','Seub'],'SRR8799692':['Scer','Seub'],'SRR8799694':['Scer','Seub'],'SRR8799654':['Scer','Seub'],'SRR8799683':['Scer','Seub'],'SRR8799682':['Scer','Seub'],'SRR8799666':['Scer','Seub'],'SRR8799653':['Scer','Seub'],'SRR8799631':['Scer','Seub'],'SRR8799695':['Scer','Seub'],'SRR8799649':['Scer','Seub'],'SRR8799645':['Scer','Seub'],'SRR8799681':['Scer','Seub'],'SRR8799684':['Scer','Seub'],'SRR8799676':['Scer','Seub'],'SRR8799687':['Scer','Seub'],'SRR8799691':['Scer','Seub'],'SRR8799693':['Scer','Seub'],'SRR8799659':['Scer','Seub'],'SRR8799643':['Scer','Seub'],'SRR8799644':['Scer','Seub'],'SRR8799662':['Scer','Seub'],'SRR8799663':['Scer','Seub'],'SRR8799634':['Scer','Seub'],'SRR8799640':['Scer','Seub'],'SRR8799671':['Scer','Seub'],'SRR8799667':['Scer','Seub'],'SRR8799637':['Scer','Seub'],'SRR8799690':['Scer','Seub'],'SRR8799673':['Scer','Skud'],'SRR8799672':['Scer','Skud'],'SRR8799674':['Scer','Skud'],'SRR8799650':['Scer','Skud'],'SRR8799633':['Scer','Skud'],'SRR8799677':['Scer','Skud'],'SRR8799678':['Scer','Skud'],'SRR8799651':['Scer','Skud'],'SRR8799679':['Scer','Skud'],'SRR8799657':['Suva','Seub'],'SRR8799661':['Suva','Seub'],'SRR8799665':['Suva','Seub'],'SRR8799698':['Suva','Seub'],'SRR8799660':['Suva','Seub'],'SRR8799658':['Suva','Seub'],'SRR8799655':['Suva','Seub'],'SRR8799668':['Suva','Seub'],'SRR8799641':['Suva','Seub'],'SRR8799656':['Suva','Seub'],'SRR8799630':['Suva','Seub'],'SRR8799629':['Suva','Seub'],'SRR8799664':['Suva','Seub'],'SRR10035841':['Scer','Skud'],'SRR10047007':['Scer','Skud'],'SRR10047062':['Scer','Seub'],'SRR10047068':['Scer','Seub'],'SRR10047077':['Scer','Seub'],'SRR10047225':['Scer','Seub'],'SRR10047243':['Scer','Seub','Suva'],'SRR10047261':['Scer','Seub'],'SRR10047170':['Scer','Seub'],'SRR10047181':['Scer','Seub'],'SRR10047231':['Scer','Skud'],'SRR10047188':['Scer','Seub'],'SRR10047256':['Scer','Seub'],'SRR10047124':['Scer','Seub'],'SRR10047019':['Seub','Suva'],'SRR10047000':['Scer','Seub'],'SRR10047191':['Scer','Seub'],'SRR10047313':['Scer','Seub'],'SRR10047163':['Scer','Seub'],'SRR10047100':['Scer','Seub'],'SRR10047089':['Scer','Seub'],'SRR10047028':['Scer','Skud'],'SRR10047024':['Scer','Seub'],'SRR10047374':['Scer','Seub'],'SRR10047307':['Scer','Seub'],'SRR10047017':['Scer','Seub'],'SRR10047241':['Scer','Seub'],'SRR10047295':['Seub','Suva'],'SRR10047331':['Seub','Suva'],'SRR10047328':['Seub','Suva'],'SRR10047359':['Seub','Suva'],'SRR10047351':['Seub','Suva'],'SRR10047335':['Seub','Suva'],'SRR10035842':['Seub','Suva'],'SRR10047405':['Seub','Suva'],'SRR10047402':['Seub','Suva'],'SRR10047389':['Seub','Suva'],'SRR10047380':['Seub','Suva'],'SRR10047371':['Seub','Suva'],'SRR10047378':['Seub','Suva'],'SRR10047356':['Scer','Seub','Skud','Suva'],'SRR10047345':['Seub','Suva'],'SRR10047410':['Scer','Seub','Skud','Suva'],'SRR10047383':['Scer','Seub','Skud','Suva'],'SRR10047395':['Seub','Suva'],'SRR10047344':['Seub','Suva'],'SRR10047362':['Scer','Seub'],'SRR10047189':['Scer','Seub'],'SRR10047157':['Scer','Seub'],'SRR10047176':['Scer','Seub'],'SRR10047149':['Scer','Seub'],'SRR10047146':['Scer','Seub'],'SRR10047122':['Scer','Skud'],'SRR10047102':['Scer','Seub'],'SRR10047096':['Scer','Seub'],'SRR10047094':['Scer','Seub'],'SRR10047057':['Scer','Seub'],'SRR10046945':['Scer','Seub'],'SRR10047365':['Scer','Seub','Suva'],'SRR10046950':['Seub','Suva'],'SRR10046952':['Seub','Suva'],'SRR10046972':['Scer','Skud'],'SRR10046976':['Scer','Seub'],'SRR5141258':['Scer','Seub'],'SRR5141260':['Scer','Seub'],'SRR12703370':['Scer','Seub'],'SRR12703369':['Scer','Seub'],'SRR12703368':['Scer','Seub'],'SRR12703367':['Scer','Seub'],'SRR12703366':['Scer','Seub'],'SRR5251559':['Scer','Suva'],'SRR10208681':['Scer','Seub'],'SRR9925228':['Scer','Skud'],'SRR9925227':['Scer','Skud'],'SRR9925226':['Scer','Skud'],'SRR9925224':['Scer','Skud'],'SRR9925223':['Scer','Skud'],'SRR9925222':['Scer','Skud'],'SRR9925225':['Scer','Skud']}
species=['Scer','Spar','Smik','Sjur','Skud','Sarb','Seub','Suva']
chro=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33]
genomes=['ERR1111503','ERR1111502','SRR2967902','ERR3010130','SRR2967903','SRR800807','SRR800768','ERR3010131','ERR3010132','ERR3010133','ERR3010134','ERR3010135','ERR3010136','ERR3010137','ERR3010138','ERR3010139','ERR3010140','ERR3010143','SRR800854','ERR3010122','ERR3010128','ERR3010129','ERR3010125','ERR3010126','ERR3010127','ERR3010146','ERR3010147','ERR3010149','ERR3010150','SRR2967905','SRR8799672','SRR10047028','SRR2968005','SRR8799673','SRR8799674','SRR8799678','SRR10035841','SRR2967904','SRR2967849','SRR2967853','SRR2967864','SRR2968024','SRR2967875','SRR2968042','SRR3265414','SRR8799650','SRR8799633','SRR8799677','SRR8799651','SRR8799679','SRR9925228','SRR9925227','SRR9925226','SRR9925224','SRR9925223','SRR9925222','SRR9925225','SRR10047154','DRR040657','DRR040645','DRR040654','SRR8648841','SRR10047092','DRR040649','DRR040656','DRR040637','DRR040641','DRR040658','DRR040650','DRR040660','SRR2968046','SRR8648839','SRR1649181','SRR1649183','SRR10047152','SRR8799642','SRR8799680','SRR8799685','SRR8799646','SRR8799652','SRR8799686','SRR8799647','SRR8799692','SRR8799694','SRR8799654','SRR8799683','SRR8799682','SRR8799666','SRR8799653','SRR8799631','SRR8799695','SRR8799649','SRR8799645','SRR8799681','SRR8799684','SRR8799676','SRR8799687','SRR8799691','SRR8799693','SRR8799659','SRR8799643','SRR8799644','SRR8799662','SRR8799663','SRR8799634','SRR8799640','SRR8799671','SRR8799667','SRR8799637','SRR8799690','SRR10047062','SRR10047068','SRR10047077','SRR10047225','SRR10047261','SRR10047170','SRR10047181','SRR10047188','SRR10047256','SRR10047124','SRR10047000','SRR10047191','SRR10047313','SRR10047163','SRR10047100','SRR10047089','SRR10047024','SRR10047374','SRR10047307','SRR10047017','SRR10047241','SRR10047362','SRR10047189','SRR10047157','SRR10047176','SRR10047149','SRR10047146','SRR10047102','SRR10047096','SRR10047094','SRR10047057','SRR10046945','SRR10046976','SRR5141258','SRR5141260','SRR12703370','SRR12703369','SRR12703368','SRR12703367','SRR12703366','SRR10208681','SRR2967882','SRR2967906','SRR5251559','SRR1119203','SRR10047416','DRR040667','DRR040669','SRR8799657','SRR8799661','SRR8799665','SRR8799698','SRR8799660','SRR8799658','SRR8799655','SRR8799668','SRR8799641','SRR8799656','SRR8799630','SRR8799629','SRR8799664','SRR10047019','SRR10047295','SRR10047331','SRR10047328','SRR10047359','SRR10047351','SRR10047335','SRR10035842','SRR10047405','SRR10047402','SRR10047389','SRR10047380','SRR10047371','SRR10047378','SRR10047345','SRR10047395','SRR10047344','SRR10046950','SRR10046952','SRR1119204','SRR10047336','SRR10047231','SRR10047356','SRR10047007','SRR10047410','SRR10047383','SRR10046972','SRR10047243','SRR10047365','SRR1119201','SRR7631525','SRR1119202','SRR10047122']
hybrid_depth = pickle.load( open( "../Pickle/hybrid_depth.p", "rb" ) )
hybrid_depth_pos = pickle.load( open( "../Pickle/hybrid_depth_pos.p", "rb" ) )
colors={'Scer':"#015482",'Spar':"#dc4d01",'Smik':"#ffda03",'Sjur':'#a2653e','Skud':'#2b5d34','Sarb':'gray','Seub':'#8c000f','Suva':'#4b006e'}
genome_depth = pickle.load( open( "../Pickle/hybrid_genome_read.p", "rb" ) )
depth_var = pickle.load( open( "../Pickle/depth_var.p", "rb" ) )
depth_1 = pickle.load( open( "../Pickle/depth_1.p", "rb" ) )
depth_2 = pickle.load( open( "../Pickle/depth_2.p", "rb" ) )
GD={'Scer_Spar':0.1315,'Scer_Skud':0.204,'Scer_Seub':0.2158,'Scer_Suva':0.215,'Seub_Suva':0.1036,'Scer_Smik':0.1884}
map_filt = pickle.load( open( "../Pickle/map_filt.p", "rb" ) )
#%%
mean_chro={}
for i in hybrid_depth:
    mean_chro[i]={}
    for chro in range(1,17):
        mean_chro[i][chro]=(np.mean(hybrid_depth[i][hybrid_sp[i][0]][chro])+np.mean(hybrid_depth[i][hybrid_sp[i][1]][chro]))
x=np.std(list(mean_chro['ERR3010134'].values()))**2
print(x,depth_var['ERR3010134'])


#%%
chro_gain={}
chro_loss={}
size=0.3
for i in genome_depth:
    if i in map_filt:
        loss=0
        gain=0
        for chro in range(1,17):
            if mean_chro[i][chro]<=((1-size)*genome_depth[i]):
                loss+=1
    
            if mean_chro[i][chro]>=((1+size)*genome_depth[i]):
                gain+=1

        chro_gain[i]=gain
        chro_loss[i]=loss
pickle.dump( chro_gain, open( "../Pickle/chro_gain.p", "wb" ) )
pickle.dump( chro_loss, open( "../Pickle/chro_loss.p", "wb" ) )

gain_num=0
loss_num=0
num=0
both=0
for i in chro_gain:
    if chro_gain[i]!=0:
        gain_num+=1
    if chro_loss[i]!=0:
        loss_num+=1
    if chro_gain[i]!=0 or chro_loss[i]!=0:
        num+=1
    if chro_gain[i]!=0 and chro_loss[i]!=0:
        both+=1
print('gain',gain_num)
print('loss',loss_num)
print('total',num)
print('both',both)

#%%
gen_dist={}
for i in hybrid_sp:
    if len(hybrid_sp[i])==2:
        if 'Scer' in hybrid_sp[i] and 'Spar' in hybrid_sp[i]:
            gen_dist[i]=GD['Scer_Spar']
        elif 'Scer' in hybrid_sp[i] and 'Smik' in hybrid_sp[i]:
            gen_dist[i]=GD['Scer_Smik']
        elif 'Scer' in hybrid_sp[i] and 'Skud' in hybrid_sp[i]:
            gen_dist[i]=GD['Scer_Skud']
        elif 'Scer' in hybrid_sp[i] and 'Seub' in hybrid_sp[i]:
            gen_dist[i]=GD['Scer_Seub']
        elif 'Scer' in hybrid_sp[i] and 'Suva' in hybrid_sp[i]:
            gen_dist[i]=GD['Scer_Suva']
        elif 'Seub' in hybrid_sp[i] and 'Suva' in hybrid_sp[i]:
            gen_dist[i]=GD['Seub_Suva']
#%%
color='black'
plt.figure(figsize=[8,8])
plt.subplot(331)
x=[]
y=[]
for i in depth_var:
    diff=abs(depth_1[i]-depth_2[i])
    y.append(diff)
    x.append(depth_var[i])
plt.scatter(x,y)

sns.regplot(x,y,scatter=False,truncate=False,color=color)
slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(x, y)
print('r2:', r_value*r_value,'p:',p_value)
plt.ylim(-0.3,4)
#plt.xlim(-.02,43)
plt.ylabel('species difference\n(sp1-sp2)',weight='bold')
plt.subplot(337)
x=[]
y=[]
for i in depth_var:
    y.append(genome_depth[i])
    x.append(depth_var[i])
sns.regplot(x,y,scatter=False,truncate=False,color=color)
slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(x, y)
print('r2:', r_value*r_value,'p:',p_value)
plt.ylim(6.7,10)
plt.xlim(-.02,0.48)
plt.ylabel('genome read depth',weight='bold')
plt.xlabel('read depth variance',weight='bold')

plt.subplot(334)
x=[]
y=[]
for i in depth_var:
    y.append(gen_dist[i])
    x.append(depth_var[i])
sns.regplot(x,y,scatter=False,truncate=False,color=color)
slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(x, y)
print('r2:', r_value*r_value,'p:',p_value)
plt.ylim(0.1,0.22)
plt.xlim(-.02,0.48)
plt.ylabel('genetic distance',weight='bold')

plt.subplot(335)
x=[]
y=[]
for i in depth_var:
    diff=abs(depth_1[i]-depth_2[i])
    y.append(gen_dist[i])
    x.append(diff)
sns.regplot(x,y,scatter=False,truncate=True,color=color)
slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(x, y)
print('r2:', r_value*r_value,'p:',p_value)

plt.xlim(-0.3,4)
plt.ylim(0.1,0.22)

plt.subplot(338)
x=[]
y=[]
for i in depth_var:
    diff=abs(depth_1[i]-depth_2[i])
    y.append(genome_depth[i])
    x.append(diff)
sns.regplot(x,y,scatter=False,truncate=True,color=color)
slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(x, y)
print('r2:', r_value*r_value,'p:',p_value)
plt.ylim(6.7,10)
plt.xlim(-0.3,4)
plt.xlabel('species difference\n(sp1-sp2)',weight='bold')

plt.subplot(339)
x=[]
y=[]
for i in depth_var:
    y.append(genome_depth[i])
    x.append(gen_dist[i])
sns.regplot(x,y,scatter=False,truncate=False,color=color)
slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(x, y)
print('r2:', r_value*r_value,'p:',p_value)
plt.ylim(6.7,10)
plt.xlim(0.1,0.22)
plt.xlabel('genetic distance',weight='bold')

plt.tight_layout()


#%%
from joypy import joyplot
chro_gain = pickle.load( open( "../Pickle/chro_gain.p", "rb" ) )
chro_loss = pickle.load( open( "../Pickle/chro_loss.p", "rb" ) )
pairwise=[]
for i in genomes:
    if len(hybrid_sp[i])==2:# and i!='SRR10047351':
        pairwise.append(i)
data=pd.DataFrame(columns=['cross','variance','gain','loss'],index=pairwise)
data=data.astype(float)

for i in depth_var:
    data.loc[i,'variance']=depth_var[i]
    data.loc[i,'gain']=chro_gain[i]
    data.loc[i,'loss']=chro_loss[i]
    
for i in pairwise:
    if 'Scer' in hybrid_sp[i] and 'Spar' in hybrid_sp[i]:
        data.loc[i,'cross']='Scer_Spar'
    elif 'Scer' in hybrid_sp[i] and 'Smik' in hybrid_sp[i]:
        data.loc[i,'cross']='Scer_Smik'
    elif 'Scer' in hybrid_sp[i] and 'Skud' in hybrid_sp[i]:
        data.loc[i,'cross']='Scer_Skud'
    elif 'Scer' in hybrid_sp[i] and 'Seub' in hybrid_sp[i]:
        data.loc[i,'cross']='Scer_Seub'
    elif 'Scer' in hybrid_sp[i] and 'Suva' in hybrid_sp[i]:
        data.loc[i,'cross']='Scer_Suva'
    elif 'Seub' in hybrid_sp[i] and 'Suva' in hybrid_sp[i]:
        data.loc[i,'cross']='Seub_Suva'
    
#%%

plt.figure()
joyplot(
    data=data[['variance','cross']], 
    by='cross',
    colormap=[cm.autumn],
    figsize=[4,4],
    alpha=0.8,
    kind='kde'
)

#%% CHROMOSOME GAINS
plot_loc={'all':7,'Scer_Spar':6,'Scer_Skud':4,'Scer_Seub':3,'Scer_Suva':2,'Seub_Suva':1,'Scer_Smik':5}
counts={}
for i in GD:
    counts[i]={}
    for n in range(0,8):
        counts[i][n]=0
counts['all']={}
for n in range(0,8):
    counts['all'][n]=0


for i in pairwise:
    if 'Scer' in hybrid_sp[i] and 'Spar' in hybrid_sp[i] and i in map_filt:
        counts['Scer_Spar'][chro_gain[i]]+=1
        counts['all'][chro_gain[i]]+=1
    elif 'Scer' in hybrid_sp[i] and 'Smik' in hybrid_sp[i] and i in map_filt:
        counts['Scer_Smik'][chro_gain[i]]+=1
        counts['all'][chro_gain[i]]+=1
    elif 'Scer' in hybrid_sp[i] and 'Skud' in hybrid_sp[i] and i in map_filt:
        counts['Scer_Skud'][chro_gain[i]]+=1
        counts['all'][chro_gain[i]]+=1
    elif 'Scer' in hybrid_sp[i] and 'Seub' in hybrid_sp[i] and i in map_filt:
        counts['Scer_Seub'][chro_gain[i]]+=1
        counts['all'][chro_gain[i]]+=1
    elif 'Scer' in hybrid_sp[i] and 'Suva' in hybrid_sp[i] and i in map_filt:
        counts['Scer_Suva'][chro_gain[i]]+=1
        counts['all'][chro_gain[i]]+=1
    elif 'Seub' in hybrid_sp[i] and 'Suva' in hybrid_sp[i] and i in map_filt:
        counts['Seub_Suva'][chro_gain[i]]+=1
        counts['all'][chro_gain[i]]+=1

norm_counts={}
for i in counts:
    norm_counts[i]={}
    for n in range(0,8):
        norm_counts[i][n]=(counts[i][n]/sum(list(counts[i].values())))
        if n==0:
            print(i,1-(counts[i][n]/sum(list(counts[i].values()))))
        
plt.figure(figsize=[8,3.5])
plt.subplot(121)

x=[]
y=[]
size=[]
for i in norm_counts:
    for n in range(0,8):
        x.append(n)
        y.append(plot_loc[i])
        size.append(norm_counts[i][n]*600)
plt.scatter(x,y,s=size,cmap='Reds',c=x,edgecolor='black')

plt.xlim(-0.5,8.5)
plt.ylim(0.5,7.5)
plt.xticks([0,1,2,3,4,5,6,7])
plt.xlabel('chromosome gain (n)',weight='bold')
#plt.title('high chromosomes',weight='bold',fontsize=12)
plt.yticks([1,2,3,4,5,6,7],['Seub x Suva','Scer x Suva','Scer x Seub','Scer x Skud','Scer x Smik','Scer x Spar','All'],fontstyle='italic',weight='bold')
plt.text(0,6,s='27',verticalalignment='center',horizontalalignment='center',fontsize=10)
plt.text(0,5,s='1',verticalalignment='center',horizontalalignment='center',fontsize=10)
plt.text(0,4,s='22',verticalalignment='center',horizontalalignment='center',fontsize=10)
plt.text(0,3,s='41',verticalalignment='center',horizontalalignment='center',fontsize=10)
plt.text(0,2,s='5',verticalalignment='center',horizontalalignment='center',fontsize=10)
plt.text(0,1,s='13',verticalalignment='center',horizontalalignment='center',fontsize=10)
plt.text(0,7,s='109',verticalalignment='center',horizontalalignment='center',fontsize=10)

plt.text(7.6,1,s='54%',verticalalignment='center',horizontalalignment='center',fontsize=12)
plt.text(7.6,2,s='0%',verticalalignment='center',horizontalalignment='center',fontsize=12)
plt.text(7.6,3,s='46%',verticalalignment='center',horizontalalignment='center',fontsize=12)
plt.text(7.6,4,s='29%',verticalalignment='center',horizontalalignment='center',fontsize=12)
plt.text(7.6,5,s='0%',verticalalignment='center',horizontalalignment='center',fontsize=12)
plt.text(7.6,6,s='7%',verticalalignment='center',horizontalalignment='center',fontsize=12)
plt.text(7.6,7,s='36%',verticalalignment='center',horizontalalignment='center',fontsize=12)

#LOSS
counts={}
for i in GD:
    counts[i]={}
    for n in range(0,8):
        counts[i][n]=0
counts['all']={}
for n in range(0,8):
    counts['all'][n]=0

for i in pairwise:
    if 'Scer' in hybrid_sp[i] and 'Spar' in hybrid_sp[i] and i in map_filt:
        counts['Scer_Spar'][chro_loss[i]]+=1
        counts['all'][chro_loss[i]]+=1
    elif 'Scer' in hybrid_sp[i] and 'Smik' in hybrid_sp[i] and i in map_filt:
        counts['Scer_Smik'][chro_loss[i]]+=1
        counts['all'][chro_loss[i]]+=1
    elif 'Scer' in hybrid_sp[i] and 'Skud' in hybrid_sp[i] and i in map_filt:
        counts['Scer_Skud'][chro_loss[i]]+=1
        counts['all'][chro_loss[i]]+=1
    elif 'Scer' in hybrid_sp[i] and 'Seub' in hybrid_sp[i] and i in map_filt:
        counts['Scer_Seub'][chro_loss[i]]+=1
        counts['all'][chro_loss[i]]+=1
    elif 'Scer' in hybrid_sp[i] and 'Suva' in hybrid_sp[i] and i in map_filt:
        counts['Scer_Suva'][chro_loss[i]]+=1
        counts['all'][chro_loss[i]]+=1
    elif 'Seub' in hybrid_sp[i] and 'Suva' in hybrid_sp[i] and i in map_filt:
        counts['Seub_Suva'][chro_loss[i]]+=1
        counts['all'][chro_loss[i]]+=1

norm_counts={}
for i in counts:
    norm_counts[i]={}
    for n in range(0,8):
        norm_counts[i][n]=(counts[i][n]/sum(list(counts[i].values())))
        if n==0:
            print(i,1-(counts[i][n]/sum(list(counts[i].values()))))
plt.subplot(122)
x=[]
y=[]
size=[]
for i in norm_counts:
    for n in range(0,8):
        x.append(n)
        y.append(plot_loc[i])
        size.append(norm_counts[i][n]*600)
plt.scatter(x,y,s=size,cmap='Blues',c=x,edgecolor='black')
plt.xticks([0,1,2,3,4,5,6,7])
plt.yticks([1,2,3,4,5,6,7],[])
plt.xlim(-0.5,7.5)
plt.ylim(0.5,7.5)
plt.xlabel('chromosome loss (n)',weight='bold')
#plt.title('low chromosomes',weight='bold',fontsize=12)
plt.tight_layout(w_pad=1)
sns.despine(trim=True)

plt.text(0,6,s='29',verticalalignment='center',horizontalalignment='center',fontsize=10)
plt.text(0,5,s='1',verticalalignment='center',horizontalalignment='center',fontsize=10)
plt.text(0,4,s='28',verticalalignment='center',horizontalalignment='center',fontsize=10)
plt.text(0,3,s='59',verticalalignment='center',horizontalalignment='center',fontsize=10)
plt.text(0,2,s='4',verticalalignment='center',horizontalalignment='center',fontsize=10)
plt.text(0,1,s='24',verticalalignment='center',horizontalalignment='center',fontsize=10)
plt.text(0,7,s='145',verticalalignment='center',horizontalalignment='center',fontsize=10)

plt.text(7.6,1,s='14%',verticalalignment='center',horizontalalignment='center',fontsize=12)
plt.text(7.6,2,s='20%',verticalalignment='center',horizontalalignment='center',fontsize=12)
plt.text(7.6,3,s='22%',verticalalignment='center',horizontalalignment='center',fontsize=12)
plt.text(7.6,4,s='10%',verticalalignment='center',horizontalalignment='center',fontsize=12)
plt.text(7.6,5,s='0%',verticalalignment='center',horizontalalignment='center',fontsize=12)
plt.text(7.6,6,s='0%',verticalalignment='center',horizontalalignment='center',fontsize=12)
plt.text(7.6,7,s='15%',verticalalignment='center',horizontalalignment='center',fontsize=12)

plt.savefig('../Figures/Hybrid_GI_read_depth_loss-gain.svg',bbox_inches='tight',dpi=1000,transparent=True)
#%%
var_hy={}
for i in GD:
    var_hy[i]=[]
var_hy['all']=[]
for i in pairwise:
    if 'Scer' in hybrid_sp[i] and 'Spar' in hybrid_sp[i] and i in map_filt:
        var_hy['Scer_Spar'].append(depth_var[i])
        var_hy['all'].append(depth_var[i])
    elif 'Scer' in hybrid_sp[i] and 'Smik' in hybrid_sp[i] and i in map_filt:
        var_hy['Scer_Smik'].append(depth_var[i])
        var_hy['all'].append(depth_var[i])
    elif 'Scer' in hybrid_sp[i] and 'Skud' in hybrid_sp[i] and i in map_filt:
        var_hy['Scer_Skud'].append(depth_var[i])
        var_hy['all'].append(depth_var[i])
    elif 'Scer' in hybrid_sp[i] and 'Seub' in hybrid_sp[i] and i in map_filt:
        var_hy['Scer_Seub'].append(depth_var[i])
        var_hy['all'].append(depth_var[i])
    elif 'Scer' in hybrid_sp[i] and 'Suva' in hybrid_sp[i] and i in map_filt:
        var_hy['Scer_Suva'].append(depth_var[i])
        var_hy['all'].append(depth_var[i])
    elif 'Seub' in hybrid_sp[i] and 'Suva' in hybrid_sp[i] and i in map_filt:
        var_hy['Seub_Suva'].append(depth_var[i])
        var_hy['all'].append(depth_var[i])

plot_loc={'all':7,'Scer_Spar':6,'Scer_Skud':4,'Scer_Seub':3,'Scer_Suva':2,'Seub_Suva':1,'Scer_Smik':5}
x=[]
y=[]
mean={}
sd={}
for i in var_hy:
    x.append(np.mean(var_hy[i]))
    y.append(plot_loc[i])
    mean[i]=np.mean(var_hy[i])
    sd[i]=np.std(var_hy[i])/np.sqrt(len(var_hy[i]))
plt.figure(figsize=[2,3.5])
for i in mean:
    plt.hlines(plot_loc[i],xmin=mean[i]-sd[i],xmax=mean[i]+sd[i],color='black',zorder=0)
plt.scatter(x,y,cmap='binary',c=x,edgecolor='black',s=75,zorder=100)
plt.xlim(-0.3,4.5)

plt.yticks([1,2,3,4,5,6,7],[])
plt.xticks([0,1,2,3,4])
sns.despine(trim=True)
plt.xlabel('variance',weight='bold')
plt.savefig('../Figures/Hybrid_GI_read_depth_var-mean.svg',bbox_inches='tight',dpi=1000,transparent=True)

#%%
plt.figure(figsize=[3,1])
plt.scatter([1,2,3,4,5],[1,1,1,1,1],s=[600,300,150,30,6],color='lightgrey',edgecolor='black')
plt.xlim(0,6)
plt.savefig('../Figures/Hybrid_GI_read_depth_loss-gain_legend.svg',bbox_inches='tight',dpi=1000,transparent=True)