#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 25 14:23:53 2021

@author: devin
"""
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import matplotlib.cm as cm
import matplotlib as mpl
import pickle
font = {'family' : 'sans-serif','weight' : 'normal','size' : 12}
plt.rc('font', **font)

#%%
plt.figure(figsize=[6,6])
plt.hlines([1],xmin=0,xmax=0.2158,zorder=0,color='#6C9A8B',linewidth=3)
plt.hlines([2],xmin=0,xmax=0.215,zorder=0,color='#6C9A8B',linewidth=3)
plt.hlines([3],xmin=0,xmax=0.204,zorder=0,color='#6C9A8B',linewidth=3)
plt.hlines([4],xmin=0,xmax=0.1884,zorder=0,color='#6C9A8B',linewidth=3)
plt.hlines([5],xmin=0,xmax=0.1315,zorder=0,color='#6C9A8B',linewidth=3)
plt.hlines([6],xmin=0,xmax=0.1036,zorder=0,color='#6C9A8B',linewidth=3)
plt.text(-.005,1, s='S. cerevisiae',ha='right',va='center',fontstyle='italic')
plt.text(-.005,2, s='S. cerevisiae',ha='right',va='center',fontstyle='italic')
plt.text(-.005,3, s='S. cerevisiae',ha='right',va='center',fontstyle='italic')
plt.text(-.005,4, s='S. cerevisiae',ha='right',va='center',fontstyle='italic')
plt.text(-.005,5, s='S. cerevisiae',ha='right',va='center',fontstyle='italic')
plt.text(-.005,6, s='S. eubayanus',ha='right',va='center',fontstyle='italic')
plt.text(0.223,1, s='S. eubayanus',ha='left',va='center',fontstyle='italic')
plt.text(0.222,2, s='S. uvarum',ha='left',va='center',fontstyle='italic')
plt.text(0.21,3, s='S. kudriavzevii',ha='left',va='center',fontstyle='italic')
plt.text(0.195,4, s='S. mikatae',ha='left',va='center',fontstyle='italic')
plt.text(0.138,5, s='S. paradoxus',ha='left',va='center',fontstyle='italic')
plt.text(0.11,6, s='S. uvarum',ha='left',va='center',fontstyle='italic')
plt.text(-.005,7, s='human',ha='right',va='center')
plt.text(-.005,8, s='human',ha='right',va='center')
plt.text(-.005,9, s='human',ha='right',va='center')
plt.text(-.005,10, s='human',ha='right',va='center')
plt.text(-.005,11, s='human',ha='right',va='center')
plt.text(-.005,12, s='human',ha='right',va='center')
plt.text(0.252,7, s='rabbit',ha='left',va='center',fontstyle='italic')
plt.text(0.247,8, s='mouse',ha='left',va='center',fontstyle='italic')
plt.text(0.21,9, s='S. kudriavzevii',ha='left',va='center',fontstyle='italic')
plt.text(0.194,10, s='S. mikatae',ha='left',va='center',fontstyle='italic')
plt.text(0.138,11, s='S. paradoxus',ha='left',va='center',fontstyle='italic')
plt.text(0.11,12, s='S. uvarum',ha='left',va='center',fontstyle='italic')
plt.hlines([7],xmin=0,xmax=0.2458,zorder=0,color='#4059AD',linewidth=3)
plt.hlines([8],xmin=0,xmax=0.2410,zorder=0,color='#4059AD',linewidth=3)
plt.hlines([9],xmin=0,xmax=0.204,zorder=0,color='#4059AD',linewidth=3)
plt.hlines([10],xmin=0,xmax=0.1884,zorder=0,color='#4059AD',linewidth=3)
plt.hlines([11],xmin=0,xmax=0.1315,zorder=0,color='#4059AD',linewidth=3)
plt.hlines([12],xmin=0,xmax=0.1036,zorder=0,color='#4059AD',linewidth=3)
plt.scatter([0,0,0,0,0,0],[1,2,3,4,5,6],color='#6C9A8B',edgecolor='black',marker='o',s=100,zorder=100)
plt.scatter([0.2158,0.215,0.204,0.1884,0.1315,0.1036],[1,2,3,4,5,6],color='#6C9A8B',edgecolor='black',marker='o',s=100,zorder=100)
plt.scatter([0,0,0,0,0,0],[7,8,9,10,11,12],color='#4059AD',edgecolor='black',marker='o',s=100,zorder=100)
plt.scatter([0.2458,0.2410,0,0,0,0],[7,8,9,10,11,12],color='#4059AD',edgecolor='black',marker='o',s=100,zorder=100)
plt.yticks([])
plt.ylim(0.5,12.5)
plt.xticks([0,0.05,0.10,0.15,0.20,0.25])
plt.xlabel('genetic distance (dDDH)',weight='bold')
sns.despine(left=True,right=True,bottom=False,trim=True)

plt.savefig('../Figures/Hybrid_GI_genetic_distance.png',bbox_inches='tight',dpi=1000)
