#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 11 14:26:43 2021

@author: devin
"""

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import matplotlib.cm as cm
import matplotlib as mpl
import pickle
font = {'family' : 'sans-serif','weight' : 'normal','size' : 12}
plt.rc('font', **font)
#%%
hybrid_order = pickle.load( open( "../Pickle/hybrid_order.p", "rb" ) )
hybrid_sp={'SRR1119201':['Suva','Scer','Skud'],'SRR1119202':['Suva','Scer','Skud'],'SRR1119203':['Suva','Scer'],'SRR1119204':['Scer','Suva','Seub'],'SRR1649181':['Scer','Seub'],'SRR1649183':['Scer','Seub'],'SRR800854':['Scer','Spar'],'SRR800807':['Scer','Spar'],'SRR800768':['Scer','Spar'],'SRR2967902':['Scer','Spar'],'SRR2967903':['Scer','Spar'],'SRR2967904':['Scer','Skud'],'SRR2967905':['Scer','Smik'],'SRR2967906':['Scer','Suva'],'SRR2967849':['Scer','Skud'],'SRR2967853':['Scer','Skud'],'SRR2967864':['Scer','Skud'],'DRR040649':['Scer','Seub'],'DRR040656':['Scer','Seub'],'DRR040657':['Scer','Seub'],'DRR040637':['Scer','Seub'],'DRR040641':['Scer','Seub'],'DRR040645':['Scer','Seub'],'DRR040658':['Scer','Seub'],'DRR040650':['Scer','Seub'],'DRR040669':['Scer','Suva','Seub'],'DRR040660':['Scer','Seub'],'SRR2968024':['Scer','Skud'],'DRR040667':['Scer','Suva','Seub'],'SRR2968005':['Scer','Skud'],'SRR2967882':['Scer','Suva'],'ERR1111502':['Scer','Spar'],'ERR1111503':['Scer','Spar'],'SRR2967875':['Scer','Skud'],'DRR040654':['Scer','Seub'],'SRR2968042':['Scer','Skud'],'SRR2968046':['Scer','Seub'],'SRR3265414':['Scer','Skud'],'SRR7631525':['Scer','Suva'],'ERR3010122':['Scer','Spar'],'ERR3010128':['Scer','Spar'],'ERR3010129':['Scer','Spar'],'ERR3010125':['Scer','Spar'],'ERR3010126':['Scer','Spar'],'ERR3010127':['Scer','Spar'],'SRR8648839':['Scer','Seub'],'ERR3010130':['Scer','Spar'],'SRR8648841':['Scer','Seub'],'ERR3010131':['Scer','Spar'],'ERR3010132':['Scer','Spar'],'ERR3010133':['Scer','Spar'],'ERR3010134':['Scer','Spar'],'ERR3010135':['Scer','Spar'],'ERR3010136':['Scer','Spar'],'ERR3010137':['Scer','Spar'],'ERR3010138':['Scer','Spar'],'ERR3010139':['Scer','Spar'],'ERR3010140':['Scer','Spar'],'ERR3010143':['Scer','Spar'],'SRR10047154':['Scer','Seub'],'SRR10047336':['Scer','Seub','Skud'],'SRR10047416':['Seub','Suva'],'SRR10047092':['Scer','Seub'],'SRR10047152':['Scer','Seub'],'ERR3010146':['Scer','Spar'],'ERR3010147':['Scer','Spar'],'ERR3010149':['Scer','Spar'],'ERR3010150':['Scer','Spar'],'SRR8799642':['Scer','Seub'],'SRR8799680':['Scer','Seub'],'SRR8799685':['Scer','Seub'],'SRR8799646':['Scer','Seub'],'SRR8799652':['Scer','Seub'],'SRR8799686':['Scer','Seub'],'SRR8799647':['Scer','Seub'],'SRR8799692':['Scer','Seub'],'SRR8799694':['Scer','Seub'],'SRR8799654':['Scer','Seub'],'SRR8799683':['Scer','Seub'],'SRR8799682':['Scer','Seub'],'SRR8799666':['Scer','Seub'],'SRR8799653':['Scer','Seub'],'SRR8799631':['Scer','Seub'],'SRR8799695':['Scer','Seub'],'SRR8799649':['Scer','Seub'],'SRR8799645':['Scer','Seub'],'SRR8799681':['Scer','Seub'],'SRR8799684':['Scer','Seub'],'SRR8799676':['Scer','Seub'],'SRR8799687':['Scer','Seub'],'SRR8799691':['Scer','Seub'],'SRR8799693':['Scer','Seub'],'SRR8799659':['Scer','Seub'],'SRR8799643':['Scer','Seub'],'SRR8799644':['Scer','Seub'],'SRR8799662':['Scer','Seub'],'SRR8799663':['Scer','Seub'],'SRR8799634':['Scer','Seub'],'SRR8799640':['Scer','Seub'],'SRR8799671':['Scer','Seub'],'SRR8799667':['Scer','Seub'],'SRR8799637':['Scer','Seub'],'SRR8799690':['Scer','Seub'],'SRR8799673':['Scer','Skud'],'SRR8799672':['Scer','Skud'],'SRR8799674':['Scer','Skud'],'SRR8799650':['Scer','Skud'],'SRR8799633':['Scer','Skud'],'SRR8799677':['Scer','Skud'],'SRR8799678':['Scer','Skud'],'SRR8799651':['Scer','Skud'],'SRR8799679':['Scer','Skud'],'SRR8799657':['Suva','Seub'],'SRR8799661':['Suva','Seub'],'SRR8799665':['Suva','Seub'],'SRR8799698':['Suva','Seub'],'SRR8799660':['Suva','Seub'],'SRR8799658':['Suva','Seub'],'SRR8799655':['Suva','Seub'],'SRR8799668':['Suva','Seub'],'SRR8799641':['Suva','Seub'],'SRR8799656':['Suva','Seub'],'SRR8799630':['Suva','Seub'],'SRR8799629':['Suva','Seub'],'SRR8799664':['Suva','Seub'],'SRR10035841':['Scer','Skud'],'SRR10047007':['Scer','Skud'],'SRR10047062':['Scer','Seub'],'SRR10047068':['Scer','Seub'],'SRR10047077':['Scer','Seub'],'SRR10047225':['Scer','Seub'],'SRR10047243':['Scer','Seub','Suva'],'SRR10047261':['Scer','Seub'],'SRR10047170':['Scer','Seub'],'SRR10047181':['Scer','Seub'],'SRR10047231':['Scer','Skud'],'SRR10047188':['Scer','Seub'],'SRR10047256':['Scer','Seub'],'SRR10047124':['Scer','Seub'],'SRR10047019':['Seub','Suva'],'SRR10047000':['Scer','Seub'],'SRR10047191':['Scer','Seub'],'SRR10047313':['Scer','Seub'],'SRR10047163':['Scer','Seub'],'SRR10047100':['Scer','Seub'],'SRR10047089':['Scer','Seub'],'SRR10047028':['Scer','Skud'],'SRR10047024':['Scer','Seub'],'SRR10047374':['Scer','Seub'],'SRR10047307':['Scer','Seub'],'SRR10047017':['Scer','Seub'],'SRR10047241':['Scer','Seub'],'SRR10047295':['Seub','Suva'],'SRR10047331':['Seub','Suva'],'SRR10047328':['Seub','Suva'],'SRR10047359':['Seub','Suva'],'SRR10047351':['Seub','Suva'],'SRR10047335':['Seub','Suva'],'SRR10035842':['Seub','Suva'],'SRR10047405':['Seub','Suva'],'SRR10047402':['Seub','Suva'],'SRR10047389':['Seub','Suva'],'SRR10047380':['Seub','Suva'],'SRR10047371':['Seub','Suva'],'SRR10047378':['Seub','Suva'],'SRR10047356':['Scer','Seub','Skud','Suva'],'SRR10047345':['Seub','Suva'],'SRR10047410':['Scer','Seub','Skud','Suva'],'SRR10047383':['Scer','Seub','Skud','Suva'],'SRR10047395':['Seub','Suva'],'SRR10047344':['Seub','Suva'],'SRR10047362':['Scer','Seub'],'SRR10047189':['Scer','Seub'],'SRR10047157':['Scer','Seub'],'SRR10047176':['Scer','Seub'],'SRR10047149':['Scer','Seub'],'SRR10047146':['Scer','Seub'],'SRR10047122':['Scer','Skud'],'SRR10047102':['Scer','Seub'],'SRR10047096':['Scer','Seub'],'SRR10047094':['Scer','Seub'],'SRR10047057':['Scer','Seub'],'SRR10046945':['Scer','Seub'],'SRR10047365':['Scer','Seub','Suva'],'SRR10046950':['Seub','Suva'],'SRR10046952':['Seub','Suva'],'SRR10046972':['Scer','Skud'],'SRR10046976':['Scer','Seub'],'SRR5141258':['Scer','Seub'],'SRR5141260':['Scer','Seub'],'SRR12703370':['Scer','Seub'],'SRR12703369':['Scer','Seub'],'SRR12703368':['Scer','Seub'],'SRR12703367':['Scer','Seub'],'SRR12703366':['Scer','Seub'],'SRR5251559':['Scer','Suva'],'SRR10208681':['Scer','Seub'],'SRR9925228':['Scer','Skud'],'SRR9925227':['Scer','Skud'],'SRR9925226':['Scer','Skud'],'SRR9925224':['Scer','Skud'],'SRR9925223':['Scer','Skud'],'SRR9925222':['Scer','Skud'],'SRR9925225':['Scer','Skud']}
species=['Scer','Spar','Smik','Sjur','Skud','Sarb','Seub','Suva']
chro=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33]
genomes=['ERR1111503','ERR1111502','SRR2967902','ERR3010130','SRR2967903','SRR800807','SRR800768','ERR3010131','ERR3010132','ERR3010133','ERR3010134','ERR3010135','ERR3010136','ERR3010137','ERR3010138','ERR3010139','ERR3010140','ERR3010143','SRR800854','ERR3010122','ERR3010128','ERR3010129','ERR3010125','ERR3010126','ERR3010127','ERR3010146','ERR3010147','ERR3010149','ERR3010150','SRR2967905','SRR8799672','SRR10047028','SRR2968005','SRR8799673','SRR8799674','SRR8799678','SRR10035841','SRR2967904','SRR2967849','SRR2967853','SRR2967864','SRR2968024','SRR2967875','SRR2968042','SRR3265414','SRR8799650','SRR8799633','SRR8799677','SRR8799651','SRR8799679','SRR9925228','SRR9925227','SRR9925226','SRR9925224','SRR9925223','SRR9925222','SRR9925225','SRR10047154','DRR040657','DRR040645','DRR040654','SRR8648841','SRR10047092','DRR040649','DRR040656','DRR040637','DRR040641','DRR040658','DRR040650','DRR040660','SRR2968046','SRR8648839','SRR1649181','SRR1649183','SRR10047152','SRR8799642','SRR8799680','SRR8799685','SRR8799646','SRR8799652','SRR8799686','SRR8799647','SRR8799692','SRR8799694','SRR8799654','SRR8799683','SRR8799682','SRR8799666','SRR8799653','SRR8799631','SRR8799695','SRR8799649','SRR8799645','SRR8799681','SRR8799684','SRR8799676','SRR8799687','SRR8799691','SRR8799693','SRR8799659','SRR8799643','SRR8799644','SRR8799662','SRR8799663','SRR8799634','SRR8799640','SRR8799671','SRR8799667','SRR8799637','SRR8799690','SRR10047062','SRR10047068','SRR10047077','SRR10047225','SRR10047261','SRR10047170','SRR10047181','SRR10047188','SRR10047256','SRR10047124','SRR10047000','SRR10047191','SRR10047313','SRR10047163','SRR10047100','SRR10047089','SRR10047024','SRR10047374','SRR10047307','SRR10047017','SRR10047241','SRR10047362','SRR10047189','SRR10047157','SRR10047176','SRR10047149','SRR10047146','SRR10047102','SRR10047096','SRR10047094','SRR10047057','SRR10046945','SRR10046976','SRR5141258','SRR5141260','SRR12703370','SRR12703369','SRR12703368','SRR12703367','SRR12703366','SRR10208681','SRR2967882','SRR2967906','SRR5251559','SRR1119203','SRR10047416','DRR040667','DRR040669','SRR8799657','SRR8799661','SRR8799665','SRR8799698','SRR8799660','SRR8799658','SRR8799655','SRR8799668','SRR8799641','SRR8799656','SRR8799630','SRR8799629','SRR8799664','SRR10047019','SRR10047295','SRR10047331','SRR10047328','SRR10047359','SRR10047351','SRR10047335','SRR10035842','SRR10047405','SRR10047402','SRR10047389','SRR10047380','SRR10047371','SRR10047378','SRR10047345','SRR10047395','SRR10047344','SRR10046950','SRR10046952','SRR1119204','SRR10047336','SRR10047231','SRR10047356','SRR10047007','SRR10047410','SRR10047383','SRR10046972','SRR10047243','SRR10047365','SRR1119201','SRR7631525','SRR1119202','SRR10047122']
species_num={'Scer':1,'Spar':2,'Smik':3,'Sjur':4,'Skud':5,'Sarb':6,'Seub':7,'Suva':8}
colors={'Scer':"#015482",'Spar':"#dc4d01",'Smik':"#ffda03",'Sjur':'#a2653e','Skud':'#2b5d34','Sarb':'gray','Seub':'#8c000f','Suva':'#4b006e'}

#%%
species_MQ={'Scer':20,'Spar':22,'Smik':24,'Sjur':26,'Skud':28,'Sarb':30,'Seub':32,'Suva':34}
species_MQ={'Scer':1,'Spar':3,'Smik':5,'Sjur':7,'Skud':9,'Sarb':11,'Seub':13,'Suva':15}
plt.figure(figsize=[10,8])
plt.subplot(141)
plt.vlines([1,2,3,4,5,6,7,8],0,205,zorder=0,color='slategray',linestyles='--')
plt.subplot(142)
plt.vlines([1,2,3,4,5,6,7,8],0,205,zorder=0,color='slategray',linestyles='--')
plt.subplot(143)
plt.vlines([1,2,3,4,5,6,7,8],0,205,zorder=0,color='slategray',linestyles='--')
plt.subplot(144)
plt.vlines([1,2,3,4,5,6,7,8],0,205,zorder=0,color='slategray',linestyles='--')

species_count={'Scer':0,'Spar':0,'Smik':0,'Sjur':0,'Skud':0,'Sarb':0,'Seub':0,'Suva':0}
blank=[0,1,2]
n=204
for i in hybrid_order:
    if i not in blank:
        data=pd.read_csv('../DATA/sppIDer/nuclear/nuclear_chi/{}_MQ_chiSquared.txt'.format(i), sep='\t',names=['chi'],skiprows=[0])
    if i not in blank and n>=153:
        plt.subplot(141)
        for j in hybrid_sp[i]:
            plt.scatter(species_num[j],n,linewidth=1.5,edgecolor='black',color='white',s=65)
        for k in species_MQ:
            if float(data.loc[species_MQ[k],'chi'])>0:
                plt.scatter(species_num[k],n,linewidth=1.5,edgecolor='black',color=colors[k],s=65)
                species_count[k]+=1
        n-=1
    elif i not in blank and n>=102:
        plt.subplot(142)
        for j in hybrid_sp[i]:
            plt.scatter(species_num[j],n,linewidth=1.5,edgecolor='black',color='white',s=65)
        for k in species_MQ:
            if float(data.loc[species_MQ[k],'chi'])>0:
                plt.scatter(species_num[k],n,linewidth=1.5,edgecolor='black',color=colors[k],s=65)
                species_count[k]+=1
        n-=1
    elif i not in blank and n>=52:
        plt.subplot(143)
        for j in hybrid_sp[i]:
            plt.scatter(species_num[j],n,linewidth=1.5,edgecolor='black',color='white',s=65)
        for k in species_MQ:
            if float(data.loc[species_MQ[k],'chi'])>0:
                plt.scatter(species_num[k],n,linewidth=1.5,edgecolor='black',color=colors[k],s=65)
                species_count[k]+=1
        n-=1
    elif i not in blank:
        plt.subplot(144)
        for j in hybrid_sp[i]:
            plt.scatter(species_num[j],n,linewidth=1.5,edgecolor='black',color='white',s=65)
        for k in species_MQ:
            if float(data.loc[species_MQ[k],'chi'])>0:
                plt.scatter(species_num[k],n,linewidth=1.5,edgecolor='black',color=colors[k],s=65)
                species_count[k]+=1
        n-=1

plt.subplot(141)
plt.ylabel('interspecific hybrids',weight='bold')
plt.yticks(range(152,205),[])
plt.xticks([1,2,3,4,5,6,7,8],['Scer','Spar','Smik','Sjur','Skud','Sarb','Seub','Suva'],rotation=60)
plt.ylim(152,205)
plt.xlim(0.5,8.5)
plt.subplot(142)
plt.ylabel('interspecific hybrids',weight='bold')
plt.xticks([1,2,3,4,5,6,7,8],['Scer','Spar','Smik','Sjur','Skud','Sarb','Seub','Suva'],rotation=60)
plt.yticks(range(101,153),[])
plt.ylim(101,153)
plt.xlim(0.5,8.5)
plt.subplot(143)
plt.ylabel('interspecific hybrids',weight='bold')
plt.xticks([1,2,3,4,5,6,7,8],['Scer','Spar','Smik','Sjur','Skud','Sarb','Seub','Suva'],rotation=60)
plt.yticks(range(51,102),[])
plt.ylim(51,102)
plt.xlim(0.5,8.5)
plt.subplot(144)
plt.ylabel('interspecific hybrids',weight='bold')
plt.xticks([1,2,3,4,5,6,7,8],['Scer','Spar','Smik','Sjur','Skud','Sarb','Seub','Suva'],rotation=60)
plt.yticks(range(0,52),[])
plt.ylim(0,52)
plt.xlim(0.5,8.5)
plt.tight_layout()
plt.savefig('../Figures/Hybrid_GI_MQ_confirm.png',bbox_inches='tight',dpi=1000,transparent=True)