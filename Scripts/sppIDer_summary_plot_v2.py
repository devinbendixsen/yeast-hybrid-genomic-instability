#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  1 10:50:58 2021

@author: devin
"""
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import matplotlib.cm as cm
import matplotlib as mpl
import pickle
import scipy.stats
font = {'family' : 'sans-serif','weight' : 'normal','size' : 12}
plt.rc('font', **font)

#%%import pickle
hybrid_order = pickle.load( open( "../Pickle/hybrid_order.p", "rb" ) )
map_filt = pickle.load( open( "../Pickle/map_filt.p", "rb" ) )
#%%
species=['Scer','Spar','Smik','Sjur','Skud','Sarb','Seub','Suva']
genomes=['ERR1111503','ERR1111502','SRR2967902','ERR3010130','SRR2967903','SRR800807','SRR800768','ERR3010131','ERR3010132','ERR3010133','ERR3010134','ERR3010135','ERR3010136','ERR3010137','ERR3010138','ERR3010139','ERR3010140','ERR3010143','SRR800854','ERR3010122','ERR3010128','ERR3010129','ERR3010125','ERR3010126','ERR3010127','ERR3010146','ERR3010147','ERR3010149','ERR3010150','SRR2967905','SRR8799672','SRR10047028','SRR2968005','SRR8799673','SRR8799674','SRR8799678','SRR10035841','SRR2967904','SRR2967849','SRR2967853','SRR2967864','SRR2968024','SRR2967875','SRR2968042','SRR3265414','SRR8799650','SRR8799633','SRR8799677','SRR8799651','SRR8799679','SRR9925228','SRR9925227','SRR9925226','SRR9925224','SRR9925223','SRR9925222','SRR9925225','SRR10047154','DRR040657','DRR040645','DRR040654','SRR8648841','SRR10047092','DRR040649','DRR040656','DRR040637','DRR040641','DRR040658','DRR040650','DRR040660','SRR2968046','SRR8648839','SRR1649181','SRR1649183','SRR10047152','SRR8799642','SRR8799680','SRR8799685','SRR8799646','SRR8799652','SRR8799686','SRR8799647','SRR8799692','SRR8799694','SRR8799654','SRR8799683','SRR8799682','SRR8799666','SRR8799653','SRR8799631','SRR8799695','SRR8799649','SRR8799645','SRR8799681','SRR8799684','SRR8799676','SRR8799687','SRR8799691','SRR8799693','SRR8799659','SRR8799643','SRR8799644','SRR8799662','SRR8799663','SRR8799634','SRR8799640','SRR8799671','SRR8799667','SRR8799637','SRR8799690','SRR10047062','SRR10047068','SRR10047077','SRR10047225','SRR10047261','SRR10047170','SRR10047181','SRR10047188','SRR10047256','SRR10047124','SRR10047000','SRR10047191','SRR10047313','SRR10047163','SRR10047100','SRR10047089','SRR10047024','SRR10047374','SRR10047307','SRR10047017','SRR10047241','SRR10047362','SRR10047189','SRR10047157','SRR10047176','SRR10047149','SRR10047146','SRR10047102','SRR10047096','SRR10047094','SRR10047057','SRR10046945','SRR10046976','SRR5141258','SRR5141260','SRR12703370','SRR12703369','SRR12703368','SRR12703367','SRR12703366','SRR10208681','SRR2967882','SRR2967906','SRR5251559','SRR1119203','SRR10047416','DRR040667','DRR040669','SRR8799657','SRR8799661','SRR8799665','SRR8799698','SRR8799660','SRR8799658','SRR8799655','SRR8799668','SRR8799641','SRR8799656','SRR8799630','SRR8799629','SRR8799664','SRR10047019','SRR10047295','SRR10047331','SRR10047328','SRR10047359','SRR10047351','SRR10047335','SRR10035842','SRR10047405','SRR10047402','SRR10047389','SRR10047380','SRR10047371','SRR10047378','SRR10047345','SRR10047395','SRR10047344','SRR10046950','SRR10046952','SRR1119204','SRR10047336','SRR10047231','SRR10047356','SRR10047007','SRR10047410','SRR10047383','SRR10046972','SRR10047243','SRR10047365','SRR1119201','SRR7631525','SRR1119202','SRR10047122']
data_mapped=pd.DataFrame(columns=species,index=hybrid_order)
data_mapped=data_mapped.astype(float)
#%%

for i in genomes:
    data=pd.read_csv('../DATA/sppIDer/nuclear/nuclear_summary/{}_MQsummary.txt'.format(i), sep='\t',skiprows=6,header = None,index_col=0,names=['total','%reads','%nonzero','aveMQ','nonzeroMQ','medianMQ','nonzeromedMQ'])
    data['%nonzero'] = data['%nonzero'].map(lambda x: x.rstrip('%'))
    data['%nonzero'] = data['%nonzero'].astype(float)
    for j in species:
        data_mapped.loc[i,j]=data.loc[j,'%nonzero']

#%%
label={'Scer':'S. cerevisiae','Spar':'S. paradoxus','Smik':'S. mikatae','Sjur':'S. jurei','Skud':'S. kudriavzevii','Sarb':'S. arboricola','Seub':'S. eubayanus','Suva':'S. uvarum','Unmapped':'unmapped'}
colors={'Scer':"#015482",'Spar':"#dc4d01",'Smik':"#ffda03",'Sjur':'#a2653e','Skud':'#2b5d34','Sarb':'gray','Seub':'#8c000f','Suva':'#4b006e'}
plot_loc={'Scer':811,'Spar':812,'Smik':813,'Sjur':814,'Skud':815,'Sarb':816,'Seub':817,'Suva':818}

plt.figure(figsize=[10,6])
for i in colors:
    plt.subplot(plot_loc[i])
    x=pd.DataFrame(data_mapped[i])
    x_t=x.transpose()
    sns.heatmap(x_t,cmap=sns.light_palette(colors[i], as_cmap=True),vmin=0,vmax=100,cbar=False,linewidths=0)
    plt.xticks([])
    plt.yticks([])
plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=0.25)
plt.savefig('../Figures/Hybrid_GI_mapped.png',bbox_inches='tight',dpi=1000) 

pickle.dump( data_mapped, open( "../Pickle/data_mapped.p", "wb" ) )
#%% Plot S1 vs S2
alphnum=1
plt.figure(figsize=[5,5])
hybrid_sp={'SRR1119201':['Suva','Scer','Skud'],'SRR1119202':['Suva','Scer','Skud'],'SRR1119203':['Suva','Scer'],'SRR1119204':['Scer','Suva','Seub'],'SRR1649181':['Scer','Seub'],'SRR1649183':['Scer','Seub'],'SRR800854':['Scer','Spar'],'SRR800807':['Scer','Spar'],'SRR800768':['Scer','Spar'],'SRR2967902':['Scer','Spar'],'SRR2967903':['Scer','Spar'],'SRR2967904':['Scer','Skud'],'SRR2967905':['Scer','Smik'],'SRR2967906':['Scer','Suva'],'SRR2967849':['Scer','Skud'],'SRR2967853':['Scer','Skud'],'SRR2967864':['Scer','Skud'],'DRR040649':['Scer','Seub'],'DRR040656':['Scer','Seub'],'DRR040657':['Scer','Seub'],'DRR040637':['Scer','Seub'],'DRR040641':['Scer','Seub'],'DRR040645':['Scer','Seub'],'DRR040658':['Scer','Seub'],'DRR040650':['Scer','Seub'],'DRR040669':['Scer','Suva','Seub'],'DRR040660':['Scer','Seub'],'SRR2968024':['Scer','Skud'],'DRR040667':['Scer','Suva','Seub'],'SRR2968005':['Scer','Skud'],'SRR2967882':['Scer','Suva'],'ERR1111502':['Scer','Spar'],'ERR1111503':['Scer','Spar'],'SRR2967875':['Scer','Skud'],'DRR040654':['Scer','Seub'],'SRR2968042':['Scer','Skud'],'SRR2968046':['Scer','Seub'],'SRR3265414':['Scer','Skud'],'SRR7631525':['Scer','Suva'],'ERR3010122':['Scer','Spar'],'ERR3010128':['Scer','Spar'],'ERR3010129':['Scer','Spar'],'ERR3010125':['Scer','Spar'],'ERR3010126':['Scer','Spar'],'ERR3010127':['Scer','Spar'],'SRR8648839':['Scer','Seub'],'ERR3010130':['Scer','Spar'],'SRR8648841':['Scer','Seub'],'ERR3010131':['Scer','Spar'],'ERR3010132':['Scer','Spar'],'ERR3010133':['Scer','Spar'],'ERR3010134':['Scer','Spar'],'ERR3010135':['Scer','Spar'],'ERR3010136':['Scer','Spar'],'ERR3010137':['Scer','Spar'],'ERR3010138':['Scer','Spar'],'ERR3010139':['Scer','Spar'],'ERR3010140':['Scer','Spar'],'ERR3010143':['Scer','Spar'],'SRR10047154':['Scer','Seub'],'SRR10047336':['Scer','Seub','Skud'],'SRR10047416':['Seub','Suva'],'SRR10047092':['Scer','Seub'],'SRR10047152':['Scer','Seub'],'ERR3010146':['Scer','Spar'],'ERR3010147':['Scer','Spar'],'ERR3010149':['Scer','Spar'],'ERR3010150':['Scer','Spar'],'SRR8799642':['Scer','Seub'],'SRR8799680':['Scer','Seub'],'SRR8799685':['Scer','Seub'],'SRR8799646':['Scer','Seub'],'SRR8799652':['Scer','Seub'],'SRR8799686':['Scer','Seub'],'SRR8799647':['Scer','Seub'],'SRR8799692':['Scer','Seub'],'SRR8799694':['Scer','Seub'],'SRR8799654':['Scer','Seub'],'SRR8799683':['Scer','Seub'],'SRR8799682':['Scer','Seub'],'SRR8799666':['Scer','Seub'],'SRR8799653':['Scer','Seub'],'SRR8799631':['Scer','Seub'],'SRR8799695':['Scer','Seub'],'SRR8799649':['Scer','Seub'],'SRR8799645':['Scer','Seub'],'SRR8799681':['Scer','Seub'],'SRR8799684':['Scer','Seub'],'SRR8799676':['Scer','Seub'],'SRR8799687':['Scer','Seub'],'SRR8799691':['Scer','Seub'],'SRR8799693':['Scer','Seub'],'SRR8799659':['Scer','Seub'],'SRR8799643':['Scer','Seub'],'SRR8799644':['Scer','Seub'],'SRR8799662':['Scer','Seub'],'SRR8799663':['Scer','Seub'],'SRR8799634':['Scer','Seub'],'SRR8799640':['Scer','Seub'],'SRR8799671':['Scer','Seub'],'SRR8799667':['Scer','Seub'],'SRR8799637':['Scer','Seub'],'SRR8799690':['Scer','Seub'],'SRR8799673':['Scer','Skud'],'SRR8799672':['Scer','Skud'],'SRR8799674':['Scer','Skud'],'SRR8799650':['Scer','Skud'],'SRR8799633':['Scer','Skud'],'SRR8799677':['Scer','Skud'],'SRR8799678':['Scer','Skud'],'SRR8799651':['Scer','Skud'],'SRR8799679':['Scer','Skud'],'SRR8799657':['Suva','Seub'],'SRR8799661':['Suva','Seub'],'SRR8799665':['Suva','Seub'],'SRR8799698':['Suva','Seub'],'SRR8799660':['Suva','Seub'],'SRR8799658':['Suva','Seub'],'SRR8799655':['Suva','Seub'],'SRR8799668':['Suva','Seub'],'SRR8799641':['Suva','Seub'],'SRR8799656':['Suva','Seub'],'SRR8799630':['Suva','Seub'],'SRR8799629':['Suva','Seub'],'SRR8799664':['Suva','Seub'],'SRR10035841':['Scer','Skud'],'SRR10047007':['Scer','Skud'],'SRR10047062':['Scer','Seub'],'SRR10047068':['Scer','Seub'],'SRR10047077':['Scer','Seub'],'SRR10047225':['Scer','Seub'],'SRR10047243':['Scer','Seub','Suva'],'SRR10047261':['Scer','Seub'],'SRR10047170':['Scer','Seub'],'SRR10047181':['Scer','Seub'],'SRR10047231':['Scer','Skud'],'SRR10047188':['Scer','Seub'],'SRR10047256':['Scer','Seub'],'SRR10047124':['Scer','Seub'],'SRR10047019':['Seub','Suva'],'SRR10047000':['Scer','Seub'],'SRR10047191':['Scer','Seub'],'SRR10047313':['Scer','Seub'],'SRR10047163':['Scer','Seub'],'SRR10047100':['Scer','Seub'],'SRR10047089':['Scer','Seub'],'SRR10047028':['Scer','Skud'],'SRR10047024':['Scer','Seub'],'SRR10047374':['Scer','Seub'],'SRR10047307':['Scer','Seub'],'SRR10047017':['Scer','Seub'],'SRR10047241':['Scer','Seub'],'SRR10047295':['Seub','Suva'],'SRR10047331':['Seub','Suva'],'SRR10047328':['Seub','Suva'],'SRR10047359':['Seub','Suva'],'SRR10047351':['Seub','Suva'],'SRR10047335':['Seub','Suva'],'SRR10035842':['Seub','Suva'],'SRR10047405':['Seub','Suva'],'SRR10047402':['Seub','Suva'],'SRR10047389':['Seub','Suva'],'SRR10047380':['Seub','Suva'],'SRR10047371':['Seub','Suva'],'SRR10047378':['Seub','Suva'],'SRR10047356':['Scer','Seub','Skud','Suva'],'SRR10047345':['Seub','Suva'],'SRR10047410':['Scer','Seub','Skud','Suva'],'SRR10047383':['Scer','Seub','Skud','Suva'],'SRR10047395':['Seub','Suva'],'SRR10047344':['Seub','Suva'],'SRR10047362':['Scer','Seub'],'SRR10047189':['Scer','Seub'],'SRR10047157':['Scer','Seub'],'SRR10047176':['Scer','Seub'],'SRR10047149':['Scer','Seub'],'SRR10047146':['Scer','Seub'],'SRR10047122':['Scer','Skud'],'SRR10047102':['Scer','Seub'],'SRR10047096':['Scer','Seub'],'SRR10047094':['Scer','Seub'],'SRR10047057':['Scer','Seub'],'SRR10046945':['Scer','Seub'],'SRR10047365':['Scer','Seub','Suva'],'SRR10046950':['Seub','Suva'],'SRR10046952':['Seub','Suva'],'SRR10046972':['Scer','Skud'],'SRR10046976':['Scer','Seub'],'SRR5141258':['Scer','Seub'],'SRR5141260':['Scer','Seub'],'SRR12703370':['Scer','Seub'],'SRR12703369':['Scer','Seub'],'SRR12703368':['Scer','Seub'],'SRR12703367':['Scer','Seub'],'SRR12703366':['Scer','Seub'],'SRR5251559':['Scer','Suva'],'SRR10208681':['Scer','Seub'],'SRR9925228':['Scer','Skud'],'SRR9925227':['Scer','Skud'],'SRR9925226':['Scer','Skud'],'SRR9925224':['Scer','Skud'],'SRR9925223':['Scer','Skud'],'SRR9925222':['Scer','Skud'],'SRR9925225':['Scer','Skud']}
for i in hybrid_sp:
    if len(hybrid_sp[i])==2 and i in map_filt:
        if 'Scer' in hybrid_sp[i] and 'Spar' in hybrid_sp[i]:
            if data_mapped.loc[i,'Scer']>data_mapped.loc[i,'Spar']:
                plt.scatter(data_mapped.loc[i,'Spar'],data_mapped.loc[i,'Scer'],color='#3D405B',edgecolor='black',alpha=alphnum)
            elif data_mapped.loc[i,'Spar']>data_mapped.loc[i,'Scer']:
                plt.scatter(data_mapped.loc[i,'Scer'],data_mapped.loc[i,'Spar'],color='#3D405B',edgecolor='black',alpha=alphnum)
        if 'Scer' in hybrid_sp[i] and 'Smik' in hybrid_sp[i]:
            if data_mapped.loc[i,'Scer']>data_mapped.loc[i,'Smik']:
                plt.scatter(data_mapped.loc[i,'Smik'],data_mapped.loc[i,'Scer'],color='white',edgecolor='black',alpha=alphnum)
            elif data_mapped.loc[i,'Smik']>data_mapped.loc[i,'Scer']:
                plt.scatter(data_mapped.loc[i,'Scer'],data_mapped.loc[i,'Smik'],color='white',edgecolor='black',alpha=alphnum)
        if 'Scer' in hybrid_sp[i] and 'Skud' in hybrid_sp[i]:
            if data_mapped.loc[i,'Scer']>data_mapped.loc[i,'Skud']:
                plt.scatter(data_mapped.loc[i,'Skud'],data_mapped.loc[i,'Scer'],color='#81B29A',edgecolor='black',alpha=alphnum)
            elif data_mapped.loc[i,'Skud']>data_mapped.loc[i,'Scer']:
                plt.scatter(data_mapped.loc[i,'Scer'],data_mapped.loc[i,'Skud'],color='#81B29A',edgecolor='black',alpha=alphnum)
        if 'Scer' in hybrid_sp[i] and 'Seub' in hybrid_sp[i]:
            if data_mapped.loc[i,'Scer']>data_mapped.loc[i,'Seub']:
                plt.scatter(data_mapped.loc[i,'Seub'],data_mapped.loc[i,'Scer'],color='#E07A5F',edgecolor='black',alpha=alphnum)
            elif data_mapped.loc[i,'Seub']>data_mapped.loc[i,'Scer']:
                plt.scatter(data_mapped.loc[i,'Scer'],data_mapped.loc[i,'Seub'],color='#E07A5F',edgecolor='black',alpha=alphnum)
        if 'Scer' in hybrid_sp[i] and 'Suva' in hybrid_sp[i]:
            if data_mapped.loc[i,'Scer']>data_mapped.loc[i,'Suva']:
                plt.scatter(data_mapped.loc[i,'Suva'],data_mapped.loc[i,'Scer'],color='#F2CC8F',edgecolor='black',alpha=alphnum)
            elif data_mapped.loc[i,'Suva']>data_mapped.loc[i,'Scer']:
                plt.scatter(data_mapped.loc[i,'Scer'],data_mapped.loc[i,'Suva'],color='#F2CC8F',edgecolor='black',alpha=alphnum)
        if 'Seub' in hybrid_sp[i] and 'Suva' in hybrid_sp[i]:
            if data_mapped.loc[i,'Seub']>data_mapped.loc[i,'Suva']:
                plt.scatter(data_mapped.loc[i,'Suva'],data_mapped.loc[i,'Seub'],color='peru',edgecolor='black',alpha=alphnum)
            elif data_mapped.loc[i,'Suva']>data_mapped.loc[i,'Seub']:
                plt.scatter(data_mapped.loc[i,'Seub'],data_mapped.loc[i,'Suva'],color='peru',edgecolor='black',alpha=alphnum)
plt.xlabel('dominant species 2 (%)',weight='bold')
plt.ylabel('dominant species 1 (%)',weight='bold')
plt.ylim(28,100)
sns.despine(trim=True)
plt.savefig('../Figures/Hybrid_GI_S1vsS2.svg',bbox_inches='tight',dpi=1000)
#%%
plot_loc={'Scer_Spar':231,'Scer_Smik':232, 'Scer_Skud':233,'Scer_Seub':234,'Scer_Suva':235,'Seub_Suva':236}
plt.figure(figsize=[7.5,5])
for i in hybrid_sp:
    if len(hybrid_sp[i])==2 and i in map_filt:
        if 'Scer' in hybrid_sp[i] and 'Spar' in hybrid_sp[i]:
            plt.subplot(plot_loc['Scer_Spar'])
            plt.scatter(data_mapped.loc[i,'Spar'],data_mapped.loc[i,'Scer'],color='#3D405B',edgecolor='black',alpha=alphnum)
            plt.ylabel('S. cerevisiae',fontstyle='italic')
            plt.xlabel('S. paradoxus',fontstyle='italic')
        if 'Scer' in hybrid_sp[i] and 'Smik' in hybrid_sp[i]:
            plt.subplot(plot_loc['Scer_Smik'])
            plt.scatter(data_mapped.loc[i,'Smik'],data_mapped.loc[i,'Scer'],color='white',edgecolor='black',alpha=alphnum)
            plt.ylabel('S. cerevisiae',fontstyle='italic')
            plt.xlabel('S. mikatae',fontstyle='italic')
        if 'Scer' in hybrid_sp[i] and 'Skud' in hybrid_sp[i]:
            plt.subplot(plot_loc['Scer_Skud'])
            plt.scatter(data_mapped.loc[i,'Skud'],data_mapped.loc[i,'Scer'],color='#81B29A',edgecolor='black',alpha=alphnum)
            plt.ylabel('S. cerevisiae',fontstyle='italic')
            plt.xlabel('S. kudriavzevii',fontstyle='italic')
        if 'Scer' in hybrid_sp[i] and 'Seub' in hybrid_sp[i]:
            plt.subplot(plot_loc['Scer_Seub'])
            plt.scatter(data_mapped.loc[i,'Seub'],data_mapped.loc[i,'Scer'],color='#E07A5F',edgecolor='black',alpha=alphnum)
            plt.ylabel('S. cerevisiae',fontstyle='italic')
            plt.xlabel('S. eubayanus',fontstyle='italic')
        if 'Scer' in hybrid_sp[i] and 'Suva' in hybrid_sp[i]:
            plt.subplot(plot_loc['Scer_Suva'])
            plt.scatter(data_mapped.loc[i,'Suva'],data_mapped.loc[i,'Scer'],color='#F2CC8F',edgecolor='black',alpha=alphnum)
            plt.ylabel('S. cerevisiae',fontstyle='italic')
            plt.xlabel('S. uvarum',fontstyle='italic')
        if 'Seub' in hybrid_sp[i] and 'Suva' in hybrid_sp[i]:
            plt.subplot(plot_loc['Seub_Suva'])
            plt.scatter(data_mapped.loc[i,'Suva'],data_mapped.loc[i,'Seub'],color='peru',edgecolor='black',alpha=alphnum)
            plt.ylabel('S. eubayanus',fontstyle='italic')
            plt.xlabel('S. uvarum',fontstyle='italic')
plt.tight_layout()
plt.savefig('../Figures/Hybrid_GI_S1vsS2_subplots.svg',bbox_inches='tight',dpi=1000)

#%% MITOCHONDRIA
species=['Scer_mito','Spar_mito','Smik_mito','Sjur_mito','Skud_mito','Sarb_mito','Seub_mito','Suva_mito']
species2=['Scer_mito','Spar_mito','Smik_mito','Sjur_mito','Skud_mito','Sarb_mito','Seub_mito_CD','Seub_mito_FM','Suva_mito']
data_mitomapped=pd.DataFrame(columns=species,index=hybrid_order)
data_mitomapped=data_mitomapped.astype(float)
Seub_FM=[]
Seub_CD=[]
for i in genomes:
    data=pd.read_csv('../DATA/sppIDer/mito/mito_summary/{}_MQsummary.txt'.format(i), sep='\t',skiprows=6,header = None,index_col=0,names=['total','%reads','%nonzero','aveMQ','nonzeroMQ','medianMQ','nonzeromedMQ'])
    data['%nonzero'] = data['%nonzero'].map(lambda x: x.rstrip('%'))
    data['%nonzero'] = data['%nonzero'].astype(float)
    for j in species2:
        if 'Seub' in j:
            if data.loc['Seub_mito_FM','%nonzero'] > data.loc['Seub_mito_CD','%nonzero']:
                data_mitomapped.loc[i,'Seub_mito']=data.loc['Seub_mito_FM','%nonzero']
                Seub_FM.append(i)
            else:
                data_mitomapped.loc[i,'Seub_mito']=data.loc['Seub_mito_CD','%nonzero']
                Seub_CD.append(i)
        else:
            data_mitomapped.loc[i,j]=data.loc[j,'%nonzero']

plot_loc={'Scer_mito':811,'Spar_mito':812,'Smik_mito':813,'Sjur_mito':814,'Skud_mito':815,'Sarb_mito':816,'Seub_mito':817,'Suva_mito':818}
colors={'Scer_mito':"#015482",'Spar_mito':"#dc4d01",'Smik_mito':"#ffda03",'Sjur_mito':'#a2653e','Skud_mito':'#2b5d34','Sarb_mito':'gray','Seub_mito':'#8c000f','Suva_mito':'#4b006e'}

plt.figure(figsize=[10,6])
for i in colors:
    plt.subplot(plot_loc[i])
    x=pd.DataFrame(data_mitomapped[i])
    x_t=x.transpose()
    sns.heatmap(x_t,cmap=sns.light_palette(colors[i], as_cmap=True),vmin=0,vmax=100,cbar=False)
    plt.xticks([])
    plt.yticks([])
plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=0.25)
plt.savefig('../Figures/Hybrid_GI_mitomapped.svg',bbox_inches='tight',dpi=1000)

print('FM',len(set(Seub_FM)))
print('CD',len(set(Seub_CD)))
#%% DOMINANT MITOCHONDRIA
plt.figure(figsize=[10,6/8])
dom_spe={}
dom_num={}
for index, row in data_mitomapped.iterrows():
    x=(max(row))
    for i in species:
        if row[i]==x:
            dom_spe[index]=i
            dom_num[index]=x
x=['mito']
for s in species:
    data_mito_dom=pd.DataFrame(index=x,columns=hybrid_order)
    data_mito_dom=data_mito_dom.astype(float)
    for g in dom_spe:
        if dom_spe[g]==s:
            data_mito_dom.loc['mito',g]=dom_num[g]
    sns.heatmap(data_mito_dom,cmap=sns.light_palette(colors[s], as_cmap=True),vmin=0,vmax=100,cbar=False)
plt.xticks([])
plt.yticks([])
plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=0.25)
plt.savefig('../Figures/Hybrid_GI_mitomapped_dom.svg',bbox_inches='tight',dpi=1000)

#%% MITOCHONDRIAL INHERITANCE PATTERNS
colors={'Scer':"#015482",'Spar':"#dc4d01",'Smik':"#ffda03",'Sjur':'#a2653e','Skud':'#2b5d34','Sarb':'gray','Seub':'#8c000f','Suva':'#4b006e'}
hybrid_sp={'SRR1119201':['Suva','Scer','Skud'],'SRR1119202':['Suva','Scer','Skud'],'SRR1119203':['Suva','Scer'],'SRR1119204':['Scer','Suva','Seub'],'SRR1649181':['Scer','Seub'],'SRR1649183':['Scer','Seub'],'SRR800854':['Scer','Spar'],'SRR800807':['Scer','Spar'],'SRR800768':['Scer','Spar'],'SRR2967902':['Scer','Spar'],'SRR2967903':['Scer','Spar'],'SRR2967904':['Scer','Skud'],'SRR2967905':['Scer','Smik'],'SRR2967906':['Scer','Suva'],'SRR2967849':['Scer','Skud'],'SRR2967853':['Scer','Skud'],'SRR2967864':['Scer','Skud'],'DRR040649':['Scer','Seub'],'DRR040656':['Scer','Seub'],'DRR040657':['Scer','Seub'],'DRR040637':['Scer','Seub'],'DRR040641':['Scer','Seub'],'DRR040645':['Scer','Seub'],'DRR040658':['Scer','Seub'],'DRR040650':['Scer','Seub'],'DRR040669':['Scer','Suva','Seub'],'DRR040660':['Scer','Seub'],'SRR2968024':['Scer','Skud'],'DRR040667':['Scer','Suva','Seub'],'SRR2968005':['Scer','Skud'],'SRR2967882':['Scer','Suva'],'ERR1111502':['Scer','Spar'],'ERR1111503':['Scer','Spar'],'SRR2967875':['Scer','Skud'],'DRR040654':['Scer','Seub'],'SRR2968042':['Scer','Skud'],'SRR2968046':['Scer','Seub'],'SRR3265414':['Scer','Skud'],'SRR7631525':['Scer','Suva'],'ERR3010122':['Scer','Spar'],'ERR3010128':['Scer','Spar'],'ERR3010129':['Scer','Spar'],'ERR3010125':['Scer','Spar'],'ERR3010126':['Scer','Spar'],'ERR3010127':['Scer','Spar'],'SRR8648839':['Scer','Seub'],'ERR3010130':['Scer','Spar'],'SRR8648841':['Scer','Seub'],'ERR3010131':['Scer','Spar'],'ERR3010132':['Scer','Spar'],'ERR3010133':['Scer','Spar'],'ERR3010134':['Scer','Spar'],'ERR3010135':['Scer','Spar'],'ERR3010136':['Scer','Spar'],'ERR3010137':['Scer','Spar'],'ERR3010138':['Scer','Spar'],'ERR3010139':['Scer','Spar'],'ERR3010140':['Scer','Spar'],'ERR3010143':['Scer','Spar'],'SRR10047154':['Scer','Seub'],'SRR10047336':['Scer','Seub','Skud'],'SRR10047416':['Seub','Suva'],'SRR10047092':['Scer','Seub'],'SRR10047152':['Scer','Seub'],'ERR3010146':['Scer','Spar'],'ERR3010147':['Scer','Spar'],'ERR3010149':['Scer','Spar'],'ERR3010150':['Scer','Spar'],'SRR8799642':['Scer','Seub'],'SRR8799680':['Scer','Seub'],'SRR8799685':['Scer','Seub'],'SRR8799646':['Scer','Seub'],'SRR8799652':['Scer','Seub'],'SRR8799686':['Scer','Seub'],'SRR8799647':['Scer','Seub'],'SRR8799692':['Scer','Seub'],'SRR8799694':['Scer','Seub'],'SRR8799654':['Scer','Seub'],'SRR8799683':['Scer','Seub'],'SRR8799682':['Scer','Seub'],'SRR8799666':['Scer','Seub'],'SRR8799653':['Scer','Seub'],'SRR8799631':['Scer','Seub'],'SRR8799695':['Scer','Seub'],'SRR8799649':['Scer','Seub'],'SRR8799645':['Scer','Seub'],'SRR8799681':['Scer','Seub'],'SRR8799684':['Scer','Seub'],'SRR8799676':['Scer','Seub'],'SRR8799687':['Scer','Seub'],'SRR8799691':['Scer','Seub'],'SRR8799693':['Scer','Seub'],'SRR8799659':['Scer','Seub'],'SRR8799643':['Scer','Seub'],'SRR8799644':['Scer','Seub'],'SRR8799662':['Scer','Seub'],'SRR8799663':['Scer','Seub'],'SRR8799634':['Scer','Seub'],'SRR8799640':['Scer','Seub'],'SRR8799671':['Scer','Seub'],'SRR8799667':['Scer','Seub'],'SRR8799637':['Scer','Seub'],'SRR8799690':['Scer','Seub'],'SRR8799673':['Scer','Skud'],'SRR8799672':['Scer','Skud'],'SRR8799674':['Scer','Skud'],'SRR8799650':['Scer','Skud'],'SRR8799633':['Scer','Skud'],'SRR8799677':['Scer','Skud'],'SRR8799678':['Scer','Skud'],'SRR8799651':['Scer','Skud'],'SRR8799679':['Scer','Skud'],'SRR8799657':['Suva','Seub'],'SRR8799661':['Suva','Seub'],'SRR8799665':['Suva','Seub'],'SRR8799698':['Suva','Seub'],'SRR8799660':['Suva','Seub'],'SRR8799658':['Suva','Seub'],'SRR8799655':['Suva','Seub'],'SRR8799668':['Suva','Seub'],'SRR8799641':['Suva','Seub'],'SRR8799656':['Suva','Seub'],'SRR8799630':['Suva','Seub'],'SRR8799629':['Suva','Seub'],'SRR8799664':['Suva','Seub'],'SRR10035841':['Scer','Skud'],'SRR10047007':['Scer','Skud'],'SRR10047062':['Scer','Seub'],'SRR10047068':['Scer','Seub'],'SRR10047077':['Scer','Seub'],'SRR10047225':['Scer','Seub'],'SRR10047243':['Scer','Seub','Suva'],'SRR10047261':['Scer','Seub'],'SRR10047170':['Scer','Seub'],'SRR10047181':['Scer','Seub'],'SRR10047231':['Scer','Skud'],'SRR10047188':['Scer','Seub'],'SRR10047256':['Scer','Seub'],'SRR10047124':['Scer','Seub'],'SRR10047019':['Seub','Suva'],'SRR10047000':['Scer','Seub'],'SRR10047191':['Scer','Seub'],'SRR10047313':['Scer','Seub'],'SRR10047163':['Scer','Seub'],'SRR10047100':['Scer','Seub'],'SRR10047089':['Scer','Seub'],'SRR10047028':['Scer','Skud'],'SRR10047024':['Scer','Seub'],'SRR10047374':['Scer','Seub'],'SRR10047307':['Scer','Seub'],'SRR10047017':['Scer','Seub'],'SRR10047241':['Scer','Seub'],'SRR10047295':['Seub','Suva'],'SRR10047331':['Seub','Suva'],'SRR10047328':['Seub','Suva'],'SRR10047359':['Seub','Suva'],'SRR10047351':['Seub','Suva'],'SRR10047335':['Seub','Suva'],'SRR10035842':['Seub','Suva'],'SRR10047405':['Seub','Suva'],'SRR10047402':['Seub','Suva'],'SRR10047389':['Seub','Suva'],'SRR10047380':['Seub','Suva'],'SRR10047371':['Seub','Suva'],'SRR10047378':['Seub','Suva'],'SRR10047356':['Scer','Seub','Skud','Suva'],'SRR10047345':['Seub','Suva'],'SRR10047410':['Scer','Seub','Skud','Suva'],'SRR10047383':['Scer','Seub','Skud','Suva'],'SRR10047395':['Seub','Suva'],'SRR10047344':['Seub','Suva'],'SRR10047362':['Scer','Seub'],'SRR10047189':['Scer','Seub'],'SRR10047157':['Scer','Seub'],'SRR10047176':['Scer','Seub'],'SRR10047149':['Scer','Seub'],'SRR10047146':['Scer','Seub'],'SRR10047122':['Scer','Skud'],'SRR10047102':['Scer','Seub'],'SRR10047096':['Scer','Seub'],'SRR10047094':['Scer','Seub'],'SRR10047057':['Scer','Seub'],'SRR10046945':['Scer','Seub'],'SRR10047365':['Scer','Seub','Suva'],'SRR10046950':['Seub','Suva'],'SRR10046952':['Seub','Suva'],'SRR10046972':['Scer','Skud'],'SRR10046976':['Scer','Seub'],'SRR5141258':['Scer','Seub'],'SRR5141260':['Scer','Seub'],'SRR12703370':['Scer','Seub'],'SRR12703369':['Scer','Seub'],'SRR12703368':['Scer','Seub'],'SRR12703367':['Scer','Seub'],'SRR12703366':['Scer','Seub'],'SRR5251559':['Scer','Suva'],'SRR10208681':['Scer','Seub'],'SRR9925228':['Scer','Skud'],'SRR9925227':['Scer','Skud'],'SRR9925226':['Scer','Skud'],'SRR9925224':['Scer','Skud'],'SRR9925223':['Scer','Skud'],'SRR9925222':['Scer','Skud'],'SRR9925225':['Scer','Skud']}

plt.figure(figsize=[4,4])
nuc_spe={}
nuc_num={}
for index, row in data_mapped.iterrows():
    if index!=0 and index!=1 and index!=2:
        x=row.nlargest(1)
        y=x.index
        nuc_spe[index]=y[0]
        nuc_num[index]=x[0]
x=[]
y=[]
for i in nuc_spe:
    if len(hybrid_sp[i])==2 and i in map_filt:
        x.append(nuc_num[i])
        z=data_mitomapped.at[i,'{}_mito'.format(nuc_spe[i])]
        y.append(z)

plt.xticks([40,60,80,100])
plt.xlim(40,100)
slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(x, y)
print(r_value*r_value,p_value)
sns.regplot(x=x,y=y,scatter=False,ci=95, color='black',truncate=False)

mit_spe={}
mit_num={}
for index, row in data_mitomapped.iterrows():
    if index!=0 and index!=1 and index!=2:
        x=row.nlargest(1)
        y=x.index
        mit_spe[index]=y[0]
        mit_num[index]=x[0]
        
for i in nuc_spe:
    if len(hybrid_sp[i])==2 and i in map_filt:
        z=data_mitomapped.at[i,'{}_mito'.format(nuc_spe[i])]
        if 'Scer' in hybrid_sp[i] and 'Spar' in hybrid_sp[i]:
            plt.scatter(nuc_num[i],z,color='#3D405B',alpha=1,edgecolor='black',zorder=100)
        elif 'Scer' in hybrid_sp[i] and 'Smik' in hybrid_sp[i]:
            plt.scatter(nuc_num[i],z,color='white',alpha=1,edgecolor='black',zorder=100)
        elif 'Scer' in hybrid_sp[i] and 'Seub' in hybrid_sp[i]:
            plt.scatter(nuc_num[i],z,color='#E07A5F',alpha=1,edgecolor='black',zorder=100)
        elif 'Scer' in hybrid_sp[i] and 'Skud' in hybrid_sp[i]:
            plt.scatter(nuc_num[i],z,color='#81B29A',alpha=1,edgecolor='black',zorder=100)
        elif 'Scer' in hybrid_sp[i] and 'Suva' in hybrid_sp[i]:
            plt.scatter(nuc_num[i],z,color='#F2CC8F',alpha=1,edgecolor='black',zorder=100)
        elif 'Seub' in hybrid_sp[i] and 'Suva' in hybrid_sp[i]:
            plt.scatter(nuc_num[i],z,color='peru',alpha=1,edgecolor='black',zorder=100)
    
plt.yticks([0,25,50,75,100])
plt.xticks([40,60,80,100])
plt.xlim(40,100)
plt.ylim(-5)
plt.ylabel('mtDNA inherited (%)',weight='bold')
plt.xlabel('majority nuclear species (%)',weight='bold')
sns.despine(trim=True)
#plt.plot([0,100],[0,100],color='black',linestyle='dashed')
plt.savefig('../Figures/Hybrid_GI_mit_inherit_dom.png',bbox_inches='tight',dpi=1000,transparent=True)


#%% MITOCHONDRIAL INHERITANCE PATTERN PANELS
x_ScSp=[]
y_ScSp=[]
x_ScSm=[]
y_ScSm=[]
x_ScSk=[]
y_ScSk=[]
x_ScSe=[]
y_ScSe=[]
x_ScSu=[]
y_ScSu=[]
x_SeSu=[]
y_SeSu=[]
for i in nuc_spe:
    if len(hybrid_sp[i])==2 and i in map_filt:
        z=data_mitomapped.at[i,'{}_mito'.format(nuc_spe[i])]
        if 'Scer' in hybrid_sp[i] and 'Spar' in hybrid_sp[i]:
            x_ScSp.append(nuc_num[i])
            y_ScSp.append(z)
        elif 'Scer' in hybrid_sp[i] and 'Smik' in hybrid_sp[i]:
            x_ScSm.append(nuc_num[i])
            y_ScSm.append(z)
        elif 'Scer' in hybrid_sp[i] and 'Skud' in hybrid_sp[i]:
            x_ScSk.append(nuc_num[i])
            y_ScSk.append(z)
        elif 'Scer' in hybrid_sp[i] and 'Seub' in hybrid_sp[i]:
            x_ScSe.append(nuc_num[i])
            y_ScSe.append(z)
        elif 'Scer' in hybrid_sp[i] and 'Suva' in hybrid_sp[i]:
            x_ScSu.append(nuc_num[i])
            y_ScSu.append(z)
        elif 'Seub' in hybrid_sp[i] and 'Suva' in hybrid_sp[i]:
            x_SeSu.append(nuc_num[i])
            y_SeSu.append(z)

plt.figure(figsize=[7.5,5])
plt.subplot(231)
slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(x_ScSp, y_ScSp)
print('Sc x Sp','r2:', r_value*r_value,'p:',p_value)
plt.xlim([40,100])
plt.ylim(-5,105)
sns.regplot(x=x_ScSp,y=y_ScSp,scatter=False,ci=95, color='#3D405B',truncate=False)
plt.scatter(x_ScSp,y_ScSp,color='#3D405B',alpha=1,edgecolor='black',zorder=100)
plt.xlabel('nuclear (%)',weight='bold')
plt.ylabel('mtDNA (%)',weight='bold')
plt.title('Scer x Spar',fontstyle='italic',fontsize=13,weight='bold')
plt.yticks([0,25,50,75,100])
plt.xticks([40,60,80,100])
plt.xlim([40,100])
plt.xticks([50,75,100])
plt.ylim(-5)
plt.xticks([40,60,80,100])
plt.xlim([40,100])
plt.subplot(232)
plt.scatter(x_ScSm,y_ScSm,color='white',alpha=1,edgecolor='black',zorder=100)
plt.xlabel('nuclear (%)',weight='bold')
plt.ylabel('mtDNA (%)',weight='bold')
plt.title('Scer x Smik',fontstyle='italic',fontsize=13,weight='bold')
plt.yticks([0,25,50,75,100])
plt.xticks([50,75])
plt.ylim(-5,105)
plt.xticks([40,60,80,100])
plt.xlim([40,100])
plt.subplot(233)
slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(x_ScSk, y_ScSk)
print('Sc x Sk','r2:', r_value*r_value,'p:',p_value)
plt.xlim([40,100])
plt.ylim(-5,105)
sns.regplot(x=x_ScSk,y=y_ScSk,scatter=False,ci=95, color='#81B29A',truncate=False)
plt.scatter(x_ScSk,y_ScSk,color='#81B29A',alpha=1,edgecolor='black',zorder=100)
plt.xlabel('nuclear (%)',weight='bold')
plt.ylabel('mtDNA (%)',weight='bold')
plt.title('Scer x Skud',fontstyle='italic',fontsize=13,weight='bold')
plt.ylim(-5)
plt.yticks([0,25,50,75,100])
plt.xticks([50,75,100])
plt.xlim(48)
plt.xticks([40,60,80,100])
plt.xlim([40,100])
plt.subplot(234)
slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(x_ScSe, y_ScSe)
print('Sc x Se','r2:', r_value*r_value,'p:',p_value)
plt.xlim([40,100])
plt.ylim(-5,105)
sns.regplot(x=x_ScSe,y=y_ScSe,scatter=False,ci=95, color='#E07A5F',truncate=False)
plt.scatter(x_ScSe,y_ScSe,color='#E07A5F',alpha=1,edgecolor='black',zorder=100)
plt.xlabel('nuclear (%)',weight='bold')
plt.ylabel('mtDNA (%)',weight='bold')
plt.title('Scer x Seub',fontstyle='italic',fontsize=13,weight='bold')
plt.yticks([0,25,50,75,100])
plt.xticks([40,60,80,100])
plt.ylim(-5)
plt.xlim()
plt.xticks([40,60,80,100])
plt.xlim([40,100])
plt.subplot(235)
slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(x_ScSu, y_ScSu)
print('Sc x Su','r2:', r_value*r_value,'p:',p_value)
plt.xlim([40,100])
plt.ylim(-5,105)
sns.regplot(x=x_ScSu,y=y_ScSu,scatter=False,ci=95, color='#F2CC8F',truncate=False)
plt.scatter(x_ScSu,y_ScSu,color='#F2CC8F',alpha=1,edgecolor='black',zorder=100)
plt.xlabel('nuclear (%)',weight='bold')
plt.ylabel('mtDNA (%)',weight='bold')
plt.title('Scer x Suva',fontstyle='italic',fontsize=13,weight='bold')
plt.yticks([0,25,50,75,100])
plt.xticks([50,60])
plt.xticks([40,60,80,100])
plt.xlim([40,100])
plt.subplot(236)
slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(x_SeSu, y_SeSu)
print('Se x Su','r2:', r_value*r_value,'p:',p_value)
plt.xlim([40,100])
plt.ylim(-5,105)
sns.regplot(x=x_SeSu,y=y_SeSu,scatter=False,ci=95, color='peru',truncate=False)
plt.scatter(x_SeSu,y_SeSu,color='peru',alpha=1,edgecolor='black',zorder=100)
plt.xlabel('nuclear (%)',weight='bold')
plt.ylabel('mtDNA (%)',weight='bold')
plt.title('Seub x Suva',fontstyle='italic',fontsize=13,weight='bold')
plt.yticks([0,25,50,75,100])
plt.xticks([50,75,100])

plt.xlim(40)
plt.xticks([40,60,80,100])
plt.xlim([40,100])
plt.tight_layout()
plt.savefig('../Figures/Hybrid_GI_mit_inherit_dom_panels.png',bbox_inches='tight',dpi=1000)
#%%
CD=[]
FM=[]
for i in mit_spe:
    if mit_spe[i]=='Seub_mito':
        if i in Seub_CD:
            CD.append(i)
        elif i in Seub_FM:
            FM.append(i)
n=0
m=0
for i in CD:
    if len(hybrid_sp[i])==2 and i in map_filt:
        if 'Scer' in hybrid_sp[i] and 'Seub' in hybrid_sp[i]:
            n+=1
        elif 'Suva' in hybrid_sp[i] and 'Seub' in hybrid_sp[i]:
            m+=1
print('CD','Scer x Seub', n)
print('CD','Seub x Suva', m)
n=0
m=0
for i in FM:
    if len(hybrid_sp[i])==2 and i in map_filt:
        if 'Scer' in hybrid_sp[i] and 'Seub' in hybrid_sp[i]:
            n+=1
        elif 'Suva' in hybrid_sp[i] and 'Seub' in hybrid_sp[i]:
            m+=1
print('FM','Scer x Seub', n)
print('FM','Seub x Suva', m)

plt.figure(figsize=[3,3])
d={'Scer x Seub':[70,2],'Seub x Suva':[6,0]}
df=pd.DataFrame(data=d)
sns.heatmap(data=df,annot=True,cmap='Reds',cbar=False,linewidth=1,linecolor='black')
plt.yticks([0.5,1.5],['Holarctic','Patagonia B'],rotation=0)
plt.xlabel('hybrid cross',weight='bold')
plt.ylabel('mitochondria origin',weight='bold')
plt.savefig('../Figures/Hybrid_GI_mit_Seub_origin.png',bbox_inches='tight',dpi=1000)

#%%
plt.figure(figsize=[11.5,3])
plt.subplot(161)
yes=[]
no=[]
for i in genomes:
    if i in map_filt:
        if nuc_spe[i]==mit_spe[i].split('_')[0]:
            yes.append(nuc_num[i])
        else:
            no.append(nuc_num[i])
df = pd.DataFrame(no,columns =['False'])
df.loc[:, 'True'] = pd.Series(yes)
sns.boxplot(data=df,palette=['lightgray','darkgray'])
sns.stripplot(data=df,color='black',s=3,jitter=0.2)
plt.ylabel('nuclear (%)',weight='bold')
plt.xlabel('majority mtDNA',weight='bold')
plt.title('All',fontsize=13,weight='bold')
t,p=scipy.stats.ttest_ind(no,yes,equal_var=False)
print('All', t, p)
print(len(yes),len(no))
plt.subplot(162)
yes=[]
no=[]
for i in genomes:
     if 'Scer' in hybrid_sp[i] and 'Spar' in hybrid_sp[i] and len(hybrid_sp[i])==2 and i in map_filt:
        if nuc_spe[i]==mit_spe[i].split('_')[0]:
            yes.append(nuc_num[i])
        else:
            no.append(nuc_num[i])
df = pd.DataFrame()
df.loc[:, 'True'] = pd.Series(yes)
df.loc[:, 'False'] = pd.Series(no)
sns.boxplot(data=df,palette=['#BCBFDA','#3D405B'],order=['False','True'])
sns.stripplot(data=df,color='black',s=3,jitter=0.2,order=['False','True'])
plt.ylabel('nuclear (%)',weight='bold')
plt.xlabel('majority mtDNA',weight='bold')
plt.title('Scer x Spar',fontstyle='italic',fontsize=13,weight='bold')
t,p=scipy.stats.ttest_ind(no,yes,equal_var=False)
print('Scer x Spar', t, p)
print(len(yes),len(no))
plt.subplot(163)
yes=[]
no=[]
for i in genomes:
     if 'Scer' in hybrid_sp[i] and 'Skud' in hybrid_sp[i] and len(hybrid_sp[i])==2 and i in map_filt:
        if nuc_spe[i]==mit_spe[i].split('_')[0]:
            yes.append(nuc_num[i])
        else:
            no.append(nuc_num[i])
df = pd.DataFrame()
df.loc[:, 'True'] = pd.Series(yes)
df.loc[:, 'False'] = pd.Series(no)
sns.boxplot(data=df,palette=['#B4E5CD','#81B29A'],order=['False','True'])
sns.stripplot(data=df,color='black',s=3,jitter=0.2,order=['False','True'])
plt.ylabel('nuclear (%)',weight='bold')
plt.xlabel('majority mtDNA',weight='bold')
plt.title('Scer x Skud',fontstyle='italic',fontsize=13,weight='bold')
t,p=scipy.stats.ttest_ind(no,yes,equal_var=False)
print('Scer x Skud', t, p)
print(len(yes),len(no))

plt.subplot(164)
yes=[]
no=[]
for i in genomes:
     if 'Scer' in hybrid_sp[i] and 'Seub' in hybrid_sp[i] and len(hybrid_sp[i])==2 and i in map_filt:
        if nuc_spe[i]==mit_spe[i].split('_')[0]:
            yes.append(nuc_num[i])
        else:
            no.append(nuc_num[i])
df = pd.DataFrame()
df.loc[:, 'True'] = pd.Series(yes)
df.loc[:, 'False'] = pd.Series(no)
sns.boxplot(data=df,palette=['#FFE0C5','#E07A5F'],order=['False','True'])
sns.stripplot(data=df,color='black',s=3,jitter=0.2,order=['False','True'])
plt.ylabel('nuclear (%)',weight='bold')
plt.xlabel('majority mtDNA',weight='bold')
plt.title('Scer x Seub',fontstyle='italic',fontsize=13,weight='bold')
t,p=scipy.stats.ttest_ind(no,yes,equal_var=False)
print('Scer x Seub', t, p)
print(len(yes),len(no))

plt.subplot(165)
yes=[]
no=[]
for i in genomes:
     if 'Scer' in hybrid_sp[i] and 'Suva' in hybrid_sp[i] and len(hybrid_sp[i])==2 and i in map_filt:
        if nuc_spe[i]==mit_spe[i].split('_')[0]:
            yes.append(nuc_num[i])
        else:
            no.append(nuc_num[i])
df = pd.DataFrame()
df.loc[:, 'False'] = pd.Series(no)
df.loc[:, 'True'] = pd.Series(yes)
sns.boxplot(data=df,palette=['#FFFFDC','#F2CC8F'],order=['False','True'])
sns.stripplot(data=df,color='black',s=3,jitter=0.2,order=['False','True'])
plt.ylabel('nuclear (%)',weight='bold')
plt.xlabel('majority mtDNA',weight='bold')
plt.title('Scer x Suva',fontstyle='italic',fontsize=13,weight='bold')
t,p=scipy.stats.ttest_ind(no,yes,equal_var=False)
print('Scer x Suva', t, p)
print(len(yes),len(no))

plt.subplot(166)
yes=[]
no=[]
for i in genomes:
     if 'Seub' in hybrid_sp[i] and 'Suva' in hybrid_sp[i] and len(hybrid_sp[i])==2 and i in map_filt:
        if nuc_spe[i]==mit_spe[i].split('_')[0]:
            yes.append(nuc_num[i])
        else:
            no.append(nuc_num[i])
df = pd.DataFrame()
df.loc[:, 'True'] = pd.Series(yes)
df.loc[:, 'False'] = pd.Series(no)
sns.boxplot(data=df,palette=['wheat','peru'],order=['False','True'])
sns.stripplot(data=df,color='black',s=3,jitter=0.2,order=['False','True'])
plt.ylabel('nuclear (%)',weight='bold')
plt.xlabel('majority mtDNA',weight='bold')
plt.title('Seub x Suva',fontstyle='italic',fontsize=13,weight='bold')
t,p=scipy.stats.ttest_ind(no,yes,equal_var=False)
print('Seub x Suva', t, p)
print(len(yes),len(no))
plt.tight_layout()
plt.savefig('../Figures/Hybrid_GI_mit_inherit_boxplot.png',bbox_inches='tight',dpi=1000)

#%%

colors={'Scer_mito':"#015482",'Spar_mito':"#dc4d01",'Smik_mito':"#ffda03",'Sjur_mito':'#a2653e','Skud_mito':'#2b5d34','Sarb_mito':'gray','Seub_mito':'#8c000f','Suva_mito':'#4b006e'}
plt.figure(figsize=[(11.5/6)*5,1.25])
plt.subplot(151)
first=[]
second=[]
s1='Scer'
s2='Spar'
for i in genomes:
     if '{}'.format(s1) in hybrid_sp[i] and '{}'.format(s2) in hybrid_sp[i] and len(hybrid_sp[i])==2 and i in map_filt:
        if mit_spe[i]=='{}_mito'.format(s1):
             first.append(i)
        elif mit_spe[i]=='{}_mito'.format(s2):
            second.append(i)
d={'x':[len(first),len(second)]}
df=pd.DataFrame(data=d)
c1=sns.light_palette(colors['{}_mito'.format(s1)], as_cmap=True)
c2=sns.light_palette(colors['{}_mito'.format(s2)], as_cmap=True)
sns.heatmap(data=df,cbar=False,annot=True,linecolor='black',linewidth=1,cmap=[c2(len(second)/(len(first)+len(second))),c1(len(first)/(len(first)+len(second)))])
plt.yticks([0.5,1.5],[s1,s2],rotation=0,fontstyle='italic')
plt.title('n = {}'.format(len(first)+len(second)),fontsize=13)
plt.xticks([])
plt.subplot(152)
first=[]
second=[]
s1='Scer'
s2='Skud'
for i in genomes:
     if '{}'.format(s1) in hybrid_sp[i] and '{}'.format(s2) in hybrid_sp[i] and len(hybrid_sp[i])==2 and i in map_filt:
        if mit_spe[i]=='{}_mito'.format(s1):
             first.append(i)
        elif mit_spe[i]=='{}_mito'.format(s2):
            second.append(i)
d={'x':[len(first),len(second)]}
df=pd.DataFrame(data=d)
c1=sns.light_palette(colors['{}_mito'.format(s1)], as_cmap=True)
c2=sns.light_palette(colors['{}_mito'.format(s2)], as_cmap=True)
sns.heatmap(data=df,cbar=False,annot=True,linecolor='black',linewidth=1,cmap=[c1(len(first)/(len(first)+len(second))),c2(len(second)/(len(first)+len(second)))])
plt.yticks([0.5,1.5],[s1,s2],rotation=0,fontstyle='italic')
plt.title('n = {}'.format(len(first)+len(second)),fontsize=13)
plt.xticks([])
plt.subplot(153)
first=[]
second=[]
s1='Scer'
s2='Seub'
for i in genomes:
     if '{}'.format(s1) in hybrid_sp[i] and '{}'.format(s2) in hybrid_sp[i] and len(hybrid_sp[i])==2 and i in map_filt:
        if mit_spe[i]=='{}_mito'.format(s1):
             first.append(i)
        elif mit_spe[i]=='{}_mito'.format(s2):
            second.append(i)
d={'x':[len(first),len(second)]}
df=pd.DataFrame(data=d)
c1=sns.light_palette(colors['{}_mito'.format(s1)], as_cmap=True)
c2=sns.light_palette(colors['{}_mito'.format(s2)], as_cmap=True)
sns.heatmap(data=df,cbar=False,annot=True,linecolor='black',linewidth=1,cmap=[c1(len(first)/(len(first)+len(second))),c2(len(second)/(len(first)+len(second)))])
plt.yticks([0.5,1.5],[s1,s2],rotation=0,fontstyle='italic')
plt.title('n = {}'.format(len(first)+len(second)),fontsize=13)
plt.xticks([])
plt.subplot(154)
first=[]
second=[]
s1='Scer'
s2='Suva'
for i in genomes:
     if '{}'.format(s1) in hybrid_sp[i] and '{}'.format(s2) in hybrid_sp[i] and len(hybrid_sp[i])==2 and i in map_filt:
        if mit_spe[i]=='{}_mito'.format(s1):
             first.append(i)
        elif mit_spe[i]=='{}_mito'.format(s2):
            second.append(i)
d={'x':[len(first),len(second)]}
df=pd.DataFrame(data=d)
c1=sns.light_palette(colors['{}_mito'.format(s1)], as_cmap=True)
c2=sns.light_palette(colors['{}_mito'.format(s2)], as_cmap=True)
sns.heatmap(data=df,cbar=False,annot=True,linecolor='black',linewidth=1,cmap=[c2(len(second)/(len(first)+len(second))),c1(len(first)/(len(first)+len(second)))])
plt.yticks([0.5,1.5],[s1,s2],rotation=0,fontstyle='italic')
plt.title('n = {}'.format(len(first)+len(second)),fontsize=13)
plt.xticks([])
plt.subplot(155)
first=[]
second=[]
s1='Seub'
s2='Suva'
for i in genomes:
     if '{}'.format(s1) in hybrid_sp[i] and '{}'.format(s2) in hybrid_sp[i] and len(hybrid_sp[i])==2 and i in map_filt:
        if mit_spe[i]=='{}_mito'.format(s1):
             first.append(i)
        elif mit_spe[i]=='{}_mito'.format(s2):
            second.append(i)
d={'x':[len(first),len(second)]}
df=pd.DataFrame(data=d)
c1=sns.light_palette(colors['{}_mito'.format(s1)], as_cmap=True)
c2=sns.light_palette(colors['{}_mito'.format(s2)], as_cmap=True)
sns.heatmap(data=df,cbar=False,annot=True,linecolor='black',linewidth=1,cmap=[c1(len(first)/(len(first)+len(second))),c2(len(second)/(len(first)+len(second)))])
plt.yticks([0.5,1.5],[s1,s2],rotation=0,fontstyle='italic')
plt.title('n = {}'.format(len(first)+len(second)),fontsize=13)
plt.xticks([])
plt.tight_layout()
plt.savefig('../Figures/Hybrid_GI_mit_inherit_num.png',bbox_inches='tight',dpi=1000)