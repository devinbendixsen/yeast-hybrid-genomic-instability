SRR9925225 Num reads = 13115915
SRR9925225 Num mapped reads = 801173
SRR9925225 Unmapped reads = 94.61%
SRR9925225 Average MQ = 2.82236168807132
SRR9925225 Median MQ = 0
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	12314742	93.89%	0%	0	0	0	0
Scer_mito	56483	0.43%	7.43%	51.99	55.94	60	60
Spar_mito	15528	0.12%	1.51%	34.07	49.66	45	60
Smik_mito	13624	0.1%	1.34%	17.42	24.99	15	24
Sjur_mito	39568	0.3%	2.7%	21.79	45.15	0	50
Skud_mito	621584	4.74%	81.7%	49.9	53.7	60	60
Sarb_mito	8099	0.06%	0.98%	27.14	31.71	26	33
Seub_mito_CD	36478	0.28%	4.09%	32.12	40.51	40	40
Seub_mito_FM	7652	0.06%	0.02%	0.42	23.53	0	22
Suva_mito	2157	0.02%	0.23%	19.65	26.21	15	25
