ERR3010133 Num reads = 1286195
ERR3010133 Num mapped reads = 12351
ERR3010133 Unmapped reads = 99.36%
ERR3010133 Average MQ = 0.272260427073655
ERR3010133 Median MQ = 0
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	1273844	99.04%	0%	0	0	0	0
Scer_mito	3727	0.29%	34.98%	35.55	46.3	44	48
Spar_mito	1059	0.08%	8.28%	27.99	43.79	40	46
Smik_mito	1179	0.09%	10.01%	26.28	37.83	40	40
Sjur_mito	2181	0.17%	13.53%	23.73	46.76	3	52
Skud_mito	1114	0.09%	9.39%	25.33	36.74	40	40
Sarb_mito	1497	0.12%	12.83%	27.69	39.47	40	40
Seub_mito_CD	606	0.05%	4.13%	23.3	41.77	26	44
Seub_mito_FM	514	0.04%	3.57%	22.37	39.38	20	40
Suva_mito	474	0.04%	3.28%	21.11	37.33	18	40
