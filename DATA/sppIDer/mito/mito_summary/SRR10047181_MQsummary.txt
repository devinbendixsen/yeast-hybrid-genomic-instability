SRR10047181 Num reads = 4957348
SRR10047181 Num mapped reads = 767484
SRR10047181 Unmapped reads = 85.7%
SRR10047181 Average MQ = 7.77061787875291
SRR10047181 Median MQ = 0
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	4189864	84.52%	0%	0	0	0	0
Scer_mito	6247	0.13%	0.37%	13.12	31.62	0	40
Spar_mito	44899	0.91%	6.01%	55.29	58.31	60	60
Smik_mito	3764	0.08%	0.29%	18.68	34.7	5	40
Sjur_mito	4302	0.09%	0.16%	8.63	32.76	0	40
Skud_mito	4890	0.1%	0.32%	15.37	32.7	0	40
Sarb_mito	3451	0.07%	0.34%	24.53	34.9	40	40
Seub_mito_CD	625284	12.61%	84.52%	53.06	55.39	60	60
Seub_mito_FM	26746	0.54%	2.2%	17.17	29.5	8	24
Suva_mito	47901	0.97%	5.8%	42.87	49.98	60	60
