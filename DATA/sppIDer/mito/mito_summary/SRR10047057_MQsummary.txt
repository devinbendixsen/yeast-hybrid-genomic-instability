SRR10047057 Num reads = 2215403
SRR10047057 Num mapped reads = 560961
SRR10047057 Unmapped reads = 76.47%
SRR10047057 Average MQ = 12.660824689684
SRR10047057 Median MQ = 0
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	1654442	74.68%	0%	0	0	0	0
Scer_mito	2576	0.12%	0.21%	12.07	29.03	0	40
Spar_mito	32552	1.47%	6.07%	56.96	58.64	60	60
Smik_mito	1428	0.06%	0.15%	18.22	33.23	4.5	40
Sjur_mito	1616	0.07%	0.07%	7.59	31.71	0	40
Skud_mito	1835	0.08%	0.17%	15.5	32.43	0	40
Sarb_mito	1221	0.06%	0.16%	23.23	33.4	30	40
Seub_mito_CD	465095	20.99%	85.38%	52.2	54.55	60	60
Seub_mito_FM	19409	0.88%	2.09%	16.21	28.84	7	24
Suva_mito	35229	1.59%	5.7%	41.9	49.7	60	60
