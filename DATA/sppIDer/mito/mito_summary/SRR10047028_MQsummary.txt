SRR10047028 Num reads = 4252981
SRR10047028 Num mapped reads = 162741
SRR10047028 Unmapped reads = 96.73%
SRR10047028 Average MQ = 1.74556058444653
SRR10047028 Median MQ = 0
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	4090240	96.17%	0%	0	0	0	0
Scer_mito	14755	0.35%	7.81%	37.91	51.55	60	60
Spar_mito	5948	0.14%	3%	31.96	45.62	40	60
Smik_mito	4862	0.11%	2.21%	22.77	36.07	19	40
Sjur_mito	10095	0.24%	2.6%	14.96	41.79	0	45
Skud_mito	113373	2.67%	77.13%	52.75	55.81	60	60
Sarb_mito	3780	0.09%	1.96%	26.8	37.14	40	40
Seub_mito_CD	6576	0.15%	3.98%	40.02	47.56	46	47
Seub_mito_FM	2068	0.05%	0.82%	20.72	37.71	6	40
Suva_mito	1284	0.03%	0.49%	19.07	36.05	4	40
