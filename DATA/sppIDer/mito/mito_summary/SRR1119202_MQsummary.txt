SRR1119202 Num reads = 4208545
SRR1119202 Num mapped reads = 19081
SRR1119202 Unmapped reads = 99.69%
SRR1119202 Average MQ = 0.148944350125756
SRR1119202 Median MQ = 0
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	4189464	99.55%	0%	0	0	0	0
Scer_mito	3161	0.08%	19.81%	41.86	51.32	50	60
Spar_mito	959	0.02%	5.23%	27.71	39.02	27	40
Smik_mito	647	0.02%	1.98%	11.67	29.27	0	25
Sjur_mito	2424	0.06%	3.5%	6.92	36.88	0	40
Skud_mito	10157	0.24%	63.8%	41.36	50.59	51	60
Sarb_mito	510	0.01%	3.1%	29.1	36.74	33	40
Seub_mito_CD	616	0.01%	1.57%	8.84	26.56	0	27
Seub_mito_FM	449	0.01%	0.53%	3.31	21.55	0	27
Suva_mito	158	0%	0.48%	11.34	28.89	0	25
