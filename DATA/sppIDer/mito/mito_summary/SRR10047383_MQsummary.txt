SRR10047383 Num reads = 1653457
SRR10047383 Num mapped reads = 498281
SRR10047383 Unmapped reads = 71.69%
SRR10047383 Average MQ = 15.8482264733827
SRR10047383 Median MQ = 0
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	1155176	69.86%	0%	0	0	0	0
Scer_mito	18316	1.11%	3.5%	50.54	56.58	60	60
Spar_mito	13064	0.79%	2.44%	42.23	48.31	60	60
Smik_mito	4011	0.24%	0.59%	22.11	32.17	17	33
Sjur_mito	18241	1.1%	1.54%	18.78	47.63	0	60
Skud_mito	424641	25.68%	88.4%	55.38	56.84	60	60
Sarb_mito	3354	0.2%	0.59%	30.64	37.51	34	40
Seub_mito_CD	14450	0.87%	2.83%	45.44	49.64	49	57
Seub_mito_FM	1479	0.09%	0.05%	6.03	38.77	0	40
Suva_mito	725	0.04%	0.07%	12.74	26.46	0	23
