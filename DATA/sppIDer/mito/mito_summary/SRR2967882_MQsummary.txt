SRR2967882 Num reads = 2297782
SRR2967882 Num mapped reads = 50166
SRR2967882 Unmapped reads = 98.4%
SRR2967882 Average MQ = 0.737828915014566
SRR2967882 Median MQ = 0
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	2247616	97.82%	0%	0	0	0	0
Scer_mito	23234	1.01%	54.7%	44.23	51.13	60	60
Spar_mito	5863	0.26%	12.41%	37.04	47.63	46	59
Smik_mito	4492	0.2%	8.73%	27.6	38.66	40	40
Sjur_mito	5751	0.25%	5.93%	14.1	37.22	0	40
Skud_mito	3209	0.14%	5.4%	22.52	36.39	21	40
Sarb_mito	3251	0.14%	6.49%	27.2	37.09	40	40
Seub_mito_CD	1564	0.07%	2.09%	17.89	36.38	0	40
Seub_mito_FM	1503	0.07%	2.39%	21.54	36.88	17	40
Suva_mito	1299	0.06%	1.86%	18.79	35.63	4	40
