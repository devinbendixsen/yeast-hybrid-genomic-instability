SRR1649181 Num reads = 36630672
SRR1649181 Num mapped reads = 1112716
SRR1649181 Unmapped reads = 97.78%
SRR1649181 Average MQ = 0.91407553211145
SRR1649181 Median MQ = 0
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	35517956	96.96%	0%	0	0	0	0
Scer_mito	6149	0.02%	0.1%	2.54	18.83	0	13
Spar_mito	55585	0.15%	6.08%	37.2	41.78	40	40
Smik_mito	4121	0.01%	0.1%	3.9	19.2	0	13
Sjur_mito	6980	0.02%	0.07%	1.63	19.33	0	18
Skud_mito	5354	0.01%	0.11%	3.02	18.54	0	12
Sarb_mito	3054	0.01%	0.09%	5.52	22.33	0	18
Seub_mito_CD	812629	2.22%	83.08%	35.19	42.28	40	40
Seub_mito_FM	102637	0.28%	3.59%	7.3	25.61	0	27
Suva_mito	116207	0.32%	6.77%	17.19	36.25	0	40
