SRR8799676 Num reads = 27849239
SRR8799676 Num mapped reads = 268469
SRR8799676 Unmapped reads = 99.27%
SRR8799676 Average MQ = 0.344457419464855
SRR8799676 Median MQ = 0
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	27580770	99.04%	0%	0	0	0	0
Scer_mito	4548	0.02%	0.2%	1.64	18.33	0	12.5
Spar_mito	31453	0.11%	14.88%	52.65	55.12	60	60
Smik_mito	2330	0.01%	0.19%	3.33	20.33	0	13
Sjur_mito	2224	0.01%	0.06%	1.06	19.86	0	18
Skud_mito	3116	0.01%	0.21%	3.22	23.56	0	19
Sarb_mito	732	0%	0.11%	5.28	17.58	0	13
Seub_mito_CD	182376	0.65%	77.72%	40.37	46.91	47	52
Seub_mito_FM	23312	0.08%	2.79%	7.63	31.5	0	31
Suva_mito	18378	0.07%	3.84%	19.83	47.01	0	60
