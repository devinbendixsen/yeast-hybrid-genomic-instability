SRR10047389 Num reads = 8164389
SRR10047389 Num mapped reads = 687319
SRR10047389 Unmapped reads = 92.85%
SRR10047389 Average MQ = 3.68203842320595
SRR10047389 Median MQ = 0
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	7477070	91.58%	0%	0	0	0	0
Scer_mito	21357	0.26%	2.29%	20.99	33.49	11	40
Spar_mito	8574	0.11%	0.77%	18.28	34.78	3	40
Smik_mito	20007	0.25%	2.33%	23.02	33.89	25	40
Sjur_mito	14796	0.18%	0.81%	10.68	33.24	0	40
Skud_mito	17954	0.22%	1.81%	20.11	34.14	12	40
Sarb_mito	10787	0.13%	1.2%	22.38	34.51	20	40
Seub_mito_CD	62743	0.77%	8.29%	33.15	42.96	40	46
Seub_mito_FM	41008	0.5%	5.17%	31.2	42.33	37	46
Suva_mito	490093	6%	77.32%	50.76	55.08	60	60
