DRR040654 Num reads = 8116712
DRR040654 Num mapped reads = 2806270
DRR040654 Unmapped reads = 66.72%
DRR040654 Average MQ = 18.5064926536755
DRR040654 Median MQ = 0
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	5310442	65.43%	0%	0	0	0	0
Scer_mito	11315	0.14%	0.15%	9.9	28.42	0	40
Spar_mito	103670	1.28%	3.73%	56.96	58.61	60	60
Smik_mito	6039	0.07%	0.12%	17.26	33.1	3	40
Sjur_mito	6498	0.08%	0.06%	8.14	32.65	0	40
Skud_mito	7383	0.09%	0.12%	14.38	31.7	0	40
Sarb_mito	4292	0.05%	0.11%	22.4	33.58	21	40
Seub_mito_CD	2399157	29.56%	86.98%	55.52	56.7	60	60
Seub_mito_FM	76845	0.95%	2.15%	22.48	29.71	17	26
Suva_mito	191071	2.35%	6.58%	46.57	50.03	60	60
