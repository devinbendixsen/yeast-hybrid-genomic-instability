SRR2967875 Num reads = 1867757
SRR2967875 Num mapped reads = 21377
SRR2967875 Unmapped reads = 99.21%
SRR2967875 Average MQ = 0.360846726849371
SRR2967875 Median MQ = 0
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	1846380	98.86%	0%	0	0	0	0
Scer_mito	3229	0.17%	15.08%	29.2	42.16	40	45
Spar_mito	1201	0.06%	5.01%	22.62	36.61	21	40
Smik_mito	2429	0.13%	11.45%	21.79	31.17	16	40
Sjur_mito	2121	0.11%	4.82%	12.71	37.71	0	40
Skud_mito	7652	0.41%	44.34%	46.74	54.41	60	60
Sarb_mito	1939	0.1%	8.58%	25.11	38.28	40	40
Seub_mito_CD	1363	0.07%	5.69%	27.99	45.25	40	46
Seub_mito_FM	738	0.04%	2.35%	18.18	38.56	0	40
Suva_mito	705	0.04%	2.68%	20.94	37.09	16	40
