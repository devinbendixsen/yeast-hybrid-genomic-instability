SRR1119204 Num reads = 17027522
SRR1119204 Num mapped reads = 206590
SRR1119204 Unmapped reads = 98.96%
SRR1119204 Average MQ = 0.509101148129481
SRR1119204 Median MQ = 0
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	16820932	98.79%	0%	0	0	0	0
Scer_mito	7537	0.04%	2.28%	11.24	21	4	23
Spar_mito	1188	0.01%	0.13%	1.95	9.83	0	7
Smik_mito	6328	0.04%	1.68%	8.69	18.49	0	23
Sjur_mito	3702	0.02%	0.58%	3.62	13.11	0	11
Skud_mito	5904	0.03%	1.21%	8.17	22.53	0	23
Sarb_mito	1356	0.01%	0.13%	2.11	12.41	0	12
Seub_mito_CD	7270	0.04%	1.55%	11.35	30.11	0	25
Seub_mito_FM	8377	0.05%	2.89%	19.53	31.98	21	27
Suva_mito	164928	0.97%	89.56%	49.82	51.81	60	60
