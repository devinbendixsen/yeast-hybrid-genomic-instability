SRR8799671 Num reads = 16797122
SRR8799671 Num mapped reads = 529748
SRR8799671 Unmapped reads = 97.53%
SRR8799671 Average MQ = 1.18273761421748
SRR8799671 Median MQ = 0
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	16267374	96.85%	0%	0	0	0	0
Scer_mito	2919	0.02%	0.1%	3.74	26.88	0	30
Spar_mito	49742	0.3%	11.57%	55.58	57.68	60	60
Smik_mito	1957	0.01%	0.08%	5.1	28.84	0	40
Sjur_mito	2387	0.01%	0.05%	2.49	31.39	0	40
Skud_mito	2232	0.01%	0.08%	4.52	28.8	0	40
Sarb_mito	1073	0.01%	0.09%	9.76	29.33	0	40
Seub_mito_CD	386765	2.3%	80.84%	41.06	47.42	50	53
Seub_mito_FM	45915	0.27%	2.35%	6.29	29.71	0	27
Suva_mito	36758	0.22%	4.84%	24.08	44.15	10	55
