SRR8799640 Num reads = 16686721
SRR8799640 Num mapped reads = 621747
SRR8799640 Unmapped reads = 96.91%
SRR8799640 Average MQ = 1.50490189174973
SRR8799640 Median MQ = 0
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	16064974	96.27%	0%	0	0	0	0
Scer_mito	2713	0.02%	0.06%	2.49	23.35	0	21
Spar_mito	46675	0.28%	8.74%	55.67	57.68	60	60
Smik_mito	1888	0.01%	0.07%	3.94	22.06	0	12
Sjur_mito	2199	0.01%	0.03%	1.86	28.45	0	36
Skud_mito	2405	0.01%	0.07%	3.74	23.32	0	22.5
Sarb_mito	837	0.01%	0.04%	6.5	24.5	0	18
Seub_mito_CD	478177	2.87%	83.57%	43.68	48.5	52	57
Seub_mito_FM	45418	0.27%	2.38%	8.14	30.12	0	27
Suva_mito	41435	0.25%	5.04%	29.58	47.16	33	60
