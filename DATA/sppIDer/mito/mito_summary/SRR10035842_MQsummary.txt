SRR10035842 Num reads = 1792392
SRR10035842 Num mapped reads = 687694
SRR10035842 Unmapped reads = 62.73%
SRR10035842 Average MQ = 21.7523571852586
SRR10035842 Median MQ = 0
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	1104698	61.63%	0%	0	0	0	0
Scer_mito	1871	0.1%	0.14%	15.16	31.31	0	40
Spar_mito	861	0.05%	0.06%	13.75	29.61	0	40
Smik_mito	1061	0.06%	0.09%	16.07	29.44	3	40
Sjur_mito	1442	0.08%	0.07%	8.96	27.86	0	32
Skud_mito	1507	0.08%	0.12%	17.79	34.63	3	40
Sarb_mito	990	0.06%	0.11%	25.54	34.97	40	40
Seub_mito_CD	5774	0.32%	0.55%	30.85	48.57	40	60
Seub_mito_FM	2615	0.15%	0.15%	8.41	21.7	0	25
Suva_mito	671573	37.47%	98.72%	57.58	58.63	60	60
