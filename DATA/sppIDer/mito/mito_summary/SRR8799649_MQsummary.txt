SRR8799649 Num reads = 19990423
SRR8799649 Num mapped reads = 226215
SRR8799649 Unmapped reads = 99.15%
SRR8799649 Average MQ = 0.408584150520477
SRR8799649 Median MQ = 0
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	19764208	98.87%	0%	0	0	0	0
Scer_mito	3208	0.02%	0.2%	2.89	26.99	0	38
Spar_mito	24206	0.12%	13.66%	53.18	55.74	60	60
Smik_mito	1879	0.01%	0.19%	4.41	25.71	0	25
Sjur_mito	1939	0.01%	0.07%	1.35	22.92	0	19
Skud_mito	2186	0.01%	0.16%	3.09	24.57	0	24
Sarb_mito	729	0%	0.12%	6.71	23.28	0	16
Seub_mito_CD	155053	0.78%	78.49%	40.99	47.9	50	55
Seub_mito_FM	20441	0.1%	3.01%	7.88	31.66	0	33
Suva_mito	16574	0.08%	4.09%	20.02	47.97	0	60
