SRR8799657 Num reads = 3290462
SRR8799657 Num mapped reads = 187496
SRR8799657 Unmapped reads = 95.3%
SRR8799657 Average MQ = 2.380740151383
SRR8799657 Median MQ = 0
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	3102966	94.3%	0%	0	0	0	0
Scer_mito	10042	0.31%	3.68%	19.06	33.58	6	40
Spar_mito	5656	0.17%	1.97%	19.23	35.58	4	40
Smik_mito	9264	0.28%	4.23%	24.16	34.16	34	40
Sjur_mito	8795	0.27%	1.71%	10.27	34.09	0	40
Skud_mito	9608	0.29%	3.82%	21.7	35.25	17	40
Sarb_mito	7083	0.22%	3.39%	26.47	35.72	40	40
Seub_mito_CD	17259	0.52%	8.58%	36.89	47.94	46	57
Seub_mito_FM	12754	0.39%	6.4%	34.47	44.38	40	47
Suva_mito	107035	3.25%	66.2%	53.69	56.09	60	60
