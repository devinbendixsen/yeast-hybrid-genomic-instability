SRR10047092 Num reads = 6012655
SRR10047092 Num mapped reads = 4970981
SRR10047092 Unmapped reads = 27.39%
SRR10047092 Average MQ = 39.2190220460013
SRR10047092 Median MQ = 60
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	1041674	17.32%	0%	0	0	0	0
Scer	2252546	37.46%	47.57%	53.3	57.8	60	60
Spar	162725	2.71%	2.64%	25.62	36.2	17	40
Smik	147196	2.45%	2.24%	24.77	37.3	12	40
Sjur	120585	2.01%	1.55%	18.88	33.72	5	40
Skud	120060	2%	1.61%	16.24	27.65	6	21
Sarb	219332	3.65%	3.31%	18.31	27.8	7	27
Seub	1577933	26.24%	34.5%	54.97	57.59	60	60
Suva	370604	6.16%	6.58%	34.99	45.14	43	60
