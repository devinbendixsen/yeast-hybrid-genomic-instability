SRR800807 Num reads = 72378100
SRR800807 Num mapped reads = 66431586
SRR800807 Unmapped reads = 16.4%
SRR800807 Average MQ = NA
SRR800807 Median MQ = 60
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	5946514	8.22%	0%	0	0	0	0
Scer	59839421	82.68%	92.92%	0	0	60	60
Spar	4016254	5.55%	5.94%	50.03	55.92	60	60
Smik	302528	0.42%	0.28%	28.71	50.42	26	60
Sjur	1046046	1.45%	0.31%	5.35	29.54	0	27
Skud	679335	0.94%	0.09%	2.67	31.84	0	27
Sarb	185562	0.26%	0.11%	14.52	39.71	0	46
Seub	132100	0.18%	0.09%	18.34	42.92	0	60
Suva	230340	0.32%	0.25%	32.64	50.39	40	60
