SRR8799660 Num reads = 3397418
SRR8799660 Num mapped reads = 3340898
SRR8799660 Unmapped reads = 7.69%
SRR8799660 Average MQ = 53.4664186155486
SRR8799660 Median MQ = 60
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	56520	1.66%	0%	0	0	0	0
Scer	102980	3.03%	1.3%	18.12	45.63	0	60
Spar	17335	0.51%	0.39%	23.61	33.25	12	40
Smik	19700	0.58%	0.47%	32.75	43.97	40	60
Sjur	81982	2.41%	1.09%	16.54	39.63	0	44
Skud	118546	3.49%	2.42%	27.83	43.45	27	46
Sarb	21581	0.64%	0.5%	18.55	25.74	7	19
Seub	752876	22.16%	23.33%	56.16	57.8	60	60
Suva	2225898	65.52%	70.5%	59.03	59.43	60	60
