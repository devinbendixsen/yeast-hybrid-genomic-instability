SRR8799680 Num reads = 16526674
SRR8799680 Num mapped reads = 16142224
SRR8799680 Unmapped reads = 7.91%
SRR8799680 Average MQ = 54.0625168742362
SRR8799680 Median MQ = 60
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	384450	2.33%	0%	0	0	0	0
Scer	9192753	55.62%	56.62%	55.18	58.86	60	60
Spar	60306	0.36%	0.31%	35.32	45	40	54
Smik	58672	0.36%	0.3%	38.24	48.57	52	60
Sjur	179354	1.09%	0.2%	4.35	25.56	0	25
Skud	110044	0.67%	0.1%	6.02	42.64	0	48
Sarb	8435	0.05%	0.03%	16.85	31.78	4	32
Seub	6419946	38.85%	41.79%	58.53	59.08	60	60
Suva	112714	0.68%	0.64%	40.2	46.8	49	59
