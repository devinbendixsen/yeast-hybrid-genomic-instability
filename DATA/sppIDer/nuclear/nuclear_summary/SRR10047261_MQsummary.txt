SRR10047261 Num reads = 6614387
SRR10047261 Num mapped reads = 5850753
SRR10047261 Unmapped reads = 19.22%
SRR10047261 Average MQ = 45.6710992265799
SRR10047261 Median MQ = 60
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	763634	11.55%	0%	0	0	0	0
Scer	3053129	46.16%	53.33%	54.62	58.52	60	60
Spar	113634	1.72%	1.52%	27.35	38.27	19	40
Smik	102558	1.55%	1.32%	27.7	40.18	21	46
Sjur	85680	1.3%	0.83%	17.24	33.44	2	40
Skud	80566	1.22%	0.85%	17.2	30.41	6	30
Sarb	130708	1.98%	1.6%	18.54	28.32	7	30
Seub	2034723	30.76%	36.91%	56.6	58.41	60	60
Suva	249755	3.78%	3.64%	35.7	45.86	46	60
