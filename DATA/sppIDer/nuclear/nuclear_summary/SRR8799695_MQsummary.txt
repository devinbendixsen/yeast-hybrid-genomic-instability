SRR8799695 Num reads = 44727707
SRR8799695 Num mapped reads = 44215831
SRR8799695 Unmapped reads = 5.68%
SRR8799695 Average MQ = 55.2285845549829
SRR8799695 Median MQ = 60
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	511876	1.14%	0%	0	0	0	0
Scer	25581552	57.19%	57.09%	55.44	58.89	60	60
Spar	179535	0.4%	0.32%	31.92	42.32	40	46
Smik	68977	0.15%	0.12%	25.33	35.86	22	40
Sjur	197630	0.44%	0.12%	6.86	26.13	0	25
Skud	96826	0.22%	0.07%	10.74	37.63	0	40
Sarb	23565	0.05%	0.03%	17.69	31.23	6	30
Seub	17815095	39.83%	41.77%	57.98	58.62	60	60
Suva	252651	0.56%	0.49%	34.68	42.7	40	46
