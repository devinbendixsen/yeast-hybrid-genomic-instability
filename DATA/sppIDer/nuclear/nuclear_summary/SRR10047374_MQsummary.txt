SRR10047374 Num reads = 2510349
SRR10047374 Num mapped reads = 2272519
SRR10047374 Unmapped reads = 16.32%
SRR10047374 Average MQ = 47.8211312450978
SRR10047374 Median MQ = 60
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	237830	9.47%	0%	0	0	0	0
Scer	973225	38.77%	43.01%	54.11	58.29	60	60
Spar	30074	1.2%	1.05%	27.61	37.71	21	40
Smik	28174	1.12%	0.93%	28.54	40.95	26	46
Sjur	33562	1.34%	0.74%	14.89	32.13	0	39
Skud	26694	1.06%	0.62%	15.63	32.19	0	40
Sarb	39436	1.57%	1.13%	17.52	28.98	6	30
Seub	1067882	42.54%	49.85%	57.72	58.86	60	60
Suva	73472	2.93%	2.66%	34.22	44.92	40	60
