SRR8799693 Num reads = 15539362
SRR8799693 Num mapped reads = 15241740
SRR8799693 Unmapped reads = 7.66%
SRR8799693 Average MQ = 53.8424739702956
SRR8799693 Median MQ = 60
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	297622	1.92%	0%	0	0	0	0
Scer	8665198	55.76%	56.19%	54.54	58.62	60	60
Spar	58687	0.38%	0.3%	30.09	41.19	33	45
Smik	28605	0.18%	0.13%	25.83	38.45	22	40
Sjur	131651	0.85%	0.2%	5.56	26.11	0	25
Skud	64681	0.42%	0.07%	5.98	36.38	0	40
Sarb	10195	0.07%	0.04%	17.29	33.37	3	36
Seub	6178985	39.76%	42.49%	57.71	58.48	60	60
Suva	103738	0.67%	0.58%	35.4	43.99	40	50
