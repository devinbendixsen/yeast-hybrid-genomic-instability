SRR9925228 Num reads = 13817861
SRR9925228 Num mapped reads = 13363399
SRR9925228 Unmapped reads = 8.57%
SRR9925228 Average MQ = 53.8169713821843
SRR9925228 Median MQ = 60
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	454462	3.29%	0%	0	0	0	0
Scer	6269007	45.37%	46.92%	56.13	59.36	60	60
Spar	84886	0.61%	0.45%	32.07	48.02	40	60
Smik	57537	0.42%	0.35%	39.44	51.06	57	60
Sjur	163187	1.18%	0.13%	3.92	39	0	40
Skud	6712619	48.58%	51.76%	57.18	58.69	60	60
Sarb	15534	0.11%	0.06%	24.41	47.12	6	60
Seub	22458	0.16%	0.08%	17.93	40.67	0	48
Suva	38171	0.28%	0.24%	40.77	50.47	57	60
