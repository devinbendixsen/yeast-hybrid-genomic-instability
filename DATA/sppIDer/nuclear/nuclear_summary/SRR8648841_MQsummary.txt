SRR8648841 Num reads = 12597607
SRR8648841 Num mapped reads = 11425461
SRR8648841 Unmapped reads = 16.49%
SRR8648841 Average MQ = 47.1949554387591
SRR8648841 Median MQ = 60
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	1172146	9.3%	0%	0	0	0	0
Scer	5682697	45.11%	50.9%	55.06	58.42	60	60
Spar	221483	1.76%	1.53%	28.1	38.57	21	40
Smik	187775	1.49%	1.2%	26.31	39.25	17	42
Sjur	177717	1.41%	0.91%	17.54	32.42	3	40
Skud	152134	1.21%	0.76%	14.68	27.92	4	21
Sarb	273573	2.17%	1.69%	18.06	27.79	7	27
Seub	4248398	33.72%	39.47%	57.34	58.67	60	60
Suva	481684	3.82%	3.53%	34.49	44.73	40	60
