SRR12703366 Num reads = 23891725
SRR12703366 Num mapped reads = 22877405
SRR12703366 Unmapped reads = 11.67%
SRR12703366 Average MQ = 51.5453643887162
SRR12703366 Median MQ = 60
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	1014320	4.25%	0%	0	0	0	0
Scer	17145194	71.76%	76.23%	55.21	58.84	60	60
Spar	209418	0.88%	0.7%	30.7	43.52	36	52
Smik	137222	0.57%	0.44%	31.75	46.71	40	60
Sjur	316539	1.32%	0.34%	6.98	30.57	0	27
Skud	242954	1.02%	0.27%	7.23	30.33	0	33
Sarb	103084	0.43%	0.26%	15.78	30.14	3	27
Seub	4542026	19.01%	21.13%	57.93	59	60	60
Suva	180968	0.76%	0.62%	30.15	41.79	31	47
