SRR8799664 Num reads = 5927803
SRR8799664 Num mapped reads = 5733309
SRR8799664 Unmapped reads = 8.14%
SRR8799664 Average MQ = 52.6270311277214
SRR8799664 Median MQ = 60
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	194494	3.28%	0%	0	0	0	0
Scer	120151	2.03%	0.93%	17.32	41.05	0	40
Spar	39678	0.67%	0.42%	18.29	31.72	6	30
Smik	21127	0.36%	0.25%	23.36	35.54	9	40
Sjur	109963	1.86%	1.01%	17.04	34.19	0	40
Skud	135200	2.28%	1.71%	24.88	36.19	25	40
Sarb	34715	0.59%	0.44%	18.47	26.62	7	21
Seub	2544401	42.92%	45.61%	56.06	57.43	60	60
Suva	2728074	46.02%	49.63%	58.71	59.26	60	60
