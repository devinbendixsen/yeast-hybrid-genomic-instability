SRR800854 Num reads = 8742287
SRR800854 Num mapped reads = 7816489
SRR800854 Unmapped reads = 18.36%
SRR800854 Average MQ = 47.8238538725622
SRR800854 Median MQ = 60
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	925798	10.59%	0%	0	0	0	0
Scer	7096789	81.18%	93.28%	55.27	58.92	60	60
Spar	422829	4.84%	5.3%	50.33	56.2	60	60
Smik	56751	0.65%	0.5%	32.4	51.37	45	60
Sjur	91186	1.04%	0.22%	5.93	33.97	0	27
Skud	68057	0.78%	0.14%	5.41	37.3	0	40
Sarb	26199	0.3%	0.13%	14.52	42.32	0	58
Seub	19597	0.22%	0.1%	15.77	41.56	0	60
Suva	35081	0.4%	0.32%	31.74	48.68	40	60
