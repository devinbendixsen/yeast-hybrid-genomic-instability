SRR8799698 Num reads = 3578385
SRR8799698 Num mapped reads = 3527473
SRR8799698 Unmapped reads = 7.24%
SRR8799698 Average MQ = 53.5769309339269
SRR8799698 Median MQ = 60
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	50912	1.42%	0%	0	0	0	0
Scer	92582	2.59%	0.94%	13.08	38.77	0	40
Spar	18727	0.52%	0.41%	22.84	31.34	12	33
Smik	25241	0.71%	0.58%	34.02	44.82	46	60
Sjur	81846	2.29%	1.05%	16.19	37.99	0	43
Skud	125901	3.52%	2.51%	28.3	42.7	27	46
Sarb	25624	0.72%	0.53%	15.9	23.24	6	18
Seub	464210	12.97%	13.34%	53.51	56.11	60	60
Suva	2693342	75.27%	80.64%	59.07	59.44	60	60
