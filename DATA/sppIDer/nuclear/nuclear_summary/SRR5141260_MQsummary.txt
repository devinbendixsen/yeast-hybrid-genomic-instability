SRR5141260 Num reads = 49713182
SRR5141260 Num mapped reads = 45175130
SRR5141260 Unmapped reads = 15.55%
SRR5141260 Average MQ = 49.3510803432377
SRR5141260 Median MQ = 60
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	4538052	9.13%	0%	0	0	0	0
Scer	28660549	57.65%	64.43%	55.58	58.89	60	60
Spar	384726	0.77%	0.68%	34.35	46.44	40	60
Smik	243598	0.49%	0.39%	31.72	47.21	40	60
Sjur	698274	1.4%	0.53%	10.58	33.44	0	33
Skud	651306	1.31%	0.66%	15.15	35.68	0	40
Sarb	195284	0.39%	0.23%	16.46	33.19	0	40
Seub	13648907	27.46%	31.96%	58.26	59.26	60	60
Suva	692486	1.39%	1.12%	34.64	50.82	48	60
