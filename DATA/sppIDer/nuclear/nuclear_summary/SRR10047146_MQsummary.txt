SRR10047146 Num reads = 7723693
SRR10047146 Num mapped reads = 5280381
SRR10047146 Unmapped reads = 44.4%
SRR10047146 Average MQ = 27.5578001352462
SRR10047146 Median MQ = 12
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	2443312	31.63%	0%	0	0	0	0
Scer	1741023	22.54%	36.4%	50.45	56.19	60	60
Spar	297088	3.85%	4.84%	26.18	37.44	18	40
Smik	273412	3.54%	4.06%	23.67	37.1	9	40
Sjur	207520	2.69%	3.04%	21.77	34.65	12	40
Skud	209243	2.71%	2.99%	16.71	27.23	6	21
Sarb	434761	5.63%	6.55%	18.51	28.62	7	30
Seub	1357867	17.58%	28.72%	50.39	55.48	60	60
Suva	759467	9.83%	13.4%	34.59	45.65	42	60
