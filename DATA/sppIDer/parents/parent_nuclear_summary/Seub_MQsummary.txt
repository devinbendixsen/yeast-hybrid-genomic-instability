Seub Num reads = 14103160
Seub Num mapped reads = 12404118
Seub Unmapped reads = 18.86%
Seub Average MQ = 46.5614762223502
Seub Median MQ = 60
Species	Total mapped reads	% of all reads	% Nonzero mapped reads	All average MQ	NonZero average MQ	All Median MQ	Nonzero Median MQ
Unmapped	1699042	12.05%	0%	0	0	0	0
Scer	339434	2.41%	1.02%	10.26	29.74	0	31
Spar	40894	0.29%	0.21%	28.76	50.08	25	60
Smik	40597	0.29%	0.18%	24.52	47.57	5	60
Sjur	379842	2.69%	1.32%	12.85	32.29	0	27
Skud	286413	2.03%	1.05%	12.46	29.59	0	27
Sarb	41500	0.29%	0.15%	16.68	41.26	0	45
Seub	11053658	78.38%	94.91%	57.47	58.49	60	60
Suva	221780	1.57%	1.16%	29.92	50.05	33	60
