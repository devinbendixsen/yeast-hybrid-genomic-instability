# Yeast Hybrid Genomic Instability

Bendixsen et al. (in review) Python data (.p files) and scripts used for data analysis of high-throughput sequencing data.

## Installation

The directory is formatted as spyder python project.

## Usage

Python scripts in this directory encompass two categories:\
**(1)** Scripts used to analyze hybrid high-throughput sequencing data for various aspects of genomic instability.\
**(2)** Scripts that plot the data.
    
Figures presented in Bendixsen et al. (in review) are found in the Figures directory.

## Authors and acknowledgment

Python scripts were developed by **Devin Bendixsen, PhD**.
